<?php

use Illuminate\Database\Seeder;
use App\Models\UserModels\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 5)->states('not-approved')->create();
        factory(User::class, 15)->states('not-verified')->create();
        factory(User::class, 1)->states('admin', 'subscriber')->create();
        factory(User::class, 1)->states('content-manager')->create();
        factory(User::class, 1)->states('content-editor')->create();
        factory(User::class, 1)->states('content-editor', 'subscriber')->create();
        factory(User::class, 1)->states('subscriber')->create();
    }
}
