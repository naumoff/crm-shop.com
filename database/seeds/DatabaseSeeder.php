<?php

use Illuminate\Database\Seeder;
use Modules\CRM\Database\Seeds\CompaniesTableSeeder;
use Modules\CRM\Database\Seeds\WorkersTableSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);
         $this->call(CompaniesTableSeeder::class);
         $this->call(WorkersTableSeeder::class);
    }
}
