<?php

use App\Models\PageModels\Page;
use App\Services\DataHelpers\MenuPageBooleans;
use Illuminate\Database\Migrations\Migration;

class AddBooleanFieldsToMenuPages extends Migration
{
    use MenuPageBooleans;

    #region MAIN METHODS
    /**
     * Run the migrations.
     * @SuppressWarnings(PHPMD)
     * @return void
     */
    public function up()
    {
        $this->addBooleansToMainPage();
        $this->addBooleansToContactPage();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->removeBooleansFromMainPage();
        $this->removeBooleansFromContactPage();
    }
    #endregion

    #region SERVICE METHODS
    private function addBooleansToMainPage(): void
    {
        /** @var Page $mainPage */
        $mainPage = Page::find(Page::MAIN);
        $mainPage->booleanFields()->createMany($this->mainPageBooleans);
        $mainPage->touch();
    }

    private function addBooleansToContactPage(): void
    {
        /** @var Page $contactPage */
        $contactPage = Page::find(Page::CONTACTS);
        $contactPage->booleanFields()->createMany($this->contactsPageBooleans);
        $contactPage->touch();
    }

    private function removeBooleansFromMainPage(): void
    {
        /** @var Page $mainPage */
        $mainPage = Page::find(Page::MAIN);
        foreach ($this->mainPageBooleans as $boolean) {
            $mainPage->booleanFields()->where(['name' => $boolean['name']])->delete();
        }
        $mainPage->touch();
    }

    private function removeBooleansFromContactPage(): void
    {
        /** @var Page $contactPage */
        $contactPage = Page::find(Page::CONTACTS);
        foreach ($this->contactsPageBooleans as $boolean) {
            $contactPage->booleanFields()->where(['name' => $boolean['name']])->delete();
        }
        $contactPage->touch();
    }
    #endregion
}
