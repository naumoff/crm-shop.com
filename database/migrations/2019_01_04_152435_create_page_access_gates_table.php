<?php

use App\Services\Converters\DashConverters;
use App\Services\DataHelpers\MenuPages;
use App\Services\DataHelpers\UserRoles;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageAccessGatesTable extends Migration
{
    use UserRoles, DashConverters, MenuPages;

    #region MAIN METHODS
    /**
     * Run the migrations.
     * @SuppressWarnings(PHPMD)
     * @return void
     */
    public function up()
    {
        Schema::create('page_access_gates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('page_id')->unsigned()->index();
            foreach ($this->roleNames as $roleName) {
                $table->boolean($this->dashesToUnderscore($roleName))->default(1);
            }
            $table->timestamps();
            $table->foreign('page_id')->references('id')->on('pages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page_access_gates');
    }
    #endregion
}
