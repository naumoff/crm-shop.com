<?php

use App\Models\UserModels\Role;
use App\Services\DataHelpers\UserRoles;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    use UserRoles;

    #region MAIN METHODS
    /**
     * Run the migrations.
     * @SuppressWarnings(PHPMD)
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('role', '60');
            $table->timestamps();
        });
        $this->addRoles();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
    #endregion

    #region SERVICE METHODS
    private function addRoles()
    {
        $insertRolesArray = [];
        foreach ($this->roleNames as $id => $role) {
            if ($id === Role::UNDEFINED) {
                continue; // undefined role is virtual (not approved, not verifies, guest)
            };
            $insertRolesArray[] = [
                'id' => $id,
                'role' => $role,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];
        }
        DB::table('roles')->insert($insertRolesArray);
    }
    #endregion
}
