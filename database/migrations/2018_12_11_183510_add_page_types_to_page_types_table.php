<?php

use App\Models\PageModels\PageType;
use Illuminate\Database\Migrations\Migration;

class AddPageTypesToPageTypesTable extends Migration
{
    #region PROPERTIES
    private $pageTypes = [
        PageType::MENU => 'menu',
        PageType::SINGLE => 'single'
    ];
    #endregion

    #region MAIN METHODS
    /**
     * Run the migrations.
     * @SuppressWarnings(PHPMD)
     * @return void
     */
    public function up()
    {
        foreach ($this->pageTypes as $id => $type) {
            $pageType = new PageType();
            $pageType->id = $id;
            $pageType->type = $type;
            $pageType->save();
        };
    }

    /**
     * Reverse the migrations.
     * @SuppressWarnings(PHPMD)
     * @return void
     */
    public function down()
    {
        foreach ($this->pageTypes as $id => $name) {
            $pageType = PageType::find($id);
            $pageType->delete();
        }
    }
    #endregion
}
