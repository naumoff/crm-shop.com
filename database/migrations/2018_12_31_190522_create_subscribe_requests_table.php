<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscribeRequestsTable extends Migration
{
    /**
     * Run the migrations.
     * @SuppressWarnings(PHPMD)
     * @return void
     */
    public function up()
    {
        Schema::create('subscribe_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable()->unsigned();
            $table->string('email')->unique();
            $table->string('locale');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscribe_requests');
    }
}
