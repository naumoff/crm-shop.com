<?php

use App\Contracts\LocaleContract;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLongTextFieldsTable extends Migration
{
    /**
     * Run the migrations.
     * @SuppressWarnings(PHPMD)
     * @return void
     */
    public function up()
    {
        Schema::create('long_text_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 15);
            $table->string('description');
            foreach (LocaleContract::AVAILABLE_LOCALES as $locale) {
                $table->longText($locale);
            }
            $table->integer('page_id')->unsigned()->index();
            $table->foreign('page_id')->references('id')->on('pages')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('long_text_fields');
    }
}
