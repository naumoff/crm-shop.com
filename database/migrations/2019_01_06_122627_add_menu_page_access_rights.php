<?php

use App\Models\PageModels\Page;
use App\Models\PageModels\PageAccessGate;
use App\Services\Converters\DashConverters;
use App\Services\DataHelpers\MenuPages;
use App\Services\DataHelpers\UserRoles;
use Illuminate\Database\Migrations\Migration;

class AddMenuPageAccessRights extends Migration
{
    use MenuPages, DashConverters, UserRoles;
    /**
     * Run the migrations.
     * @SuppressWarnings(PHPMD)
     * @return void
     */
    public function up()
    {
        foreach ($this->getMenuPageIds() as $menuPageId) {
            /** @var Page $menuPage */
            $menuPage = Page::find($menuPageId);
            /** @var PageAccessGate $pageAccessGate */
            $pageAccessGate = new PageAccessGate();
            foreach ($this->roleNames as $roleName) {
                $column = $this->dashesToUnderscore($roleName);
                $pageAccessGate->$column = 1;
            }
            $menuPage->pageAccessGate()->save($pageAccessGate);
            $menuPage->touch();
        }
    }

    /**
     * @throws Exception
     */
    public function down()
    {
        foreach ($this->getMenuPageIds() as $menuPageId) {
            /** @var Page $menuPage */
            $menuPage = Page::find($menuPageId);
            /** @var PageAccessGate $pageAccessGate */
            $pageAccessGate = PageAccessGate::where('page_id', '=', $menuPageId)->first();
            if (!empty($pageAccessGate)) {
                $pageAccessGate->delete();
                $menuPage->touch();
            }
        }
    }
}
