<?php

use App\Contracts\LocaleContract;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesMetadataTable extends Migration
{
    /**
     * Run the migrations.
     * @SuppressWarnings(PHPMD)
     * @return void
     */
    public function up()
    {
        Schema::create('metadata_fields', function (Blueprint $table) {
            $table->increments('id');
            foreach (LocaleContract::AVAILABLE_LOCALES as $locale) {
                $table->string('header_'.$locale, 50)->nullable();
                $table->string('title_'.$locale, 125)->nullable();
                $table->string('description_'.$locale, 160)->nullable();
                $table->string('keywords_'.$locale, 160)->nullable();
            }
            $table->integer('page_id')->unsigned()->index()->unique();
            $table->foreign('page_id')->references('id')->on('pages')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metadata_fields');
    }
}
