<?php

use Faker\Generator as Faker;
use App\Models\UserModels\User;
use \App\Models\UserModels\Role;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'approved' => '1',
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => Str::random(10),
    ];
});

$factory->state(User::class, 'not-approved', function ($user, $faker) {
    return [
        'approved' => 0
    ];
});

$factory->state(User::class, 'approved', function ($user, $faker) {
    return [
        'approved' => 1
    ];
});

$factory->state(User::class, 'not-verified', function ($user, $faker) {
    return [
        'email_verified_at' => null
    ];
});

$factory->state(User::class, 'verified', function ($user, $faker) {
    return [
        'approved' => now()
    ];
});

$factory->afterCreatingState(User::class, 'admin', function (User $user, $faker) {
    $user->attachRole(Role::ADMIN);
});

$factory->afterCreatingState(User::class, 'content-manager', function (User $user, $faker) {
    $user->attachRole(Role::CONTENT_MANAGER);
});

$factory->afterCreatingState(User::class, 'content-editor', function (User $user, $faker) {
    $user->attachRole(Role::CONTENT_EDITOR);
});

$factory->afterCreatingState(User::class, 'subscriber', function (User $user, $faker) {
    $user->attachRole(Role::SUBSCRIBER);
});
