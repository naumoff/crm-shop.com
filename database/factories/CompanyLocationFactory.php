<?php

use Faker\Generator as Faker;
use Modules\CRM\App\Models\Company;
use Modules\CRM\App\Models\CompanyLocation;
use Modules\CRM\App\Models\Country;

$factory->define(CompanyLocation::class, function (Faker $faker) {
    $countriesArray = Country::pluck('id')->toArray();
    $countryId = $countriesArray[array_rand($countriesArray)];
    return [
        'country_id' => $countryId,
        'company_id' => factory(Company::class)->create()->id,
        'address' => $faker->address,
    ];
});
