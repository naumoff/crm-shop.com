<?php

namespace Modules\CRM;

use Illuminate\Support\ServiceProvider;
use Modules\CRM\App\Contracts\CompanyContract;
use Modules\CRM\App\Contracts\MultiTenancyContract;
use Modules\CRM\App\Services\AppLogic\CompanyService;
use Modules\CRM\App\Services\MultiTenancyService;
use Illuminate\Support\Facades\Response;

class CRMServiceProvider extends ServiceProvider
{
    #region PROPERTIES
    /**
     * All of the container singletons that should be registered.
     * @var array

     */
    public $singletons = [
        MultiTenancyContract::class => MultiTenancyService::class,
        CompanyContract::class => CompanyService::class,
    ];
    #endregion

    #region MAIN METHODS
    /**
     * Bootstrap services.
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes/crm.php');
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
        $this->publishes([
            __DIR__.'/database/factories' => database_path('factories')
        ]);
    }

    /**
     * Register services.
     * @return void
     */
    public function register()
    {
        //
    }
    #endregion
}
