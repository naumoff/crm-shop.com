<?php
/**
 * User: Andrey Naumoff
 * Date: 21-Jul-19
 * Time: 3:53 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace Modules\CRM\App\Services\Helpers;

use Illuminate\Support\Facades\App;
use Modules\CRM\App\Contracts\MultiTenancyContract;

trait CrmConnection
{
    /**
     * @return string
     */
    private function getCrmConnection(): string
    {
        /** @var MultiTenancyContract $multiTenancyService */
        $multiTenancyService = App::make(MultiTenancyContract::class);
        return $multiTenancyService->getConnectionName();
    }
}
