<?php
/**
 * USER: Andrey Naumoff
 * DATE: 9/29/2019
 * TIME: 5:11 PM
 * EMAIL: andrey.naumoff@crm-shop.com
 */

namespace Modules\CRM\app\Services\Helpers;

trait JsonApiHeader
{
    /**
     * Customize the outgoing response for the resource.
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Http\Response  $response
     * @return void
     */
    public function withResponse($request, $response)
    {
        $response->header('Content-Type', 'application/vnd.api+json');
    }
}
