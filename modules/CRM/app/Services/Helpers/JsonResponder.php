<?php
/**
 * User: Andrey Naumoff
 * Date: 21-Jul-19
 * Time: 5:37 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace Modules\CRM\App\Services\Helpers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;

trait JsonResponder
{
    #region PROPERTIES
    /** @var int $statusCode */
    private $statusCode = 200;
    #endregion

    #region MAIN METHODS
    /**
     * @param int $statusCode
     * @return $this
     */
    public function setStatusCode(int $statusCode): self
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @param string $message
     * @return JsonResponse
     */
    public function respondWithError(string $message): JsonResponse
    {
        return $this->respond(
            [
                'error' => [
                    'message' => $message,
                    'status_code' => $this->getStatusCode()
                ]
            ]
        );
    }

    /**
     * @param string $message
     * @return JsonResponse
     */
    public function respondNotFound(string $message = 'Not Found'): JsonResponse
    {
        return $this->setStatusCode(404)->respondWithError($message);
    }

    /**
     * @param Model $model
     * @return JsonResponse
     */
    public function respondModelCreated(Model $model): JsonResponse
    {
        return $this->setStatusCode(201)->respond([
            'data' => [
                'status' => 'created',
                'status_code' => $this->getStatusCode(),
                'id' => $model->id,
            ]
        ]);
    }

    /**
     * @param Model $model
     * @return JsonResponse
     */
    public function respondModelUpdated(Model $model): JsonResponse
    {
        return $this->setStatusCode(202)->respond([
            'data' => [
                'status' => 'updated',
                'status_code' => $this->getStatusCode(),
                'id' => $model->id,
            ]
        ]);
    }

    /**
     * @param Model $model
     * @return JsonResponse
     */
    public function respondModelDeleted(Model $model): JsonResponse
    {
        return $this->setStatusCode(202)->respond([
            'data' => [
                'status' => 'deleted',
                'status_code' => $this->getStatusCode(),
                'id' => $model->id,
            ]
        ]);
    }

    /**
     * @param string $message
     * @param array $errors
     * @return JsonResponse
     */
    public function respondWithValidationError(string $message, array $errors): JsonResponse
    {
        return $this->setStatusCode(422)->respond([
            'error' => [
                'message' => $message,
                'validation_errors' => $errors,
            ]
        ]);
    }

    /**
     * @param string $message
     * @return JsonResponse
     *
     */
    public function respondWithAuthorizationError(string $message = 'You are not authorized.'): JsonResponse
    {
        return $this->setStatusCode(401)->respond([
            'error' => [
                'message' => $message,
                'status_code' => $this->getStatusCode(),
            ]
        ]);
    }

    /**
     * @param array $data
     * @param array $headers
     * @return JsonResponse
     */
    public function respond(array $data, array $headers = []): JsonResponse
    {
        return Response::json($data, $this->getStatusCode(), $headers);
    }
    #endregion
}
