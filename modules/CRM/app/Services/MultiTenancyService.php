<?php
/**
 * User: Andrey Naumoff
 * Date: 05-Mar-19
 * Time: 11:07 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace Modules\CRM\App\Services;

use Modules\CRM\App\Contracts\MultiTenancyContract;

class MultiTenancyService implements MultiTenancyContract
{
    #region PROPERTIES
    /** @var string $connectionName */
    private $connectionName;
    #endregion

    #region MAIN METHODS
    public function __construct()
    {
        $this->setCRMConnection();
    }

    /**
     * @return string
     */
    public function getConnectionName(): string
    {
        return $this->connectionName;
    }

    /**
     * temporary plug - later dynamic db creation on the fly
     * @todo complete multi tenancy service that create new db and connection to it on the fly
     */
    private function setCRMConnection(): void
    {
        config([
            'database.connections.crm' => [
                'driver' => 'mysql',
                'host' => env('DB_HOST_CRM', '127.0.0.1'),
                'port' => env('DB_PORT_CRM', '3306'),
                'database' => env('DB_DATABASE_CRM', 'pb_crm'),
                'username' => env('DB_USERNAME_CRM', 'homestead'),
                'password' => env('DB_PASSWORD_CRM', 'secret'),
                'unix_socket' => env('DB_SOCKET_CRM', ''),
                'charset' => 'utf8mb4',
                'collation' => 'utf8mb4_unicode_ci',
                'prefix' => '',
                'prefix_indexes' => true,
                'strict' => true,
                'engine' => null,
            ]
        ]);
        $this->connectionName = 'crm';
    }
    #endregion
}
