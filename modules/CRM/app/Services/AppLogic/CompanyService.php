<?php
/**
 * User: Andrey Naumoff
 * Date: 22-Jul-19
 * Time: 11:44 AM
 * E-mail: andrey.naumoff@crm-shop.com
 */

namespace Modules\CRM\App\Services\AppLogic;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Modules\CRM\App\Contracts\CompanyContract;
use Modules\CRM\App\Models\Company;
use Modules\CRM\App\Services\Helpers\JsonResponder;

class CompanyService implements CompanyContract
{
    use JsonResponder;

    #region MAIN METHODS
    /**
     * @param array $validatedInput
     * @return JsonResponse
     */
    public function saveCompany(array $validatedInput): JsonResponse
    {
        DB::beginTransaction();
        try {
            $company = new Company();
            $company->company = $validatedInput['company'];
            $company->phones = $this->encodeAndCleanSimpleArray($validatedInput['phones'] ?? []);
            $company->emails = $this->encodeAndCleanSimpleArray($validatedInput['emails'] ?? []);
            $company->sites = $this->encodeAndCleanSimpleArray($validatedInput['sites'] ?? []);
            $company->save();
            DB::commit();
            return $this->respondModelCreated($company);
        } catch (\Throwable $throwable) {
            DB::rollback();
            \LogRec::error($throwable->getMessage());
            return $this->respondWithError('company was not created! Contact site admin, please...');
        }
    }

    /**
     * @param array $validatedInput
     * @param Company $company
     * @return JsonResponse
     */
    public function updateCompany(array $validatedInput, Company $company): JsonResponse
    {
        DB::beginTransaction();
        try {
            $company->company = $validatedInput['company'];
            $company->phones = $this->encodeAndCleanSimpleArray($validatedInput['phones'] ?? []);
            $company->emails = $this->encodeAndCleanSimpleArray($validatedInput['emails'] ?? []);
            $company->sites = $this->encodeAndCleanSimpleArray($validatedInput['sites'] ?? []);
            $company->save();
            DB::commit();
            return $this->respondModelUpdated($company);
        } catch (\Throwable $throwable) {
            DB::rollback();
            \LogRec::error($throwable->getMessage());
            return $this->respondWithError('company was not updated! Contact site admin, please...');
        }
    }

    /**
     * @param Company $company
     * @return JsonResponse
     */
    public function deleteCompany(Company $company): JsonResponse
    {
        DB::beginTransaction();
        try {
            $company->delete();
            DB::commit();
            return $this->respondModelDeleted($company);
        } catch (\Throwable $throwable) {
            DB::rollback();
            \LogRec::error($throwable->getMessage());
            return $this->respondWithError('company was not deleted! Contact site admin, please...');
        }
    }
    #endregion

    #region SERVICE METHODS
    /**
     * @param array $simpleArray
     * @return string
     */
    private function encodeAndCleanSimpleArray(array $simpleArray): string
    {
        $result = [];
        foreach ($simpleArray as $element) {
            if (empty($element)) {
                continue;
            }
            $result[] = $element;
        }
        return json_encode(array_unique($result));
    }
    #endregion
}
