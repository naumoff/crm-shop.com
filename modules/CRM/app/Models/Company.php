<?php

namespace Modules\CRM\App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\CRM\App\Services\Helpers\CrmConnection;

class Company extends Model
{
    use CrmConnection, SoftDeletes;

    #region CLASS CONSTANTS
    #endregion

    #region CLASS PROPERTIES
    protected $table = 'companies';
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at'];
    protected $connection = 'crm';
    #endregion

    #region MAIN METHODS
    /**
     * Company constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setConnection($this->getCrmConnection());
    }
    #endregion

    #region SCOPE METHODS
    #endregion

    #region RELATION METHODS
    /**
     * @return BelongsToMany
     */
    public function workers(): BelongsToMany
    {
        return $this
            ->belongsToMany(Worker::class, 'employment', 'company_id', 'worker_id')
            ->using(Employment::class)
            ->withPivot([
                'position',
                'department_id',
                'status',
                'created_at',
                'updated_at',
            ]);
    }
    
    /**
     * @return BelongsToMany
     */
    public function industries(): BelongsToMany
    {
        return $this
            ->belongsToMany(
                Industry::class,
                'company_industries',
                'company_id',
                'industry_id'
            )
            ->using(CompanyIndustry::class);
    }
    
    /**
     * @return BelongsToMany
     */
    public function locations(): BelongsToMany
    {
        return $this
            ->belongsToMany(
                Country::class,
                'company_locations',
                'company_id',
                'country_id'
            )
            ->using(CompanyLocation::class)
            ->withPivot('address');
    }
    #endregion
}
