<?php

namespace Modules\CRM\App\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\CRM\App\Services\Helpers\CrmConnection;

class Industry extends Model
{
    use CrmConnection;
    
    #region CLASS CONSTANTS
    #endregion
    
    #region CLASS PROPERTIES
    protected $table = 'industries';
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at'];
    protected $connection = 'crm';
    #endregion
    
    #region MAIN METHODS
    /**
     * Company constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setConnection($this->getCrmConnection());
    }
    #endregion
    
    #region SCOPE METHODS
    #endregion
    
    #region RELATION METHODS
    #endregion
}
