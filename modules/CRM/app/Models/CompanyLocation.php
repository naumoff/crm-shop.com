<?php

namespace Modules\CRM\App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Modules\CRM\App\Services\Helpers\CrmConnection;

class CompanyLocation extends Pivot
{
    use CrmConnection;
    #region CLASS CONSTANTS
    #endregion
    
    #region CLASS PROPERTIES
    /**
     * Indicates if the IDs are auto-incrementing.
     * @var bool
     */
    public $incrementing = true;
    protected $table = 'company_locations';
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at'];
    protected $connection = 'crm';
    #endregion
    
    #region MAIN METHODS
    /**
     * Company constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setConnection($this->getCrmConnection());
    }
    #endregion
    
    #region SCOPE METHODS
    #endregion
    
    #region RELATION METHODS
    #endregion
}
