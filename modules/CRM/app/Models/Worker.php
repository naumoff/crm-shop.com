<?php

namespace Modules\CRM\App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\CRM\App\Services\Helpers\CrmConnection;

class Worker extends Model
{
    use CrmConnection, SoftDeletes;

    #region CLASS CONSTANTS
    #endregion

    #region CLASS PROPERTIES
    protected $table = 'workers';
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at'];
    protected $connection = 'crm';
    #endregion

    #region MAIN METHODS
    /**
     * Company constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->setConnection($this->getCrmConnection());
    }
    #endregion

    #region SCOPE METHODS
    #endregion

    #region RELATION METHODS
    /**
     * @return BelongsToMany
     */
    public function companies(): BelongsToMany
    {
        return $this
            ->belongsToMany(Company::class, 'employment', 'worker_id', 'company_id')
            ->using(Employment::class)
            ->withPivot([
                'position',
                'status',
                'created_at',
                'updated_at',
            ]);
    }
    #endregion
}
