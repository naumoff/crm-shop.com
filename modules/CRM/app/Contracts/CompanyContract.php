<?php
/**
 * User: Andrey Naumoff
 * Date: 22-Jul-19
 * Time: 11:45 AM
 * E-mail: andrey.naumoff@crm-shop.com
 */

namespace Modules\CRM\App\Contracts;

use Illuminate\Http\JsonResponse;
use Modules\CRM\App\Models\Company;

interface CompanyContract
{
    /**
     * @param array $validatedInput
     * @return JsonResponse
     */
    public function saveCompany(array $validatedInput): JsonResponse;

    /**
     * @param Company $company
     * @return JsonResponse
     */
    public function deleteCompany(Company $company): JsonResponse;

    /**
     * @param array $validatedInput
     * @param Company $company
     * @return JsonResponse
     */
    public function updateCompany(array $validatedInput, Company $company): JsonResponse;
}
