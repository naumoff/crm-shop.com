<?php
/**
 * User: Andrey Naumoff
 * Date: 05-Mar-19
 * Time: 11:10 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace Modules\CRM\App\Contracts;

interface MultiTenancyContract
{
    /**
     * @return string
     */
    public function getConnectionName(): string;
}
