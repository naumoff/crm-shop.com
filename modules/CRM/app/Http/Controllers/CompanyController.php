<?php

namespace Modules\CRM\App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Modules\CRM\App\Contracts\CompanyContract;
use Modules\CRM\App\Http\Requests\RequestCompanyPatch;
use Modules\CRM\App\Http\Requests\RequestCompanyPost;
use Modules\CRM\App\Http\Resources\CompanyResource;
use Modules\CRM\App\Models\Company;

class CompanyController extends Controller
{
    #region PROPERTIES
    /** @var CompanyContract $companyService */
    private $companyService;
    #endregion

    #region MAIN METHODS
    /**
     * CompanyController constructor.
     * @param CompanyContract $companyContract
     */
    public function __construct(CompanyContract $companyContract)
    {
        $this->companyService = $companyContract;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return CompanyResource::collection(Company::with(['industries', 'locations'])
            ->paginate(10))->response()->header('Content-Type', 'application/vnd.api+json');
    }

    /**
     * @param RequestCompanyPost $request
     * @return JsonResponse
     */
    public function store(RequestCompanyPost $request): JsonResponse
    {
        return $this->companyService->saveCompany($request->validated());
    }

    /**
     * @param Company $company
     * @return CompanyResource
     */
    public function show(Company $company): CompanyResource
    {
        return new CompanyResource($company);
    }

    /**
     * @param RequestCompanyPatch $request
     * @param Company $company
     * @return JsonResponse
     */
    public function update(RequestCompanyPatch $request, Company $company): JsonResponse
    {
        return $this->companyService->updateCompany($request->validated(), $company);
    }

    /**
     * @param Company $company
     * @return JsonResponse
     */
    public function destroy(Company $company): JsonResponse
    {
        return $this->companyService->deleteCompany($company);
    }
    #endregion
}
