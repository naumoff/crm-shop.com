<?php

namespace Modules\CRM\App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\CRM\app\Services\Helpers\JsonApiHeader;

class CompanyResource extends JsonResource
{
    use JsonApiHeader;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'company' => $this->company,
            'logo' => $this->logo,
            'phones' => json_decode($this->phones),
            'emails' => json_decode($this->emails),
            'domains' => json_decode($this->domains),
            'industries' => CompanyIndustryResource::collection($this->whenLoaded('industries')),
            'locations' => CompanyLocationResource::collection($this->whenLoaded('locations'))
        ];
    }
}
