<?php

namespace Modules\CRM\App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Modules\CRM\app\Services\Helpers\JsonApiHeader;

class CompanyLocationResource extends JsonResource
{
    use JsonApiHeader;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
