<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Modules\CRM\App\Services\Helpers\CrmConnection;

class CreateWorkersTable extends Migration
{
    use CrmConnection;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->getCrmConnection())->create('workers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->json('phones')->nullable();
            $table->json('mobiles')->nullable();
            $table->json('emails')->nullable();
            $table->json('other')->nullable();
            $table->json('social_links')->nullable();
            $table->unsignedInteger('country_id')->nullable();
            $table->string('address')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('country_id')->references('id')->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->getCrmConnection())->dropIfExists('workers');
    }
}
