<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\CRM\App\Models\Country;
use Modules\CRM\App\Services\Helpers\CrmConnection;

class CreateCountriesTable extends Migration
{
    use CrmConnection;
    
    #region MAIN METHODS
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::connection($this->getCrmConnection())->create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('country');
            $table->timestamps();
        });
        $this->addMainCountries();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::connection($this->getCrmConnection())->dropIfExists('countries');
    }
    #endregion
    
    #region SERVICE METHODS
    private function addMainCountries(): void
    {
        $countryList = [
            'Australia',
            'Belgium',
            'Canada',
            'France',
            'Germany',
            'Netherlands',
            'Russia',
            'Spain',
            'Ukraine',
            'United Kingdom',
            'USA',
        ];
        foreach ($countryList as $countryName) {
            $country = new Country;
            $country->country = $countryName;
            $country->save();
        }
    }
    #endregion
}
