<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\CRM\App\Services\Helpers\CrmConnection;

class CreateActionsTable extends Migration
{
    use CrmConnection;
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->getCrmConnection())->create('actions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum(
                'action_type',
                ['research', 'to-do', 'cold-call', 'cold-mail', 'call', 'mail', 'meeting']
            );
            $table->longText('action');
            $table->longText('result')->nullable();
            $table->unsignedBigInteger('company_id')->nullable();
            $table->unsignedBigInteger('worker_id')->nullable();
            $table->dateTime('due_date')->nullable();
            $table->dateTime('due_datetime')->nullable();
            $table->enum(
                'status',
                ['open', 'done', 'postponed', 'cancelled']
            );
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->getCrmConnection())->dropIfExists('actions');
    }
}
