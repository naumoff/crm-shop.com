<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\CRM\App\Services\Helpers\CrmConnection;

class CreateCompanyIndustriesTable extends Migration
{
    use CrmConnection;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->getCrmConnection())->create('company_industries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('company_id');
            $table->unsignedInteger('industry_id');
            $table->timestamps();
            $table->foreign('company_id')
                ->references('id')
                ->on('companies')
                ->onDelete('cascade');
            $table->foreign('industry_id')
                ->references('id')
                ->on('industries')
                ->onDelete('cascade');
            $table->unique(['company_id', 'industry_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->getCrmConnection())->dropIfExists('company_industries');
    }
}
