<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\CRM\App\Models\Industry;
use Modules\CRM\App\Services\Helpers\CrmConnection;

class CreateIndustriesTable extends Migration
{
    use CrmConnection;
    
    #region MAIN METHODS
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->getCrmConnection())->create('industries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('industry');
            $table->string('description');
            $table->timestamps();
        });
        $this->addMainIndustries();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->getCrmConnection())->dropIfExists('industries');
    }
    #endregion
    
    #region SERVICE METHODS
    private function addMainIndustries()
    {
        $industryList = [
            'Agricultural Services & Products' =>
                'companies engaged in production/selling products/services in agriculture business',
            'Advertising/Public Relations' =>
                'companies engaged in marketing and advertising',
            'Crop Production & Basic Processing' =>
                'companies engaged in crop production',
            'Earth Moving Machinery' =>
                'companies engaged in production/selling/service of earth moving equipment',
            'It industry' =>
                'companies engaged in collection, processing, distribution and use of information',
        ];
        foreach ($industryList as $industryName => $industryDescription) {
            $industry = new Industry();
            $industry->industry = $industryName;
            $industry->description = $industryDescription;
            $industry->save();
        }
    }
    #endregion
}
