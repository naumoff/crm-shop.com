<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\CRM\App\Services\Helpers\CrmConnection;

class CreateGroupsTable extends Migration
{
    use CrmConnection;
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->getCrmConnection())->create('groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('group');
            $table->string('index');
            $table->unsignedBigInteger('parent_id')->comment('self reference table');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->getCrmConnection())->dropIfExists('groups');
    }
}
