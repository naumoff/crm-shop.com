<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Modules\CRM\App\Services\Helpers\CrmConnection;

class CreateCompanyRelationsTable extends Migration
{
    use CrmConnection;
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->getCrmConnection())->create('company_relations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('mother_id');
            $table->string('mother_role');
            $table->unsignedBigInteger('daughter_id');
            $table->string('daughter_role');
            $table->timestamps();
            $table->foreign('mother_id')
                ->references('id')
                ->on('companies')
                ->onDelete('cascade');
            $table->foreign('daughter_id')
                ->references('id')
                ->on('companies')
                ->onDelete('cascade');
            $table->unique(['mother_id', 'daughter_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->getCrmConnection())->dropIfExists('company_relations');
    }
}
