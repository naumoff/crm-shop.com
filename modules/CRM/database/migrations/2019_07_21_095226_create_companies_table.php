<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Modules\CRM\App\Services\Helpers\CrmConnection;

class CreateCompaniesTable extends Migration
{
    use CrmConnection;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->getCrmConnection())->create('companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('company');
            $table->string('logo')->nullable();
            $table->json('phones')->nullable();
            $table->json('emails')->nullable();
            $table->json('domains')->nullable();
            $table->unsignedBigInteger('creator_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->getCrmConnection())->dropIfExists('companies');
    }
}
