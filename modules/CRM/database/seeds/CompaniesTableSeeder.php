<?php
namespace Modules\CRM\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\CRM\App\Models\Company;
use Modules\CRM\App\Models\CompanyIndustry;
use Modules\CRM\App\Models\CompanyLocation;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Company::class, 5)->create();
        factory(Company::class, 5)->state('hasIndustry')->create();
        factory(Company::class, 5)->state('hasOneAddress')->create();
        factory(Company::class, 5)->state('hasTwoAddresses')->create();
        factory(Company::class, 5)->states(['hasTwoAddresses', 'hasIndustry'])->create();
    }
}
