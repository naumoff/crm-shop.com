<?php
namespace Modules\CRM\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\CRM\App\Models\Worker;

class WorkersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Worker::class, 5)->state('active')->create();
        factory(Worker::class, 5)->state('terminated')->create();
    }
}
