<?php

use Faker\Generator as Faker;
use Modules\CRM\App\Models\Company;
use Modules\CRM\App\Models\CompanyIndustry;
use Modules\CRM\App\Models\CompanyLocation;

$factory->define(Company::class, function (Faker $faker) {
    return [
        'company' => $faker->company,
        'phones' => json_encode([
            $faker->phoneNumber,
            $faker->phoneNumber,
        ]),
        'emails' => json_encode([
            $faker->companyEmail,
            $faker->companyEmail,
        ]),
        'domains' => json_encode([
            'http://'.$faker->domainName,
        ]),
        'creator_id' => 1,
    ];
});

$factory->afterCreatingState(Company::class, 'hasOneAddress', function (Company $company, $faker) {
    factory(CompanyLocation::class)->create(['company_id' => $company->id]);
});

$factory->afterCreatingState(Company::class, 'hasTwoAddresses', function (Company $company, $faker) {
    factory(CompanyLocation::class, 2)->create(['company_id' => $company->id]);
});

$factory->afterCreatingState(Company::class, 'hasIndustry', function (Company $company, $faker) {
    factory(CompanyIndustry::class)->create(['company_id' => $company->id]);
});
