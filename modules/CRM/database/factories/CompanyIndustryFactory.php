<?php

use Faker\Generator as Faker;
use Modules\CRM\App\Models\Company;
use Modules\CRM\App\Models\CompanyIndustry;
use Modules\CRM\App\Models\Industry;

$factory->define(CompanyIndustry::class, function (Faker $faker) {
    $industriesArray = Industry::pluck('id')->toArray();
    $industryId = $industriesArray[array_rand($industriesArray)];
    return [
        'industry_id' => $industryId,
        'company_id' => factory(Company::class)->create()->id,
    ];
});
