<?php

use Faker\Generator as Faker;
use Modules\CRM\App\Models\Company;
use Modules\CRM\App\Models\Country;
use Modules\CRM\App\Models\Worker;

$factory->define(Worker::class, function (Faker $faker) {
    $countriesArray = Country::pluck('id')->toArray();
    $countryId = $countriesArray[array_rand($countriesArray)];
    return [
        'name' => $faker->name,
        'phones' => json_encode([
            $faker->phoneNumber,
            $faker->phoneNumber,
        ]),
        'mobiles' => json_encode([
            $faker->phoneNumber,
        ]),
        'emails' => json_encode([
            $faker->safeEmail
        ]),
        'other' => json_encode([
            'skype' => $faker->userName,
            'viber' => $faker->phoneNumber,
            'telegram' => $faker->phoneNumber,
        ]),
        'social_links' => json_encode([
            $faker->url,
            $faker->url,
            $faker->url,
        ]),
        'country_id' => $countryId,
        'address' => $faker->address,
    ];
});

$factory->afterCreatingState(Worker::class, 'active', function (Worker $worker, $faker) {
    $worker->companies()->attach(factory(Company::class)->states(['hasOneAddress', 'hasIndustry'])->create()->id, [
        'position' => getRandomPosition(),
        'status' => 'active'
    ]);
});

$factory->afterCreatingState(Worker::class, 'terminated', function (Worker $worker, $faker) {
    $worker->companies()->attach(factory(Company::class)->state('hasTwoAddresses')->create()->id, [
        'position' => getRandomPosition(),
        'status' => 'terminated'
    ]);
});

/**
 * @return string
 * @throws Exception
 */
function getRandomPosition(): string
{
    $positions = [
        'Marketing Manager',
        'CEO',
        'Sales Manager',
        'Director',
        'Owner'
    ];
    return $positions[random_int(0, count($positions)-1)];
}
