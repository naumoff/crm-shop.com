<?php
/**
 * User: Andrey Naumoff
 * Date: 16-Jul-19
 * Time: 10:31 PM
 * E-mail: andrey.naumoff@gmail.com
 */

use App\Models\UserModels\Role;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'namespace' => 'Modules\CRM\App\Http\Controllers',
    'prefix' => 'api/crm',
    'middleware' => [
        'auth:api',
        'role:' . Role::ADMIN .','. Role::SALES_MANAGER .','. Role::SALES_AGENT,
        'approved',
        'auth',
        'bindings'
    ]
], function () {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });
    Route::apiResource('companies', 'CompanyController');
});
