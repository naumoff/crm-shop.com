<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- End Required meta tags -->
    <!-- Begin SEO tag -->
    <title> Getting Started | Looper - Bootstrap 4 Admin Theme </title>
    <meta property="og:title" content="Getting Started">
    <meta name="author" content="Beni Arisandi">
    <meta property="og:locale" content="en_US">
    <meta name="description" content="Responsive admin theme build on top of Bootstrap 4">
    <meta property="og:description" content="Responsive admin theme build on top of Bootstrap 4">
    <link rel="canonical" href="http://uselooper.com">
    <meta property="og:url" content="http://uselooper.com">
    <meta property="og:site_name" content="Looper - Bootstrap 4 Admin Theme">
    <script type="application/ld+json">
      {
        "name": "Looper - Bootstrap 4 Admin Theme",
        "description": "Responsive admin theme build on top of Bootstrap 4",
        "author":
        {
          "@type": "Person",
          "name": "Beni Arisandi"
        },
        "@type": "WebSite",
        "url": "",
        "headline": "Getting Started",
        "@context": "http://schema.org"
      }
    </script>
    <!-- End SEO tag -->
    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,500,600" rel="stylesheet">
    <!-- End Google font -->
    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="144x144" href="/dashboard/apple-touch-icon.png">
    <link rel="shortcut icon" href="/dashboard/favicon.ico">
    <meta name="theme-color" content="#070B0F">
    <!-- End Favicons -->
    <!-- BEGIN PLUGINS STYLES -->
    <link rel="stylesheet" href="/dashboard/vendor/open-iconic/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="/dashboard/vendor/fontawesome/css/all.css">
    <link rel="stylesheet" href="/dashboard/vendor/highlightjs/styles/ocean.css">
    <!-- END PLUGINS STYLES -->
    <!-- BEGIN THEME STYLES -->
    <link rel="stylesheet" href="/dashboard/stylesheets/theme.min.css" data-skin="default">
    <link rel="stylesheet" href="/dashboard/stylesheets/theme-dark.min.css" data-skin="dark">
    <!-- Disable unused skin immediately -->
    <script> var skin = localStorage.getItem('skin') || 'default';
        var unusedLink = document.querySelector('link[data-skin]:not([data-skin="'+ skin +'"])');

        unusedLink.setAttribute('rel', '');
        unusedLink.setAttribute('disabled', true);
    </script>
    <!-- END THEME STYLES -->
</head>
<body data-spy="scroll" data-target="#nav-content" data-offset="76">
    <div class="app">
        <!-- .app-header -->
        <header class="app-header app-header-dark bg-black">
            <!-- your brand and navigations goes here -->
            @include('dashboards.inclusions.header')
        </header>
        <!-- /.app-header -->

        @if(Auth::check() && Auth::user()->isApproved() && Route::currentRouteName() !== 'panel' && Route::currentRouteName() !== 'confirmation-stand-by' )
            <!-- .app-aside -->
            <aside class="app-aside app-aside-expand-md app-aside-light">
                <!-- the app navigations goes here -->
                @if(isset($asidePath) && $asidePath !== null)
                    @include($asidePath)
                @endif
            </aside>
            <!-- /.app-aside -->
        @endif

        <!-- .app-main -->
        <main class="app-main">
            <!-- .wrapper -->
            <div class="wrapper">
                <!-- your awesome app content goes here -->
                @yield('content')
            </div>
            <!-- /.wrapper -->
        </main>
        <!-- /.app-main -->

    </div>
    <!-- BEGIN BASE JS -->
    <script src="/dashboard/vendor/jquery/jquery.min.js"></script>
    <script src="/dashboard/vendor/bootstrap/js/popper.min.js"></script>
    <script src="/dashboard/vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- END BASE JS -->

    <!-- BEGIN PLUGINS JS -->
    <script src="/dashboard/vendor/highlightjs/highlight.pack.js"></script>
    <script src="/dashboard/vendor/pace/pace.min.js"></script>
    <script src="/dashboard/vendor/stacked-menu/stacked-menu.min.js"></script>
    <script src="/dashboard/vendor/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="/dashboard/vendor/flatpickr/flatpickr.min.js"></script>
    <script src="/dashboard/vendor/easy-pie-chart/jquery.easypiechart.min.js"></script>
    <script src="/dashboard/vendor/chart.js/Chart.min.js"></script>
    <!-- END PLUGINS JS -->

    <!-- BEGIN THEME JS -->
    <script src="/dashboard/javascript/theme.min.js"></script>
    <!-- END THEME JS -->

    <script>
        hljs.initHighlightingOnLoad();

        function highlightText(el)
        {
            if (document.selection) {
                var range = document.body.createTextRange();
                range.moveToElementText(el);
                range.select();
            } else if (window.getSelection) {
                var range = document.createRange();
                range.selectNodeContents(el);
                window.getSelection().removeAllRanges();
                window.getSelection().addRange(range);
            }
        }
    </script>
    @stack('script')
</body>
</html>
