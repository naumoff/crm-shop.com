<header class="app-header app-header-dark">
    <!-- .top-bar -->
    <div class="top-bar">

        <!-- .top-bar-brand -->
        <div class="top-bar-brand">
            <a href="/">
                {{--<img src="/dashboard/images/brand-inverse.png" alt="" style="height: 32px;width: auto;">--}}
                {{ config('app.name', 'Laravel') }}
            </a>
        </div>
        <!-- /.top-bar-brand -->

        <!-- .top-bar-list -->
        <div class="top-bar-list">
            @if(Route::currentRouteName() !== 'panel')
            <div class="top-bar-item px-2 d-md-none d-lg-none d-xl-none">
                <button class="hamburger hamburger-squeeze" type="button" data-toggle="aside" aria-label="toggle menu">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
            @endif

            <!-- .top-bar-item -->
            <div class="top-bar-item top-bar-item-right px-0 d-none d-sm-flex">
            @if(isset($headerNavItemPath) && $headerNavItemPath !== null)
                @include($headerNavItemPath)
            @endif
                <!-- .btn-account -->
                <div class="dropdown">
                    <button class="btn-account d-none d-md-flex" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="user-avatar user-avatar-md">
                            @guest
                                <i class="fa fa-user-secret" aria-hidden="true" style="vertical-align: top"></i>
                            @else
                                <i class="fa fa-user-circle" aria-hidden="true" style="vertical-align: top"></i>
                            @endif
                        </span>
                        <span class="account-summary pr-lg-4 d-none d-lg-block">
                            @guest
                                <span class="account-name">Guest</span>
                            @else
                                <span class="account-name">{{ Auth::user()->name }}</span>
                                <span class="account-description">{{ Auth::user()->roles->first()->role ?? 'no role yet' }}</span>
                            @endif
                        </span>
                    </button>
                    <div class="dropdown-arrow dropdown-arrow-left"></div>
                    <div class="dropdown-menu">
                        @guest
                            <a class="dropdown-item" href="{{ route('login') }}"><span class="dropdown-icon oi oi-account-logout"></span>Login</a>
                            @if (Route::has('register'))
                                <a class="dropdown-item" href="{{ route('register') }}"><span class="dropdown-icon oi oi-account-logout"></span>Register</a>
                            @endif
                        @else
                            <h6 class="dropdown-header d-none d-md-block d-lg-none">{{ Auth::user()->name }}</h6>
                            <a class="dropdown-item" href="#"><span class="dropdown-icon oi oi-person"></span>Profile</a>
                            <a class="dropdown-item"
                               href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                <span class="dropdown-icon oi oi-account-logout"></span>{{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                            <div class="dropdown-divider"></div>
                        @endif
                        <a class="dropdown-item" href="#">Help Center</a>
                        <a class="dropdown-item" href="#">Ask Forum</a>
                    </div>
                </div><!-- /.btn-account -->
            </div><!-- /.top-bar-item -->
        </div><!-- /.top-bar-list -->
    </div><!-- /.top-bar -->
</header>