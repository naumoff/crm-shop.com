@extends('dashboards.layouts.dashboard')

@section('content')
    <!-- .page -->
    <div class="page">
        <!-- .page-inner -->
        <div class="page-inner">
            <!-- .page-title-bar -->
            <header class="page-title-bar">
                <!-- page title stuff goes here -->
                <h1 class="page-title">Fields for '{{$pageModel->name}}' menu page</h1>
            </header>
            <!-- /.page-title-bar -->
            <!-- .page-section -->
            <div class="page-section">
                <!-- .section-deck -->
                <div class="section-deck">
                    <!-- .card -->
                    <section class="card card-fluid">
                        <!-- .card-header -->
                        <header class="card-header">
                            <!-- .nav-tabs -->
                            <ul class="nav nav-tabs card-header-tabs">
                                <li class="nav-item">
                                    <a class="nav-link active show" data-toggle="tab" href="#access">Access</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#header">Header</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#boolean">Boolean Fields</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#text">Text Fields</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#long-text">Long Text Fields</a>
                                </li>
                            </ul><!-- /.nav-tabs -->
                        </header><!-- /.card-header -->
                        <!-- .card-body -->
                        <div class="card-body">
                            <!-- .tab-content -->
                            <div id="myTabContent" class="tab-content">
                                <div class="tab-pane fade active show" id="access">
                                    @include('dashboards.cms.manager.inclusions.menu_page_access_form')
                                </div>
                                <div class="tab-pane fade" id="header">
                                    @include('dashboards.cms.manager.inclusions.menu_page_header_form')
                                </div>
                                <div class="tab-pane fade" id="boolean">
                                    @include('dashboards.cms.manager.inclusions.menu_page_boolean_form')
                                </div>
                                <div class="tab-pane fade" id="text">
                                    @include('dashboards.cms.manager.inclusions.menu_page_text_form')
                                </div>
                                <div class="tab-pane fade" id="long-text">
{{--                                    @include('')--}}
                                </div>
                            </div><!-- /.tab-content -->
                        </div><!-- /.card-body -->
                    </section><!-- /.card -->
                </div><!-- /.section-deck -->
                <!-- page content goes here -->
            </div>
            <!-- /.page-section -->
        </div>
        <!-- /.page-inner -->
    </div>
@endsection


