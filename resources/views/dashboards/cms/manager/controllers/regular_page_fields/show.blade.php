@extends('dashboards.layouts.dashboard')

@section('content')
    <div class="page">
        <div class="page-inner">
            <header class="page-title-bar">
                <h1 class="page-title">Pages list of type '{{ $pageType->type }}'</h1>
            </header>
            <div class="page-section">
                <div class="section-deck">
                    <section class="card card-fluid">
                        <div class="col-xl-8">
                            <header class="card-header border-0">
                                <div class="d-flex align-items-center">
                                    <span class="mr-auto">Content Management table to {{ $pageType->type }} page:</span>
                                    <div class="dropdown">
                                        <button type="button" class="btn btn-icon btn-light" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
                                        <div class="dropdown-arrow"></div>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="#!" class="dropdown-item">Add new {{ $pageType->type }} page</a>
                                        </div>
                                    </div>
                                </div>
                            </header>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="thead-">
                                    <tr>
                                        <th> Page Id </th>
                                        <th> Page Name </th>
                                        <th> Show / Hide </th>
                                        <th style="width:100px; min-width:100px;"> &nbsp; </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($pageType->pages as $page)
                                        <tr>
                                            <td> {{ $page->id }} </td>
                                            <td> {{ $page->name }} </td>
                                            <td>
                                                <div class="list-group-item d-flex justify-content-between align-items-center">
                                                    <span>Show / Hide</span> <!-- .switcher-control -->
                                                    <label class="switcher-control switcher-control-success">
                                                        <input
                                                                type="checkbox"
                                                                class="switcher-input menu-page-access"
                                                                checked
                                                        >
                                                        <span class="switcher-indicator"></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td class="align-middle text-right">
                                                <a href="{{ route('regular-page-fields-form', ['pageTypeName' => $pageType->type, 'page' => $page->name]) }}" class="btn btn-sm btn-icon btn-secondary">
                                                    <i class="fa fa-pencil-alt"></i><span class="sr-only">Edit</span>
                                                </a>
                                                <a href="#!" class="btn btn-sm btn-icon btn-secondary">
                                                    <i class="far fa-trash-alt"></i> <span class="sr-only">Remove</span>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection


