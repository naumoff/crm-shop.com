<div class="row">
    <div class="col-xl-8">
        <section class="card card-fluid">
            <!-- .card-header -->
            <header class="card-header border-0">
                <div class="d-flex align-items-center">
                    <span class="mr-auto">Access Management table to {{ ucfirst($pageModel->name) }} page:</span>
                    <div class="dropdown">
                        <button type="button" class="btn btn-icon btn-light" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
                        <div class="dropdown-arrow"></div>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="#!" class="dropdown-item">Edit</a> <a href="#!" class="dropdown-item">Archive</a> <a href="#!" class="dropdown-item">Remove</a>
                        </div>
                    </div>
                </div>
            </header><!-- /.card-header -->
            <!-- .table-responsive -->
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead class="thead-">
                        <tr>
                            <th> User's Role </th>
                            <th> Description </th>
                            <th> Show / Hide </th>
                            <th style="width:100px; min-width:100px;"> &nbsp; </th>
                        </tr>
                    </thead>
                    <tbody>
                    @inject('accessGateTableService', 'App\Contracts\SitePageAccessTableContract')
                    @foreach($accessGateTableService->convertModelToArray($pageModel->page_access_gate) as $accessRightObject)
                        <tr>
                            <td> {{ $accessRightObject->role }} </td>
                            <td> {{ $accessRightObject->description }} </td>
                            <td>
                                <div class="list-group-item d-flex justify-content-between align-items-center">
                                    <span>Show / Hide</span> <!-- .switcher-control -->
                                    <label class="switcher-control switcher-control-success">
                                        <input
                                                type="checkbox"
                                                class="switcher-input menu-page-access"
                                                data-access-gate-id="{{ $accessRightObject->id }}"
                                                data-access-role-name="{{ $accessRightObject->role }}"
                                                data-menu-page-id="{{ $pageModel->id }}"
                                                {{ ($accessRightObject->value)? 'checked':null }}
                                        >
                                        <span class="switcher-indicator"></span>
                                    </label>
                                </div>
                            </td>
                            <td class="align-middle text-right">
                                <a href="#!" class="btn btn-sm btn-icon btn-secondary">
                                    <i class="fa fa-pencil-alt"></i><span class="sr-only">Edit</span>
                                </a>
                                <a href="#!" class="btn btn-sm btn-icon btn-secondary">
                                    <i class="far fa-trash-alt"></i> <span class="sr-only">Remove</span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </section>
    </div>
</div>

@push('script')
    <script>
        $(document).ready(function(){
            $(".menu-page-access").change(function(){
                let accessGateId = $(this).data('access-gate-id');
                let accessRoleName = $(this).data('access-role-name');
                let pageId = $(this).data('menu-page-id');
                $.post
                (
                    '/cms-manager/menu-page-access/' + accessGateId,
                    {
                        "_token": "{{ csrf_token() }}",
                        "_method": "PATCH",
                        "role": accessRoleName,
                        "page_id": pageId
                    },
                    function(data){
                        console.log(data);
                    }
                )
            })
        })
    </script>
@endpush