<section id="labels" class="card">
    @include('dashboards.inclusions.errors')
    @include('dashboards.inclusions.success')
    <div class="card-body">
        <form method="post" action="{{route('menu-page-metadata-title-patch', ['page' => $pageModel->id])}}">
            <input type="hidden" name="_method" value="PATCH">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="page_id" value="{{ $pageModel->id }}">
            <input type="hidden" name="page_type_id" value="{{ $pageModel->page_type_id }}">
            <fieldset>
                <legend>Page Title</legend>
                @foreach(\App\Contracts\LocaleContract::AVAILABLE_LOCALES as $key => $locale)
                    @if ($key === 'primary')
                        <div class="form-group">
                            <label for="lbl2">Title for locale <b>{{$locale}}</b> <span class="badge badge-danger">Required</span></label>
                            <input
                                value="{{$pageModel->metadata_field->{'title_'.$locale} ?? old("title_".$locale) ?? null}}"
                                type="text"
                                name="title_{{$locale}}"
                                class="form-control"
                                id="lbl2"
                                placeholder="Required title"
                                required="required"
                            >
                        </div>
                    @else
                        <div class="form-group">
                            <label for="lbl2">Title for locale <b>{{$locale}}</b> <span class="badge badge-secondary"><em>Optional</em></span></label>
                            <input
                                value="{{$pageModel->metadata_field->{'title_'.$locale} ?? old("title_".$locale) ?? null}}"
                                type="text"
                                name="title_{{$locale}}"
                                class="form-control"
                                id="lbl2"
                                placeholder="Optional title"
                            >
                        </div>
                    @endif
                @endforeach
            </fieldset>
            <div class="el-example">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
    </div>
</section>