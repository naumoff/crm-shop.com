<section id="labels" class="card">
    <!-- grid column -->
    <div>
        <div class="alert alert-warning has-icon" role="alert">
            <div class="alert-icon">
                <span class="fa fa-bullhorn"></span>
            </div>Headers for menu pages will go to menu pages names!!!
        </div>
    </div><!-- /grid column -->
    <!-- grid column -->
    @include('dashboards.inclusions.errors')
    @include('dashboards.inclusions.success')
    <div class="card-body col-xl-8">
        <form method="post" action="{{route('menu-page-metadata-header-patch', ['page' => $pageModel->id])}}">
            <input type="hidden" name="_method" value="PATCH">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="page_id" value="{{ $pageModel->id }}">
            <input type="hidden" name="page_type_id" value="{{ $pageModel->page_type_id }}">
            <fieldset>
                <legend>Page Header</legend>
                @foreach(\App\Contracts\LocaleContract::AVAILABLE_LOCALES as $key => $locale)
                    <div class="form-group">
                        <label for="lbl2">Header for locale <b>{{$locale}}</b> <span class="badge badge-danger">Required</span></label>
                        <input
                            value="{{$pageModel->metadata_field->{'header_'.$locale} ?? old("header_".$locale) ?? null}}"
                            type="text"
                            name="header_{{$locale}}"
                            class="form-control"
                            id="lbl2"
                            placeholder="Required header"
                            required="required"
                        >
                    </div>
                @endforeach
            </fieldset>
            <div class="el-example">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
    </div>
</section>