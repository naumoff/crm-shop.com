<div class="row">
    <!-- grid column -->
    <div class="col-xl-8">
        <!-- .card -->
        <section class="card card-fluid">
            <!-- .card-header -->
            <header class="card-header border-0">
                <div class="d-flex align-items-center">
                    <span class="mr-auto">Switchers:</span>
                    <div class="dropdown">
                        <button type="button" class="btn btn-icon btn-light" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
                        <div class="dropdown-arrow"></div>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="#!" class="dropdown-item">Add New Boolean Value</a>
                            {{--<a href="#!" class="dropdown-item">Archive</a>--}}
                            {{--<a href="#!" class="dropdown-item">Remove</a>--}}
                        </div>
                    </div>
                </div>
            </header><!-- /.card-header -->
            <!-- .table-responsive -->
            <div class="table-responsive">
                <!-- .table -->
                <table class="table table-hover">
                    <!-- thead -->
                    <thead class="thead-">
                        <tr>
                            <th> id </th>
                            <th> Variable </th>
                            <th> Description </th>
                            <th> Value </th>
                            <th style="width:100px; min-width:100px;"> &nbsp; </th>
                        </tr>
                    </thead><!-- /thead -->
                    <!-- tbody -->
                    <tbody>
                        @foreach($pageModel->boolean_fields as $boolean)
                        <tr>
                            <td> {{ $boolean->id }} </td>
                            <td> {{ $boolean->name }} </td>
                            <td> {{ $boolean->description }} </td>
                            <td>
                                <!-- .list-group-item -->
                                <div class="list-group-item d-flex justify-content-between align-items-center">
                                    <span>Show / Hide</span> <!-- .switcher-control -->
                                    <label class="switcher-control switcher-control-success">
                                        <input
                                            type="checkbox"
                                            class="switcher-input menu-page-switcher"
                                            data-boolean-id="{{ $boolean->id }}"
                                            {{ ($boolean->value)? 'checked':null }}>
                                        <span class="switcher-indicator"></span>
                                    </label> <!-- /.switcher-control -->
                                </div><!-- /.list-group-item -->
                            </td>
                            <td class="align-middle text-right">
                                <a href="#!" class="btn btn-sm btn-icon btn-secondary">
                                    <i class="fa fa-pencil-alt"></i><span class="sr-only">Edit</span>
                                </a>
                                <a href="#!" class="btn btn-sm btn-icon btn-secondary">
                                    <i class="far fa-trash-alt"></i> <span class="sr-only">Remove</span>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody><!-- /tbody -->
                </table><!-- /.table -->
            </div><!-- /.table-responsive -->
        </section><!-- /.card -->
    </div><!-- /grid column -->
</div>
@push('script')
    <script>
        $(document).ready(function(){
            $(".menu-page-switcher").change(function(){
                let booleanField = $(this).data('boolean-id');
                $.post
                (
                    '/cms-manager/menu-page-fields-booleans/'+booleanField,
                    {
                        "_token": "{{ csrf_token() }}",
                        "_method": "PATCH",
                    },
                    function(data){
                        console.log(data);
                    }
                )
            })
        })
    </script>
@endpush


