<div class="row">
    <!-- grid column -->
    <div class="col-xl-6">
        <!-- .card -->
        <section class="card card-fluid">
            <!-- .card-header -->
            <header class="card-header border-0">
                <div class="d-flex align-items-center">
                    <span class="mr-auto">Hoverable</span>
                    <div class="dropdown">
                        <button type="button" class="btn btn-icon btn-light" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
                        <div class="dropdown-arrow"></div>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="#!" class="dropdown-item">Edit</a> <a href="#!" class="dropdown-item">Archive</a> <a href="#!" class="dropdown-item">Remove</a>
                        </div>
                    </div>
                </div>
            </header><!-- /.card-header -->
            <!-- .table-responsive -->
            <div class="table-responsive">
                <!-- .table -->
                <table class="table table-hover">
                    <!-- thead -->
                    <thead class="thead-">
                    <tr>
                        <th style="min-width:200px"> Product </th>
                        <th> Variants </th>
                        <th> Prices </th>
                    </tr>
                    </thead><!-- /thead -->
                    <!-- tbody -->
                    <tbody>
                    <!-- tr -->
                    <tr>
                        <td> Wine - Chateau Bonnet </td>
                        <td> 113 </td>
                        <td> $22.38 </td>
                    </tr><!-- /tr -->
                    <!-- tr -->
                    <tr>
                        <td> Cookie - Oatmeal </td>
                        <td> 216 </td>
                        <td> $21.90 </td>
                    </tr><!-- /tr -->
                    <!-- tr -->
                    <tr>
                        <td> True - Vue Containers </td>
                        <td> 191 </td>
                        <td> $24.96 </td>
                    </tr><!-- /tr -->
                    <!-- tr -->
                    <tr>
                        <td> Nut - Pumpkin Seeds </td>
                        <td> 329 </td>
                        <td> $32.21 </td>
                    </tr><!-- /tr -->
                    <!-- tr -->
                    <tr>
                        <td> Artichoke - Bottom, Canned </td>
                        <td> 375 </td>
                        <td> $27.75 </td>
                    </tr><!-- /tr -->
                    </tbody><!-- /tbody -->
                </table><!-- /.table -->
            </div><!-- /.table-responsive -->
        </section><!-- /.card -->
    </div><!-- /grid column -->
    <!-- grid column -->
    <div class="col-xl-6">
        <!-- .card -->
        <section class="card card-fluid">
            <!-- .card-header -->
            <header class="card-header border-0">
                <div class="d-flex align-items-center">
                    <span class="mr-auto">Striped rows</span>
                    <div class="dropdown">
                        <button type="button" class="btn btn-icon btn-light" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></button>
                        <div class="dropdown-arrow"></div>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="#!" class="dropdown-item">Edit</a> <a href="#!" class="dropdown-item">Archive</a> <a href="#!" class="dropdown-item">Remove</a>
                        </div>
                    </div>
                </div>
            </header><!-- /.card-header -->
            <!-- .table-responsive -->
            <div class="table-responsive">
                <!-- .table -->
                <table class="table table-striped">
                    <!-- thead -->
                    <thead class="thead-">
                    <tr>
                        <th style="min-width:200px"> Product </th>
                        <th> Variants </th>
                        <th> Prices </th>
                    </tr>
                    </thead><!-- /thead -->
                    <!-- tbody -->
                    <tbody>
                    <!-- tr -->
                    <tr>
                        <td> Wine - Chateau Bonnet </td>
                        <td> 113 </td>
                        <td> $22.38 </td>
                    </tr><!-- /tr -->
                    <!-- tr -->
                    <tr>
                        <td> Cookie - Oatmeal </td>
                        <td> 216 </td>
                        <td> $21.90 </td>
                    </tr><!-- /tr -->
                    <!-- tr -->
                    <tr>
                        <td> True - Vue Containers </td>
                        <td> 191 </td>
                        <td> $24.96 </td>
                    </tr><!-- /tr -->
                    <!-- tr -->
                    <tr>
                        <td> Nut - Pumpkin Seeds </td>
                        <td> 329 </td>
                        <td> $32.21 </td>
                    </tr><!-- /tr -->
                    <!-- tr -->
                    <tr>
                        <td> Artichoke - Bottom, Canned </td>
                        <td> 375 </td>
                        <td> $27.75 </td>
                    </tr><!-- /tr -->
                    </tbody><!-- /tbody -->
                </table><!-- /.table -->
            </div><!-- /.table-responsive -->
        </section><!-- /.card -->
    </div><!-- /grid column -->
    <!-- grid column -->
    <div class="col-xl-6">
        <!-- .card -->
        <section class="card card-fluid">
            <!-- .card-header -->
            <header class="card-header">
                <div class="d-flex align-items-center">
                    <span class="mr-auto">Bordered</span> <button type="button" class="btn btn-icon btn-light"><i class="fa fa-copy"></i></button> <button type="button" class="btn btn-icon btn-light"><i class="fa fa-download"></i></button>
                </div>
            </header><!-- /.card-header -->
            <!-- .table-responsive -->
            <div class="table-responsive">
                <!-- .table -->
                <table class="table table-bordered">
                    <!-- thead -->
                    <thead class="thead-">
                    <tr>
                        <th style="min-width:200px"> Product </th>
                        <th> Variants </th>
                        <th> Prices </th>
                    </tr>
                    </thead><!-- /thead -->
                    <!-- tbody -->
                    <tbody>
                    <!-- tr -->
                    <tr>
                        <td> Wine - Chateau Bonnet </td>
                        <td> 113 </td>
                        <td> $22.38 </td>
                    </tr><!-- /tr -->
                    <!-- tr -->
                    <tr>
                        <td> Cookie - Oatmeal </td>
                        <td> 216 </td>
                        <td> $21.90 </td>
                    </tr><!-- /tr -->
                    <!-- tr -->
                    <tr>
                        <td> True - Vue Containers </td>
                        <td> 191 </td>
                        <td> $24.96 </td>
                    </tr><!-- /tr -->
                    <!-- tr -->
                    <tr>
                        <td> Nut - Pumpkin Seeds </td>
                        <td> 329 </td>
                        <td> $32.21 </td>
                    </tr><!-- /tr -->
                    <!-- tr -->
                    <tr>
                        <td> Artichoke - Bottom, Canned </td>
                        <td> 375 </td>
                        <td> $27.75 </td>
                    </tr><!-- /tr -->
                    </tbody><!-- /tbody -->
                </table><!-- /.table -->
            </div><!-- /.table-responsive -->
        </section><!-- /.card -->
    </div><!-- /grid column -->
    <!-- grid column -->
    <div class="col-xl-6">
        <!-- .card -->
        <section class="card card-fluid">
            <!-- .card-header -->
            <header class="card-header border-0">
                <div class="d-flex align-items-center">
                    <span class="mr-auto">Condensed</span> <button type="button" class="btn btn-icon btn-light"><i class="fa fa-copy"></i></button> <button type="button" class="btn btn-icon btn-light"><i class="fa fa-download"></i></button>
                </div>
            </header><!-- /.card-header -->
            <!-- .table-responsive -->
            <div class="table-responsive">
                <!-- .table -->
                <table class="table table-sm mb-0">
                    <!-- thead -->
                    <thead class="thead-">
                    <tr>
                        <th style="min-width:200px"> Product </th>
                        <th> Variants </th>
                        <th> Prices </th>
                    </tr>
                    </thead><!-- /thead -->
                    <!-- tbody -->
                    <tbody>
                    <!-- tr -->
                    <tr>
                        <td> Wine - Chateau Bonnet </td>
                        <td> 113 </td>
                        <td> $22.38 </td>
                    </tr><!-- /tr -->
                    <!-- tr -->
                    <tr>
                        <td> Cookie - Oatmeal </td>
                        <td> 216 </td>
                        <td> $21.90 </td>
                    </tr><!-- /tr -->
                    <!-- tr -->
                    <tr>
                        <td> True - Vue Containers </td>
                        <td> 191 </td>
                        <td> $24.96 </td>
                    </tr><!-- /tr -->
                    <!-- tr -->
                    <tr>
                        <td> Nut - Pumpkin Seeds </td>
                        <td> 329 </td>
                        <td> $32.21 </td>
                    </tr><!-- /tr -->
                    <!-- tr -->
                    <tr>
                        <td> Artichoke - Bottom, Canned </td>
                        <td> 375 </td>
                        <td> $27.75 </td>
                    </tr><!-- /tr -->
                    <!-- tr -->
                    <tr>
                        <td> Sweet Pea Sprouts </td>
                        <td> 478 </td>
                        <td> $32.89 </td>
                    </tr><!-- /tr -->
                    <!-- tr -->
                    <tr>
                        <td> Spice - Paprika </td>
                        <td> 421 </td>
                        <td> $28.32 </td>
                    </tr><!-- /tr -->
                    <!-- tr -->
                    <tr>
                        <td> Tea - Lemon Scented </td>
                        <td> 371 </td>
                        <td> $32.83 </td>
                    </tr><!-- /tr -->
                    </tbody><!-- /tbody -->
                </table><!-- /.table -->
            </div><!-- /.table-responsive -->
        </section><!-- /.card -->
    </div><!-- /grid column -->
</div><!-- /grid row -->