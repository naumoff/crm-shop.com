<section id="labels" class="card">
    @include('dashboards.inclusions.errors')
    @include('dashboards.inclusions.success')
    <div class="card-body">
        <form method="post" action="{{route('menu-page-metadata-description-patch', ['page' => $pageModel->id])}}">
            <input type="hidden" name="_method" value="PATCH">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="page_id" value="{{ $pageModel->id }}">
            <input type="hidden" name="page_type_id" value="{{ $pageModel->page_type_id }}">
            <fieldset>
                <legend>Page Description</legend>
                @foreach(\App\Contracts\LocaleContract::AVAILABLE_LOCALES as $key => $locale)
                    @if ($key === 'primary')
                        <div class="form-group">
                            <label for="lbl2">Description for locale <b>{{$locale}}</b> <span class="badge badge-danger">Required</span></label>
                            <textarea
                                    rows="3"
                                    name="description_{{$locale}}"
                                    class="form-control"
                                    id="lbl3"
                                    placeholder="Required description"
                                    required="required"
                            >{{$pageModel->metadata_field->{'description_'.$locale} ?? old("description_".$locale) ?? null}}</textarea>
                        </div>
                    @else
                        <div class="form-group">
                            <label for="lbl2">Description for locale <b>{{$locale}}</b> <span class="badge badge-secondary"><em>Optional</em></span></label>
                            <textarea
                                    rows="3"
                                    name="description_{{$locale}}"
                                    class="form-control"
                                    id="lbl3"
                                    placeholder="Optional description"
                            >{{$pageModel->metadata_field->{'description_'.$locale} ?? old("description_".$locale) ?? null}}</textarea>
                        </div>
                    @endif
                @endforeach
            </fieldset>
            <div class="el-example">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
    </div>
</section>