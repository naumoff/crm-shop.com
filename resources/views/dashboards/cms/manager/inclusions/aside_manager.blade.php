<!-- .aside-content -->
<div class="aside-content">
    <!-- .aside-header -->
    @include('dashboards.cms.manager.inclusions.aside_manager_header')
    <!-- .aside-menu -->
    <section class="aside-menu overflow-hidden">
        <!-- .stacked-menu -->
        <nav id="stacked-menu" class="stacked-menu">
            <!-- .menu -->
            <ul class="menu">
                <!-- .menu-item -->
                <li class="menu-item has-active">
                    <a href="{{route('panel')}}" class="menu-link"><span class="menu-icon fa fa-home"></span> <span class="menu-text">My Dashboards</span></a>
                </li><!-- /.menu-item -->
                <li class="menu-header">SEO</li><!-- /.menu-header -->
                <!-- .menu-item -->
                <li class="menu-item has-child">
                    <a href="#" class="menu-link">
                        <span class="menu-icon oi oi-list-rich"></span>
                        <span class="menu-text">Menu Page SEO</span>
                    </a> <!-- child menu -->
                    <ul class="menu">
                        <ul class="menu">
                            @foreach($menuPages as $page)
                                <li class="menu-item">
                                    <a href="{{ route('menu-page-metadata-form', ['pageId' => $page->id] ) }}" class="menu-link">{{$page->name}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </ul>
                </li><!-- /.menu-item -->
                <!-- .menu-header -->
                <li class="menu-header">Page Constructor</li><!-- /.menu-header -->
                <!-- .menu-item -->
                <li class="menu-item has-child">
                    <a href="#" class="menu-link"><span class="menu-icon oi oi-list-rich"></span> <span class="menu-text">Menu Page Fields</span></a> <!-- child menu -->
                    <ul class="menu">
                        <ul class="menu">
                            @foreach($menuPages as $page)
                                <li class="menu-item">
                                    <a href="{{ route('menu-page-fields-form', ['pageId' => $page->id] ) }}" class="menu-link">{{$page->name}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </ul>
                </li><!-- /.menu-item -->
                <!-- .menu-item -->
                <li class="menu-item has-child">
                    <a href="#" class="menu-link"><span class="menu-icon oi oi-list-rich"></span> <span class="menu-text">Regular Page Fields</span></a> <!-- child menu -->
                    <ul class="menu">
                        <ul class="menu">
                            @foreach($regularPageTypes as $regularPageType)
                                <li class="menu-item">
                                    <a href="{{ route('page-type-pages', ['pageType' => $regularPageType->type] ) }}" class="menu-link">{{$regularPageType->type}}</a>
                                </li>
                            @endforeach
                        </ul>
                    </ul>
                </li><!-- /.menu-item -->
            </ul><!-- /.menu -->
        </nav><!-- /.stacked-menu -->
    </section><!-- /.aside-menu -->
    <!-- Skin changer -->
    <div class="aside-footer border-top p-3">
        <button class="btn btn-light btn-block" data-toggle="skin">Night mode <i class="fas fa-moon ml-1"></i></button>
    </div><!-- /Skin changer -->
</div><!-- /.aside-content -->