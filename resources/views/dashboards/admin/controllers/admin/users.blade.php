@extends('dashboards.layouts.dashboard')

@section('content')
<div class="page">
    <div class="page-inner">
        <header class="page-title-bar">
            <h1 class="page-title">Users' status: {{ $status }}</h1>
        </header>
        @include('dashboards.inclusions.success')
        <div class="page-section">
            <!-- .section-block -->
            <div class="section-block">
                <h2 class="section-title"> Users List </h2>
            </div><!-- /.section-block -->
            <!-- .card -->
            <div class="card card-fluid">
                <!-- .card-body -->
                <div class="card-body">
                    @include('dashboards.admin.inclusions.users_filter')
                    <div class="table-responsive">
                        <!-- .table -->
                        <table class="table">
                            <!-- thead -->
                            <thead>
                            <tr>
                                <th colspan="3" style="min-width: 240px">
                                    <div class="thead-dd dropdown">
                                        <span class="custom-control custom-control-nolabel custom-checkbox"><input type="checkbox" class="custom-control-input" id="check-handle2"> <label class="custom-control-label" for="check-handle2"></label></span>
                                        <div class="thead-btn" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="fa fa-caret-down"></span>
                                        </div>
                                        <div class="dropdown-arrow"></div>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#">Select all</a> <a class="dropdown-item" href="#">Unselect all</a>
                                            <div class="dropdown-divider"></div><a class="dropdown-item" href="#">Bulk remove</a> <a class="dropdown-item" href="#">Bulk edit</a> <a class="dropdown-item" href="#">Separate actions</a>
                                        </div>
                                    </div>
                                    &nbsp; User Name
                                </th>
                                <th> Email </th>
                                <th> Roles </th>
                                <th> Status </th>
                                <th style="width:100px; min-width:100px;">
                                    @if($status === 'deleted')
                                        Edit/Restore
                                    @else
                                        Edit/Delete
                                    @endif
                                </th>
                            </tr>
                            </thead><!-- /thead -->
                            <!-- tbody -->
                            <tbody>
                            @foreach($users as $user)
                                <!-- tr -->
                                <tr id="main-uid{{ $user->id }}">
                                    <td class="align-middle col-checker">
                                        <div class="custom-control custom-control-nolabel custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="uid{{ $user->id }}"> <label class="custom-control-label" for="uid{{ $user->id }}"></label>
                                        </div>
                                    </td>
                                    <td class="align-middle px-0" style="width: 1.5rem">
                                        <button type="button" class="btn btn-sm btn-icon btn-light" data-toggle="collapse" data-target="#details-uid{{ $user->id }}">
                                            <span class="collapse-indicator"><i class="fa fa-angle-right"></i></span>
                                        </button>
                                    </td>
                                    <td class="align-middle">
                                        <a href="#">{{ $user->name }}</a>
                                    </td>
                                    <td class="align-middle"> {{ $user->email }} </td>
                                    <td class="align-middle">
                                        @foreach($user->roles as $role)
                                            @if ($role->role === 'admin')
                                                <span class="badge badge-subtle badge-danger">{{ $role->role }}</span>
                                            @elseif (in_array($role->role, ['content-manager']))
                                                <span class="badge badge-subtle badge-warning">{{ $role->role }}</span>
                                            @else
                                                <span class="badge badge-subtle badge-success">{{ $role->role }}</span>
                                            @endif
                                        @endforeach
                                        @if (count($user->roles) === 0)
                                                <span class="badge badge-subtle badge-secondary">not settled</span>
                                        @endif
                                    </td>
                                    <td class="align-middle">
                                        @if($user->approved)
                                            <span class="badge badge-subtle badge-success">approved</span>
                                        @else
                                            <span class="badge badge-subtle badge-danger">not-approved</span>
                                        @endif

                                        @if (!empty($user->email_verified_at))
                                            <span class="badge badge-subtle badge-success">verified</span>
                                        @else
                                            <span class="badge badge-subtle badge-danger">not-verified</span>
                                        @endif

                                    </td>
                                    <td class="align-middle text-right">
                                        <a href="#"
                                           class="btn btn-sm btn-icon btn-secondary user-editor"
                                           data-toggle="modal"
                                           data-target="#userEditModal"
                                           data-user-id="{{ $user->id }}"
                                           data-user-name="{{ $user->name }}"
                                           data-user-email="{{ $user->email }}"
                                           data-user-phone="{{ $user->phone ?? null }}"
                                           data-user-location="{{ $user->location ?? null }}"
                                           data-user-address="{{ $user->address ?? null }}"
                                           data-user-roles="{{ json_encode($user->roles) }}"
                                           data-user-approved="{{ $user->approved ?? 0}}"
                                        >
                                            <i class="fa fa-pencil-alt"></i> <span class="sr-only">Edit</span>
                                        </a>
                                        @if ($status === 'deleted')
                                            <a href="#" class="btn btn-sm btn-icon btn-secondary user-recover" data-uid="{{ $user->id }}">
                                                <i class="fas fa-redo"></i> <span class="sr-only">Restore</span>
                                            </a>
                                        @else
                                            <a href="#" class="btn btn-sm btn-icon btn-secondary user-remover" data-uid="{{ $user->id }}">
                                                <i class="far fa-trash-alt"></i> <span class="sr-only">Remove</span>
                                            </a>
                                        @endif
                                    </td>
                                </tr><!-- /tr -->
                                <!-- tr -->
                                <tr class="row-details bg-light collapse" id="details-uid{{ $user->id }}">
                                    <td colspan="6">
                                        <!-- .row -->
                                        <div class="row">
                                            <!-- .col -->
                                            <div class="col-md-auto text-center">
                                                <div class="tile tile-xl tile-circle bg-purple mb-2"> {{$user->name[0]}} </div>
                                                <h3 class="card-title mb-4"> {{ $user->name }} </h3>
                                            </div><!-- /.col -->
                                            <!-- .col -->
                                            <div class="col-md-7">
                                                <table class="table table-bordered">
                                                    <tbody>
                                                    <tr>
                                                        <td> Contact </td>
                                                        <td> {{ $user->name }} </td>
                                                    </tr>
                                                    <tr>
                                                        <td> Email </td>
                                                        <td> {{ $user->email }} </td>
                                                    </tr>
                                                    <tr>
                                                        <td> Phone </td>
                                                        <td> {{ $user->phone ?? 'not defined' }} </td>
                                                    </tr>
                                                    <tr>
                                                        <td> Address </td>
                                                        <td> {{ $user->address ?? 'not defined' }} </td>
                                                    </tr>
                                                    <tr>
                                                        <td> Location </td>
                                                        <td> {{ $user->location ?? 'not defined' }} </td>
                                                    </tr>
                                                    <tr>
                                                        <td> Joined </td>
                                                        <td> {{ $user->created_at }} </td>
                                                    </tr>
                                                    <tr>
                                                        <td> Last login </td>
                                                        <td> {{ $user->last_login ?? 'never' }} </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div><!-- /.col -->
                                            <!-- .col -->
                                            <div class="col">
                                                <!-- .publisher -->
                                                <div class="publisher keep-focus focus">
                                                    <label for="publisherInput85743" class="publisher-label">Send a message to user</label> <!-- .publisher-input -->
                                                    <div class="publisher-input">
                                                        <textarea id="publisherInput85743" class="form-control" placeholder="Write a message"></textarea>
                                                    </div><!-- /.publisher-input -->
                                                    <!-- .publisher-actions -->
                                                    <div class="publisher-actions align-items-center">
                                                        <!-- .publisher-tools -->
                                                        <div class="publisher-tools mr-auto">
                                                            <div class="btn btn-light btn-icon fileinput-button">
                                                                <i class="fa fa-paperclip"></i> <input type="file" id="attachment85743[]" name="attachment85743[]" multiple>
                                                            </div><button type="button" class="btn btn-light btn-icon"><i class="far fa-smile"></i></button>
                                                        </div><!-- /.publisher-tools -->
                                                        <div class="custom-control custom-control-inline custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="copymsg5743" checked> <label class="custom-control-label" for="copymsg5743">Send me a copy</label>
                                                        </div><button type="submit" class="btn btn-primary">Send</button>
                                                    </div><!-- /.publisher-actions -->
                                                </div><!-- /.publisher -->
                                            </div><!-- /.col -->
                                        </div><!-- /.row -->
                                    </td>
                                </tr><!-- /tr -->
                                <!-- tr -->
                            @endforeach
                            </tbody><!-- /tbody -->
                        </table><!-- /.table -->
                    </div><!-- /.table-responsive -->
                </div><!-- /.card-body -->
            </div><!-- /.card -->
            {{ $users->links() }}
        </div>
    </div>
</div>
@include('dashboards.admin.inclusions.user_edit_modal')

@endsection
@push('script')
    <script>
        $(document).ready(function(){

            $(".error-block").hide();

            $(".user-remover").on('click', function(){
                let userId = $(this).data('uid');
                $.post
                (
                    '/admin/users/' + userId,
                    {
                        userId: userId,
                        _token: "{{ csrf_token() }}",
                        _method: "DELETE",
                    },
                    function (userId) {
                        $("#main-uid" + userId).hide();
                        $("#details-uid" + userId).hide();
                    }
                )
            });

            $(".user-recover").on('click', function(){
                let userId = $(this).data('uid');
                $.post
                (
                    '/admin/users/' + userId + '/restore',
                    {
                        userId: userId,
                        _token: "{{ csrf_token() }}",
                        _method: "PATCH",
                    },
                    function (userId) {
                        $("#main-uid" + userId).hide();
                        $("#details-uid" + userId).hide();
                    }
                )
            });

            let editedUserId = false;

            $(".user-editor").on('click', function() {

                editedUserId = $(this).data('user-id');

                let userName = $(this).data('user-name');
                $('#userEditTitle').text(userName);
                $('#userName').val(userName);

                let userEmail = $(this).data('user-email');
                $('#userEmail').val(userEmail);

                let userPhone = $(this).data('user-phone');
                $('#userPhone').val(userPhone);

                let userLocation = $(this).data('user-location');
                $('#userLocation').val(userLocation);

                let userAddress = $(this).data('user-address');
                $('#userAddress').val(userAddress);

                let userRoles = $(this).data('user-roles');

                $(".user-roles").prop('checked', false);
                for (let role of userRoles) {
                    $('#role-' + role.id).prop('checked', true);
                }

                let userApproved = $(this).data('user-approved');
                if (userApproved === 1) {
                    $("#user-approved").prop('checked', true);
                } else {
                    $("#user-approved").prop('checked', false);
                }
            });

            $(".save-user").on('click', function (event) {
                event.preventDefault();

                $(".error-block").hide();
                $('.error-list').empty();

                let userName = $("#userName").val();
                let userEmail = $("#userEmail").val();
                let userPhone = $("#userPhone").val();
                let userLocation = $("#userLocation").val();
                let userAddress = $("#userAddress").val();
                let userRoles = $('.user-roles:checkbox:checked').map(function(){
                    return this.value;
                }).get();

                let userApproved = $('#user-approved').is(':checked');

                $.ajax({
                    type: 'post',
                    url: '/admin/users/' + editedUserId,
                    data: {
                        _token: "{{ csrf_token() }}",
                        _method: "PATCH",
                        userId: editedUserId,
                        userName: userName,
                        userEmail: userEmail,
                        userPhone: userPhone,
                        userLocation: userLocation,
                        userAddress: userAddress,
                        userRoles: userRoles,
                        userApproved: userApproved
                    },
                    dataType: 'json',
                    success: function (data) {
                        if (data.status === 200) {
                            location.reload();
                        }
                    },
                    error: function (data) {
                        let errors = data.responseJSON;
                        let errorsContainers = Object.keys(errors.errors);
                        for (let errorProperty of errorsContainers) {
                            let error = '<li>' + errors.errors[errorProperty][0] + '</li>';
                            $(".error-list").append(error);
                        }
                        $('.error-block').show();
                    }
                });
            })

        })
    </script>
@endpush