@extends('dashboards.layouts.dashboard')

@section('content')
    <div class="page">
        <div class="page-inner">
            <header class="page-title-bar">
                <h1 class="page-title">Invite User Menu</h1>
            </header>
            @include('dashboards.inclusions.errors')
            @include('dashboards.inclusions.success')
            <div class="page-section">
                <div id="selects" class="card">
                    <div class="col-xl-8">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="card-title"> User's Invitation Form </h3>
                                <form class="needs-validation" novalidate="" method="post" action="{{ route('user-create') }}">
                                    @csrf
                                    <div class="form-row">
                                        <div class="col-md-6 mb-3">
                                            <label for="validationTooltip01">User name <abbr title="Required">*</abbr></label>
                                            <input type="text" name="userName" class="form-control" id="validationTooltip01" placeholder="User's Name" value="{{ old('name') }}" required="">
                                            <div class="valid-tooltip"> Looks good! </div>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="validationTooltip02">User email <abbr title="Required">*</abbr></label>
                                            <input type="email" name="userEmail" class="form-control" id="validationTooltip02" placeholder="User's Email" value="{{ old('email') }}" required="">
                                            <div class="valid-tooltip"> Looks good! </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group">
                                            <label>New User Roles:</label>
                                            @foreach($roles as $roleId => $role)
                                                <div class="custom-control custom-checkbox mb-1">
                                                    <input type="checkbox" name="{{ $role }}" class="custom-control-input" id="ckb{{$roleId}}" value={{ $roleId }}>
                                                    <label class="custom-control-label" for="ckb{{$roleId}}">
                                                        {{ $role }}
                                                    </label>
                                                    <div class="text-muted"></div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <button class="btn btn-primary" type="submit">Invite New User</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection