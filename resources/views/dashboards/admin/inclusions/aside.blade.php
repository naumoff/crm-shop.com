<!-- .aside-content -->
<div class="aside-content">
    <!-- .aside-header -->
    @include('dashboards.admin.inclusions.aside_header')
    <!-- .aside-menu -->
    <section class="aside-menu overflow-hidden">
        <!-- .stacked-menu -->
        <nav id="stacked-menu" class="stacked-menu">
            <!-- .menu -->
            <ul class="menu">
                <!-- .menu-item -->
                <li class="menu-item has-active">
                    <a href="{{route('panel')}}" class="menu-link"><span class="menu-icon fa fa-home"></span> <span class="menu-text">My Dashboards</span></a>
                </li><!-- /.menu-item -->

                <!-- .menu-header -->
                <li class="menu-header">User Management </li><!-- /.menu-header -->
                <!-- .menu-item -->
                <li class="menu-item has-child">
                    <a href="#" class="menu-link"><span class="menu-icon oi oi-person"></span> <span class="menu-text">Users</span></a> <!-- child menu -->
                    <ul class="menu">
                        <li class="menu-item">
                            <hr>
                            <a href="{{ route('users', ['userStatus' => ltrim(\App\Contracts\ModelObserverContract::USER_ALL, 'user:')]) }}" class="menu-link">All <span class="badge badge-warning">{{ $counter['all'] }}</span></a>
                        </li>
                        @inject('stringConverter', "App\Services\ViewHelpers\StringConverterService")
                        @foreach($roles as $roleId => $roleName)
                        <li class="menu-item">
                            <a href="{{ route('users', ['userStatus' => $roleName]) }}" class="menu-link">{{ ucfirst($stringConverter->convertDashesToWhiteSpace($roleName)) }} <span class="badge badge-warning">{{ $counter[$roleName] ?? 0 }}</span></a>
                        </li>
                        @endforeach
                        <li class="menu-item">
                            <a href="{{ route('users', ['userStatus' => ltrim(\App\Contracts\ModelObserverContract::USER_WITHOUT_ROLE, 'user:')]) }}" class="menu-link">Without Role <span class="badge badge-warning">{{ $counter['without-role'] ?? 0 }}</span></a>
                        </li>
                        <li class="menu-item">
                            <a href="{{ route('users', ['userStatus' => ltrim(\App\Contracts\ModelObserverContract::USER_NOT_APPROVED, 'user:')]) }}" class="menu-link">Waiting for approval <span class="badge badge-warning">{{ $counter['not-approved'] ?? 0 }}</span></a>
                        </li>
                        <li class="menu-item">
                            <a href="{{ route('users', ['userStatus' => ltrim(\App\Contracts\ModelObserverContract::USER_NOT_VERIFIED, 'user:')]) }}" class="menu-link">Unverified <span class="badge badge-warning">{{ $counter['not-verified'] ?? 0 }}</span></a>
                        </li>
                        <li class="menu-item">
                            <a href="{{ route('users', ['userStatus' => ltrim(\App\Contracts\ModelObserverContract::USER_DELETED, 'user:')]) }}" class="menu-link">Deleted <span class="badge badge-warning">{{ $counter['deleted'] ?? 0 }}</span></a>
                        </li>
                        <li class="menu-item">
                            <hr>
                            <a href="{{ route('user-invite') }}" class="btn btn-block btn-info" role="button">Invite New User</a>
                        </li>
                    </ul><!-- /child menu -->
                </li><!-- /.menu-item -->
                <!-- .menu-header -->
            </ul><!-- /.menu -->
        </nav><!-- /.stacked-menu -->
    </section><!-- /.aside-menu -->
    <!-- Skin changer -->
    <footer class="aside-footer border-top p-3">
        <button class="btn btn-light btn-block text-primary" data-toggle="skin">Night mode <i class="fas fa-moon ml-1"></i></button>
    </footer>
    <!-- /Skin changer -->
</div><!-- /.aside-content -->
