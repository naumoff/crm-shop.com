<!-- .form-group -->
<div class="form-group">
    <!-- .input-group -->
    <div class="input-group input-group-alt">
        <!-- .input-group-prepend -->
        <div class="input-group-prepend">
            <select class="custom-select" id="user-search-type">
                <option selected> Filter By </option>
                <option value="all"> All </option>
                <option value="email"> Email </option>
                <option value="name"> Name </option>
            </select>
        </div><!-- /.input-group-prepend -->
        <!-- .input-group -->
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">
                    <span class="oi oi-magnifying-glass"></span>
                </span>
            </div>
            <input type="text" id="user-search-input" class="form-control" placeholder="Search record" name="search">
        </div><!-- /.input-group -->
    </div><!-- /.input-group -->
</div><!-- /.form-group -->
<!-- .table-responsive -->
<div class="text-muted"> presented {{ count($users) }} users of {{$counter[$status] ?? 0 }} in total</div>

@push('script')
    <script>
        $(document).ready(function(){

            let searchType = '';
            let searchInput = '';

            function setGetParamsAndReloadPage(param, value)
            {
                let url = window.location.href;
                url = url.split('?')[0];
                if (url.indexOf('?') > -1){
                    url += '&'+param+'='+value;
                } else {
                    url += '?'+param+'='+value;
                }
                window.location.href = url;
            }

            function cleanGetParamsAndReloadPage()
            {
                let url = window.location.href;
                url = url.split('?')[0];
                window.location.href = url;
            }

            $("#user-search-type").on('change', function () {
                searchType = $("#user-search-type option:selected").val();
                if (searchInput.length > 0 && searchType.length > 0) {
                    if (searchType === 'email' || searchType === 'name') {
                        setGetParamsAndReloadPage(searchType, searchInput)
                    }
                } else if (searchType === 'all') {
                    cleanGetParamsAndReloadPage();
                }
            });

            $("#user-search-input").on('change', function () {
                searchInput = $("#user-search-input").val();
                if (searchType.length > 0 && searchInput.length > 0) {
                    if (searchType === 'email' || searchType === 'name') {
                        setGetParamsAndReloadPage(searchType, searchInput)
                    }
                }
            });

        })
    </script>
@endpush