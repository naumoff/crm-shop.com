<!DOCTYPE html>
<html class="wide wow-animation" lang="{{ App::getLocale() }}">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-NFJFZ4K');</script>
    <!-- End Google Tag Manager -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-143944062-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-143944062-1');
    </script>

    <!-- Site Title-->
    <title>{{ (!empty($seo->title))? $seo->title : env('APP_NAME') }}</title>
    <meta name="description" content="{{ (!empty($seo->description))? $seo->description : null }}">
    <meta name="keywords" content="{{ (!empty($seo->keywords))? $seo->keywords : null }}">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">

    {{--SITE TEMPLATE STYLING--}}
    <!-- Favicon and Touch Icons-->
    <link rel="apple-touch-icon" sizes="180x180" href="/site/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/site/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/site/favicon-16x16.png">
    <link rel="manifest" href="/site/site.webmanifest">
    <link rel="mask-icon" color="#343b43" href="/site/safari-pinned-tab.svg">
    <meta name="msapplication-TileColor" content="#603cba">
    <meta name="theme-color" content="#ffffff">
    <!-- Vendor Styles including: Font Icons, Plugins, etc.-->
    <link rel="stylesheet" media="screen" href="/site/css/vendor.min.css">
    <!-- Main Theme Styles + Bootstrap-->
    <link rel="stylesheet" media="screen" href="/site/css/theme.min.css">
    <!-- Modernizr-->
    <script src="/site/js/modernizr.min.js"></script>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NFJFZ4K" height="0" width="0" style="display:none;visibility:hidden">
        </iframe>
    </noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- Page Loader-->
{{--@include('site.inclusions.page_loader')--}}
<!-- Page-->

    <!-- Page Header-->
    @include('site.inclusions.header')

    <!-- Content -->
    @yield('content')

    <!-- Page Footer-->
    @include('site.inclusions.footer')

<!-- Global Mailform Output-->
{{--<div class="snackbars" id="form-output-global"></div>--}}
<!-- Javascript-->
<script src="{{ url('/site/js/vendor.min.js') }}"></script>
<script src="{{ url('/site/js/theme.min.js') }}"></script>
@stack('js-scripts')
</body>
</html>
