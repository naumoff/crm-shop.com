<div class="offcanvas-container is-triggered offcanvas-container-reverse" id="mobile-menu"><span class="offcanvas-close"><i class="fe-icon-x"></i></span>
    <div class="px-4 pb-4">
        <h6>@lang('heads.menu')</h6>
        <div class="d-flex justify-content-between pt-2">
            <div class="btn-group w-100 mr-2">
                <a class="btn btn-secondary btn-sm btn-block dropdown-toggle" href="#" data-toggle="dropdown">
                    @if(App::getLocale() == 'en')
                        <img src="/site/img/flags/en.png" alt="English"/>
                        English
                    @elseif(App::getLocale() == 'ru')
                        <img src="/site/img/flags/ru.png" alt="Russian"/>
                        Русский
                    @elseif(App::getLocale() == 'ua')
                        <img src="/site/img/flags/ua.png" alt="Ukrainian"/>
                        Українська
                    @endif
                </a>
                <div class="dropdown-menu" style="min-width: 150px;">
                    <a href="{{route(\Route::currentRouteName(), !empty($pageName)? ['locale' => 'en', 'name' => $pageName]:['locale' => 'en'])}}" class="dropdown-item" {{(App::getLocale() == 'en')? 'disabled': null}}>
                        <img src="/site/img/flags/en.png" alt="English"/> English
                    </a>
                    <a href="{{route(\Route::currentRouteName(), !empty($pageName)? ['locale' => 'ru', 'name' => $pageName]:['locale' => 'ru'])}}" class="dropdown-item" {{(App::getLocale() == 'ru')? 'disabled': null}}>
                        <img src="/site/img/flags/ru.png" alt="Russian"/> Русский
                    </a>
                    <a href="{{route(\Route::currentRouteName(), !empty($pageName)? ['locale' => 'ua', 'name' => $pageName]:['locale' => 'ua'])}}" class="dropdown-item" {{(App::getLocale() == 'ua')? 'disabled': null}}>
                        <img src="/site/img/flags/ua.png" alt="Ukraine"/> Українська
                    </a>
                </div>
            </div>
            @auth
                <a class="btn btn-primary btn-sm btn-block" href="{{ route('panel') }}">
                    <i class="fe-icon-user"></i>&nbsp;@lang('link_names.dashboard')
                </a>
            @else
                <a class="btn btn-primary btn-sm btn-block" href="{{ route('login') }}">
                    <i class="fe-icon-user"></i>&nbsp;@lang('link_names.login')
                </a>
            @endif
        </div>
    </div>
    <div class="offcanvas-scrollable-area border-top" style="height:calc(100% - 235px); top: 144px;">
        <!-- Mobile Menu-->
        <div class="accordion mobile-menu" id="accordion-menu">
            <!-- MAIN -->
            @can(\App\Contracts\SitePageAccessContract::VIEW_MENU_PAGE, \App\Models\PageModels\Page::MAIN)
                <div class="card">
                    <div class="card-header">
                        <a class="mobile-menu-link" href="{{route('main', ['locale'=>App::getLocale()])}}">{{$menuPageNames->main ?? __('routes.main')}}</a>
                    </div>
                </div>
            @endcan
            <!-- Contacts-->
            @can(\App\Contracts\SitePageAccessContract::VIEW_MENU_PAGE, \App\Models\PageModels\Page::CONTACTS)
                <div class="card">
                    <div class="card-header">
                        <a class="mobile-menu-link" href="{{route('contacts', ['locale'=>App::getLocale()])}}">{{$menuPageNames->contacts ?? __('routes.contacts')}}</a>
                    </div>
                </div>
            @endcan
        </div>
    </div>
    <div class="offcanvas-footer px-4 pt-3 pb-2 text-center">
        <a class="social-btn sb-style-3 sb-facebook" href="https://www.facebook.com/naumoff.andrey" target="_blank">
            <i class="socicon-facebook"></i>
        </a>
        <a class="social-btn sb-style-3 sb-linkedin" href="https://www.linkedin.com/in/andrey-naumoff-223736150/" target="_blank">
            <i class="socicon-linkedin"></i>
        </a>
        <a class="social-btn sb-style-3 sb-github" href="https://gitlab.com/naumoff" target="_blank">
            <i class="socicon-github"></i>
        </a>
    </div>
</div>
