@if($flash = session('message'))
    <div class="col-lg-12">
        <div class="alert alert-success alert-dismissible fade show">
            <button type="button" class="close" data-dismiss="alert">×</button>
            {{$flash}}
        </div>
    </div>
@endif