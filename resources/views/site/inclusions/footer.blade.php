<!-- Footer-->
<footer class="bg-dark pt-5">
    <div class="container pt-2">
        <div class="row">
            <div class="col-lg-3 pb-4 mb-2">
                <a class="navbar-brand d-inline-block mb-4" href="{{ route('main', ['locale' => App::getLocale()]) }}">
                    <img src="/site/img/logo/logo.png" alt="CreateX">
                </a>
                <p class="text-sm text-white opacity-50" style="text-align: justify">
                    @lang('strings.call_to_action')
                </p>
                <ul class="list-icon text-sm pb-2">
                    <li><i class="fe-icon-map-pin text-white opacity-60"></i>
                        <a class="navi-link text-white" href="#">
                            &nbsp;@lang('strings.address-location')
                        </a>
                    </li>
                    <li>
                        <i class="fe-icon-phone text-white opacity-60"></i>
                        <a class="navi-link text-white" href="tel:+380952680707">
                            &nbsp;+38 095 268 0707 (viber/telegram)
                        </a>
                    </li>
                    <li>
                        <i class="fe-icon-phone text-white opacity-60"></i>
                        <a class="navi-link text-white" href="tel:+380671410707">
                            &nbsp;+38 067 141 0707
                        </a>
                    </li>
                    <li>
                        <i class="fe-icon-mail text-white opacity-60"></i>
                        <a class="navi-link text-white" href="mailto:andrey.naumoff@crm-shop.com">
                            &nbsp;andrey.naumoff@crm-shop.com
                        </a>
                    </li>
                </ul>
                <a class="social-btn sb-style-6 sb-facebook sb-light-skin" href="https://www.facebook.com/naumoff.andrey" target="_blank">
                    <i class="socicon-facebook"></i>
                </a>
                <a class="social-btn sb-style-6 sb-linkedin sb-light-skin" href="https://www.linkedin.com/in/andrey-naumoff-223736150/" target="_blank">
                    <i class="socicon-linkedin"></i>
                </a>
                <a class="social-btn sb-style-6 sb-github sb-light-skin" href="https://gitlab.com/naumoff" target="_blank">
                    <i class="socicon-github"></i>
                </a>
            </div>
            <div class="col-lg-6">
                <div class="widget widget-light-skin mb-0">
                    <h4 class="widget-title">@lang('heads.menu')</h4>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="widget widget-categories widget-light-skin">
                            <ul>
                                <!-- MAIN -->
                                @can(\App\Contracts\SitePageAccessContract::VIEW_MENU_PAGE, \App\Models\PageModels\Page::MAIN)
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('main', ['locale'=>App::getLocale()])}}">{{$menuPageNames->main ?? __('routes.main')}}</a>
                                    </li>
                                @endcan
                                <li></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="widget widget-categories widget-light-skin">
                            <ul>
                                <!-- Contacts-->
                                @can(\App\Contracts\SitePageAccessContract::VIEW_MENU_PAGE, \App\Models\PageModels\Page::CONTACTS)
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('contacts', ['locale'=>App::getLocale()])}}">{{$menuPageNames->contacts ?? __('routes.contacts')}}</a>
                                    </li>
                                @endcan
                                <li></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="widget widget-light-skin">
                    <h4 class="widget-title">@lang('heads.my_projects')</h4>
                    <a class="market-btn market-btn-light-skin mr-3 mb-3 btn-block" href="#">
                        <span class="mb-subtitle">@lang('strings.coming_soon')</span>
                        <span class="mb-title">crm-base.com</span>
                    </a>
                    <a class="market-btn market-btn-light-skin mr-3 mb-3 btn-block" href="#">
                        <span class="mb-subtitle">@lang('strings.coming_soon')</span>
                        <span class="mb-title">coder-base.com</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- Subscription-->
    <div class="pt-5" style="background-color: #30363d;">
        <div class="container">
            <h6 class="text-white text-center">@lang('heads.subscribe')</h6>
            <div class="row justify-content-center pb-5">
                <div class="col-xl-6 col-lg-7 col-md-9">
                    {{--ERRORS--}}
                    <div class="col-lg-12 error-block" id="errors-subscribe-block" style="display: none">
                        <div class="alert alert-danger alert-dismissible fade show">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <strong>@lang('heads.validation_errors')</strong>
                            <ul class="error-list"></ul>
                        </div>
                    </div>
                    {{--SUCCESS--}}
                    <div class="col-lg-12 success-block" id="success-subscribe-block" style="display: none">
                        <div class="alert alert-success alert-dismissible fade show">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <strong>@lang('heads.successful_form_submit')</strong>
                            <ul class="success-list"></ul>
                        </div>
                    </div>
                    <form>
                        <div class="input-group">
                            <input class="form-control form-control-light-skin" type="email" name="email" placeholder="@lang('placeholders.enter-email')">
                            <div class="input-group-append">
                                <button class="btn btn-primary" id="subscribe" type="submit">@lang('buttons.send')</button>
                            </div>
                        </div>
                        <small class="form-text text-white opacity-50 pt-1 text-center">
                            @lang('strings.subscribe_help_text')
                        </small>
                        <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                        <div style="position: absolute; left: -5000px;" aria-hidden="true">
                            <input type="text" name="b_c7103e2c981361a6639545bd5_1194bb7544" tabindex="-1">
                        </div>
                    </form>
                </div>
            </div>
            <hr class="hr-light">
            <div class="d-md-flex justify-content-between align-items-center py-4 text-center text-md-left">
                {{--<div class="order-2"><a class="footer-link text-white" href="#">About</a><a class="footer-link text-white ml-3" href="#">Help &amp; Info</a><a class="footer-link text-white ml-3" href="#">Privacy Policy</a></div>--}}
                {{--<p class="m-0 text-sm text-white order-1"><span class='opacity-60'>© All rights reserved. Made with</span> <i class='d-inline-block align-middle fe-icon-heart text-danger'></i> <a href='http://createx.studio/' class='d-inline-block nav-link text-white opacity-60 p-0' target='_blank'>by Createx Studio</a></p>--}}
            </div>
        </div>
    </div>
</footer>
<!-- Back To Top Button--><a class="scroll-to-top-btn" href="#"><i class="fe-icon-chevron-up"></i></a>
<!-- Backdrop-->
<div class="site-backdrop"></div>
@push('js-scripts')
<script>
  $(document).ready(function(){
    $("#subscribe").click(function(event){
      event.preventDefault();
      $('.error-list').empty();
      $('.success-list').empty();
      let email = $('input[name="email"]').val();
      hideErrorBlock();
      hideSuccessBlock();

      $.ajax({
        type: 'post',
        url: '/{{App::getLocale()}}/subscriptions',
        data: {
          _token: "{{ csrf_token() }}",
          email: email,
        },
        dataType: 'json',
        success: function (data) {
          showSuccessBlock();
          let success = '<li>' + data.message + '</li>';
          $(".success-list").append(success);
          $('input[name="email"]').val('');
          dataLayer.push({'event': 'subscription-form-send'});
        },
        error: function (data) {
          if (data.status === 422) {
            showErrorBlock();
            let errors = data.responseJSON;
            let errorsContainers = Object.keys(errors.errors);
            for (let errorProperty of errorsContainers) {
              let error = '<li>' + errors.errors[errorProperty][0] + '</li>';
              $(".error-list").append(error);
            }
          } else {
            showErrorBlock();
            let error = '<li>' + '@lang('strings.subscribe_unknown_error_message')' + '</li>';
            $(".error-list").append(error);
          }
        },
      });
    })
  });

  function showErrorBlock() {
    document.getElementById("errors-subscribe-block").style.display = 'block';
  }

  function hideErrorBlock() {
    document.getElementById("errors-subscribe-block").style.display = 'none';
  }

  function showSuccessBlock() {
    document.getElementById("success-subscribe-block").style.display = 'block';
  }

  function hideSuccessBlock() {
    document.getElementById("success-subscribe-block").style.display = 'none';
  }
</script>
@endpush
