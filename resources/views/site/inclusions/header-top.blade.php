<div class="navbar-top d-none d-lg-flex justify-content-between align-items-center">
    <div>
        <a class="navbar-link mr-3" href="tel:+380952680707" style="color: white">
            <i class="fe-icon-phone"></i>+38 095 268 0707 (viber/telegram)
        </a>
        <a class="navbar-link mr-3" href="mailto:andrey.naumoff@crm-shop.com" style="color: white">
            <i class="fe-icon-mail"></i>andrey.naumoff@crm-shop.com
        </a>
        <a class="social-btn sb-style-3 sb-facebook" href="https://www.facebook.com/naumoff.andrey" target="_blank">
            <i class="socicon-facebook"></i>
        </a>
        <a class="social-btn sb-style-3 sb-linkedin" href="https://www.linkedin.com/in/andrey-naumoff-223736150/" target="_blank">
            <i class="socicon-linkedin"></i>
        </a>
        <a class="social-btn sb-style-3 sb-github" href="https://gitlab.com/naumoff" target="_blank">
            <i class="socicon-github"></i>
        </a>
    </div>
    <div>
        <ul class="list-inline mb-0">
            <li class="dropdown-toggle mr-2">
                @auth
                    <a href="{{ route('panel') }}" class="navbar-link" style="color: white">@lang('link_names.dashboard')</a>
                @else
                    <a class="navbar-link" href="{{ route('login') }}" style="color: white">
                        <i class="fe-icon-user"></i>
                        @lang('link_names.login_or_create_new')
                    </a>
                    <div class="dropdown-menu right-aligned p-3 text-center" style="min-width: 200px;">
                        <p class="text-sm opacity-70">
                            @lang('link_names.login_or_create_new')
                        </p>
                        <a class="btn btn-primary btn-sm btn-block" href="{{ route('login') }}">@lang('link_names.login')</a>
                        @can('register')
                        <p class="text-sm text-muted mt-3 mb-0">
                            @lang('link_names.new_user_question')
                            <a href='{{ route('register') }}'>@lang('link_names.register')</a>
                        </p>
                        @endcan
                    </div>
                @endif
            </li>
            <li class="dropdown-toggle">
                <a class="navbar-link" href="#">
                    @if(App::getLocale() == 'en')
                        <img src="/site/img/flags/en.png" alt="English"/>
                        English
                    @elseif(App::getLocale() == 'ru')
                        <img src="/site/img/flags/ru.png" alt="Russian"/>
                        Русский
                    @elseif(App::getLocale() == 'ua')
                        <img src="/site/img/flags/ua.png" alt="Ukrainian"/>
                        Українська
                    @endif
                </a>
                <div class="dropdown-menu lang-dropdown right-aligned">
                    <a href="{{route(\Route::currentRouteName(), !empty($pageName)? ['locale' => 'en', 'name' => $pageName]:['locale' => 'en'])}}" class="dropdown-item" {{(App::getLocale() == 'en')? 'disabled': null}}>
                        <img src="/site/img/flags/en.png" alt="English"/> English
                    </a>
                    <a href="{{route(\Route::currentRouteName(), !empty($pageName)? ['locale' => 'ru', 'name' => $pageName]:['locale' => 'ru'])}}" class="dropdown-item" {{(App::getLocale() == 'ru')? 'disabled': null}}>
                        <img src="/site/img/flags/ru.png" alt="Russian"/> Русский
                    </a>
                    <a href="{{route(\Route::currentRouteName(), !empty($pageName)? ['locale' => 'ua', 'name' => $pageName]:['locale' => 'ua'])}}" class="dropdown-item" {{(App::getLocale() == 'ua')? 'disabled': null}}>
                        <img src="/site/img/flags/ua.png" alt="Ukraine"/> Українська
                    </a>
                </div>
            </li>
        </ul>
    </div>
</div>
