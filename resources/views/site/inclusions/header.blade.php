<!-- Off-Canvas Menu-->
@include('site.inclusions.header-side-menu')

<!-- Navbar: Simple Ghost-->
<header class="navbar-wrapper navbar-boxed navbar-simple-ghost">
    <div class="container-fluid bg-dark" >
        <div class="d-table-cell align-middle pr-md-3"><a class="navbar-brand mr-1" href="{{ route('main') }}"><img src="/site/img/logo/logo.png" alt="CreateX"/></a></div>
        <div class="d-table-cell w-100 align-middle pl-md-3">
            @include('site.inclusions.header-top')
            <div class="navbar justify-content-end justify-content-lg-between">
                <ul class="navbar-nav d-none d-lg-block">
                    <!-- MAIN -->
                    @can(\App\Contracts\SitePageAccessContract::VIEW_MENU_PAGE, \App\Models\PageModels\Page::MAIN)
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('main', ['locale'=>App::getLocale()])}}">{{$menuPageNames->main ?? __('routes.main')}}</a>
                        </li>
                    @endcan
                    <!-- Contacts-->
                    @can(\App\Contracts\SitePageAccessContract::VIEW_MENU_PAGE, \App\Models\PageModels\Page::CONTACTS)
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('contacts', ['locale'=>App::getLocale()])}}">{{$menuPageNames->contacts ?? __('routes.contacts')}}</a>
                        </li>
                    @endcan
                </ul>
                <div>
                    <ul class="navbar-buttons d-inline-block align-middle">
                        <li class="d-block d-lg-none"><a href="#mobile-menu" data-toggle="offcanvas"><i class="fe-icon-menu"></i></a></li>
                    </ul>
                    <a class="btn btn-gradient ml-3 d-none d-xl-inline-block" href="{{ route('cv.get', ['locale' => App::getLocale()]) }}" target="_blank">
                        @lang('buttons.get-cv')
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>
