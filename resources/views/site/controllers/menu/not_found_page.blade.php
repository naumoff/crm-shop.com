@extends('site.layouts.app')

@section('content')
    <div class="container py-5 mb-4 text-center" style="margin-top: 65px;">
        <h1 class="display-404">404</h1>
        <h2 class="h3">@lang('strings.404-sorry')</h2>
        <p class="text-muted">@lang('strings.404-reason')</p>
        @can(\App\Contracts\SitePageAccessContract::VIEW_MENU_PAGE, \App\Models\PageModels\Page::MAIN)
            <a class="btn btn-style-5 btn-secondary btn-block" href="{{route('main', ['locale' => App::getLocale()])}}">
                @lang('buttons.go-home-page')
            </a>
        @endcan
        @can(\App\Contracts\SitePageAccessContract::VIEW_MENU_PAGE, \App\Models\PageModels\Page::CONTACTS)
            <p class="text-muted">
                @lang('strings.or')
            </p>
            <a class="btn btn-style-5 btn-secondary btn-block" href="{{route('contacts', ['locale' => App::getLocale()])}}">
                @lang('buttons.contact_me')
            </a>
        @endcan
    </div>
@endsection
