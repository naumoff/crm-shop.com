@extends('site.layouts.app')

@section('content')
<!-- Page Content-->
<!-- Hero-->
<section class="bg-parallax py-5"><span class="bg-overlay" style="opacity: .6;"></span>
    <div class="bg-parallax-img" data-parallax="{&quot;y&quot; : 100}"><img src="/site/img/pages/contacts-hero-bg.jpg" alt="Parallax Background"/>
    </div>
    <div class="bg-parallax-content px-3 py-md-5 mx-auto mt-lg-5 mb-lg-5 text-center" style="max-width: 800px;">
        <h1 class="text-white pt-2">@lang('heads.get-in-touch')</h1>
        <p class="text-xl text-white opacity-80 pb-3">@lang('strings.connect')</p>
    </div>
</section>
<!-- Contact Details-->
<section class="container-fluid mb-5">
    <div class="row">
        <div class="col-md-3 col-sm-6 border-right py-2 border-bottom">
            <a class="scroll-to icon-box text-center mx-auto box-shadow-none px-0" href="#map">
                <div class="icon-box-icon"><i class="fe-icon-map-pin"></i></div>
                <h3 class="icon-box-title">@lang('heads.find_me')</h3>
                <p class="icon-box-text font-weight-medium">@lang('strings.address-location')</p>
            </a>
        </div>
        <div class="col-md-3 col-sm-6 py-2 border-right border-bottom">
            <a class="icon-box text-center mx-auto box-shadow-none px-0" href="tel:+380952680707">
                <div class="icon-box-icon"><i class="fe-icon-phone"></i></div>
                <h3 class="icon-box-title">@lang('heads.call_me')</h3>
                <p class="icon-box-text font-weight-medium">+38 095 268 0707</p>
                <p>Viber / Telegram</p>
            </a>
        </div>
        <div class="col-md-3 col-sm-6 py-2 border-right border-bottom">
            <a class="icon-box text-center mx-auto box-shadow-none px-0" href="mailto:support@example.com">
                <div class="icon-box-icon"><i class="fe-icon-mail"></i></div>
                <h3 class="icon-box-title">@lang('heads.email_me')</h3>
                <p class="icon-box-text font-weight-medium">andrey.naumoff@crm-shop.com</p>
            </a>
        </div>
        <div class="col-md-3 col-sm-6 py-2 border-bottom">
            <a class="icon-box text-center mx-auto box-shadow-none px-0" href="#">
                <div class="icon-box-icon"><i class="fe-icon-facebook"></i></div>
                <h3 class="icon-box-title">@lang('heads.follow_me')</h3>
            </a>
            <div style="text-align: center">
                <a class="social-btn sb-style-6 sb-facebook " href="https://www.facebook.com/naumoff.andrey" target="_blank">
                    <i class="socicon-facebook"></i>
                </a>
                <a class="social-btn sb-style-6 sb-linkedin " href="https://www.linkedin.com/in/andrey-naumoff-223736150/" target="_blank">
                    <i class="socicon-linkedin"></i>
                </a>
                <a class="social-btn sb-style-6 sb-github " href="https://gitlab.com/naumoff" target="_blank">
                    <i class="socicon-github"></i>
                </a>
            </div>
        </div>
    </div>
</section>
<!-- Contact Form-->
<section class="container mb-5 pb-3">
    <div class="wizard">
        <div class="wizard-body pt-3">
            <h2 class="h4 text-center">@lang('heads.write_me')</h2>
            <p class="text-muted text-center">@lang('strings.get_in_touch_soon')</p>

            {{--ERRORS--}}
            <div class="col-lg-12 error-block" id="error-request-block" style="display: none">
                <div class="alert alert-danger alert-dismissible fade show">
                    <button type="button" class="close" data-dismiss="alert"></button>
                    <strong>@lang('heads.unknown_server_errors')</strong>
                    <ul class="error-request-list-block"></ul>
                </div>
            </div>

            {{--SUCCESS--}}
            <div class="col-lg-12 success-block" id="success-request-block" style="display: none">
                <div class="alert alert-success alert-dismissible fade show">
                    <button type="button" class="close" data-dismiss="alert"></button>
                    <strong>@lang('heads.successful_form_submit')</strong>
                    <ul class="success-request-list-block"></ul>
                </div>
            </div>

            {{--FORM--}}
            <form class="needs-validation" method="post" enctype="multipart/form-data" id="contact-request-form" novalidate>
                @csrf
                <div class="row pt-3">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="contact-name">@lang('fields.your_name') <span class='text-danger font-weight-medium'>*</span></label>
                            <input class="form-control" name="contact_name" type="text" id="contact-name" placeholder="@lang('placeholders.name')" required>
                            <ul class="error-list-request contact_name" style="color: red"></ul>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="contact-email">@lang('fields.your_email') <span class='text-danger font-weight-medium'>*</span></label>
                            <input class="form-control" name="contact_email" type="email" id="contact-email" placeholder="@lang('placeholders.email')" required>
                            <ul class="error-list-request contact_email" style="color: red"></ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="contact-subject">@lang('fields.subject') <span class='text-danger font-weight-medium'>*</span></label></label>
                            <input class="form-control" name="contact_subject" type="text" id="contact-subject" placeholder="@lang('placeholders.subject')" required>
                            <ul class="error-list-request contact_subject" style="color: red"></ul>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="contact-file">@lang('fields.attachment')</label>
                            <div class="custom-file">
                                <input class="custom-file-input" name="contact_file" type="file" id="contact-file">
                                <label class="custom-file-label" for="contact-file">@lang('placeholders.choose_file')</label>
                            </div>
                            <ul class="error-list-request contact_file" style="color: red"></ul>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="contact-message">@lang('fields.message') <span class='text-danger font-weight-medium'>*</span></label>
                    <textarea class="form-control" rows="7" name="contact_message" id="contact-message" placeholder="@lang('placeholders.message')" required></textarea>
                    <ul class="error-list-request contact_message" style="color: red"></ul>
                </div>
                <div class="text-center">
                    <button class="btn btn-primary" id="contact_request" type="submit">@lang('buttons.send')</button>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
@push('js-scripts')
<script>
  $(document).ready(function(){
    $("#contact-request-form").on('submit', (function(event){
      event.preventDefault();

      hideRequestErrorBlock();
      hideRequestSuccessBlock();
      $('.error-list-request').empty();
      $('.error-request-list-block').empty();
      $('.success-request-list-block').empty();

      $.ajax({
        type: 'POST',
        url: '/{{App::getLocale()}}/contacts',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData:false,
        dataType: 'json',
        success: function (data) {
          let success = '<li>' + data.success + '</li>';
          $(".success-request-list-block").append(success);
          showRequestSuccessBlock();
          makeFormInputEmpty();
          dataLayer.push({'event': 'contact-form-send'});
        },
        error: function (data) {
          if (data.status === 422) {
            let errors = data.responseJSON;
            let errorsContainers = Object.keys(errors.errors);
            for (let errorProperty of errorsContainers) {
              for (let errorMessage of errors.errors[errorProperty]) {
                let error = '<li>' + errorMessage + '</li>';
                $("." + errorProperty).append(error);
              }
            }
          } else {
            let error = '<li>' + '@lang('strings.request_unknown_error_message')' + '</li>';
            $(".error-request-list-block").append(error);
            showRequestErrorBlock();
          }
        },
      });
    }));
  });
  function showRequestErrorBlock() {
    document.getElementById("error-request-block").style.display = 'block';
  }

  function hideRequestErrorBlock() {
    document.getElementById("error-request-block").style.display = 'none';
  }

  function showRequestSuccessBlock() {
    document.getElementById("success-request-block").style.display = 'block';
  }

  function hideRequestSuccessBlock() {
    document.getElementById("success-request-block").style.display = 'none';
  }

  function makeFormInputEmpty() {
    $('input[name="contact_name"]').val('');
    $('input[name="contact_email"]').val('');
    $('input[name="contact_subject"]').val('');
    $('textarea[name="contact_message"]').val('');
    $('#contact-request-form').hide();
  }
</script>
@endpush
