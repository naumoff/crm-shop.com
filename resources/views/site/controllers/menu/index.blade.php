@extends('site.layouts.app')

@section('content')
    <!-- Page Content-->
    <section class="bg-dark bg-center bg-no-repeat py-5" style="background-image: url(/site/img/homepages/conference-landing/hero-bg.jpg);">
        <div class="container py-5 my-5 text-center text-sm-left">
            <div class="pt-sm-5 mt-sm-5">
                <h1 class="display-3 text-white pt-md-5 pb-2">
                <span class="d-block" data-parallax="{&quot;y&quot; : -20}">
                    <span class="d-none d-sm-inline-block bg-white text-dark text-center mr-2" data-parallax="{&quot;rotateZ&quot; : 20}" style="width: 80px;">L</span>
                    <span>
                        <span class='d-sm-none'>L</span>aravel
                    </span>
                </span>
                    <span class="d-block" data-parallax="{&quot;y&quot; : 10}">
                    <span class="d-none d-sm-inline-block bg-white text-dark text-center mr-2" data-parallax="{&quot;rotateZ&quot; : -20}" style="width: 80px;">D</span>
                    <span>
                        <span class='d-sm-none'>D</span>eveloper
                    </span>
                </span>
                    <span class="d-block" data-parallax="{&quot;y&quot; : 40}">
                    <span class="d-none d-sm-inline-block bg-white text-dark text-center mr-2" data-parallax="{&quot;rotateZ&quot; : 20}" style="width: 80px;">H</span>
                    <span>
                        <span class='d-sm-none'>H</span>ere!
                    </span>
                </span>
                </h1>
            </div>
        </div>
    </section>

    <section class="container pb-3" style="margin-top: +10px;">
        <div class="bg-white box-shadow px-3 px-sm-5 pt-5 text-center text-sm-left">
            <!-- Project Details-->
            <div class="row">
                <div class="col-lg-4">
                    <div class="widget">
                        <h2 class="widget-title">@lang('heads.programming_languages'):</h2>
                        <ul class="list-unstyled mb-0">
                            <li>
                                <span class="text-muted">Back-end:</span>
                                <span class="font-weight-medium">&nbsp; PHP v.5.6 - 7.3</span>
                            </li>
                            <li>
                                <span class="text-muted">Front-end:</span>
                                <span class="font-weight-medium">&nbsp; JavaScript ES6</span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="widget">
                        <h2 class="widget-title">@lang('heads.progress')</h2>
                        <!-- Progress-->
                        <div class="progress mb-3">
                            <div class="progress-bar bg-success" role="progressbar" style="width: 90%;" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"><span><i class="fe-icon-server"></i>Back-End: PHP - 90%</span></div>
                        </div>
                        <!-- Progress-->
                        <div class="progress mb-3">
                            <div class="progress-bar bg-warning" role="progressbar" style="width: 50%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"><span><i class="fe-icon-monitor"></i>Front-End: JS - 50%</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="widget">
                        <h2 class="widget-title">@lang('heads.databases'):</h2>
                        <ul class="list-unstyled mb-0">
                            <li>
                                <span class="text-muted">Relational:</span>
                                <span class="font-weight-medium">&nbsp; MySQL v.5.6 - 8.0</span>
                            </li>
                            <li>
                                <span class="text-muted">Non-Relational:</span>
                                <span class="font-weight-medium">&nbsp; Redis v. 3.2</span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="widget">
                        <h2 class="widget-title">@lang('heads.progress')</h2>
                        <!-- Progress-->
                        <div class="progress mb-3">
                            <div class="progress-bar bg-success" role="progressbar" style="width: 85%;" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"><span><i class="fe-icon-database"></i>Relational DB: MySQL - 85%</span></div>
                        </div>
                        <!-- Progress-->
                        <div class="progress mb-3">
                            <div class="progress-bar bg-warning" role="progressbar" style="width: 65%;" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"><span><i class="fe-icon-database"></i>Non Relation DB: Redis - 65%</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="widget">
                        <h2 class="widget-title">@lang('heads.frameworks'):</h2>
                        <ul class="list-unstyled mb-0">
                            <li>
                                <span class="text-muted">PHP Framework:</span>
                                <span class="font-weight-medium">&nbsp; Laravel v.5.4 - 5.8</span>
                            </li>
                            <li>
                                <span class="text-muted">JS Framework:</span>
                                <span class="font-weight-medium">&nbsp; Vue.js v. 2.0 </span>
                            </li>
                            <li>
                                <span class="text-muted">Vue.js Framework:</span>
                                <span class="font-weight-medium">&nbsp; Element-UI v. 2.1 </span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="widget">
                        <h2 class="widget-title">@lang('heads.progress')</h2>
                        <!-- Progress-->
                        <div class="progress mb-3">
                            <div class="progress-bar bg-success" role="progressbar" style="width: 95%;" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"><span><i class="fe-icon-edit"></i>PHP Framework: Laravel - 95%</span></div>
                        </div>
                        <!-- Progress-->
                        <div class="progress mb-3">
                            <div class="progress-bar bg-warning" role="progressbar" style="width: 65%;" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"><span><i class="fe-icon-edit-2"></i>JS Framework: Vue.js - 65%</span></div>
                        </div>
                        <!-- Progress-->
                        <div class="progress mb-3">
                            <div class="progress-bar bg-success" role="progressbar" style="width: 85%;" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"><span><i class="fe-icon-edit-3"></i>Vue.js Framework: Element-UI - 85%</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="widget">
                        <h2 class="widget-title">@lang('heads.libraries'):</h2>
                        <ul class="list-unstyled mb-0">
                            <li>
                                <span class="text-muted">JS / CSS:</span>
                                <span class="font-weight-medium">&nbsp; Bootstrap v.3.0 - v.4.0</span>
                            </li>
                            <li>
                                <span class="text-muted">JS / CSS</span>
                                <span class="font-weight-medium">&nbsp; Jquery v.3.4.1 </span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="widget">
                        <h2 class="widget-title">@lang('heads.progress')</h2>
                        <!-- Progress-->
                        <div class="progress mb-3">
                            <div class="progress-bar bg-success" role="progressbar" style="width: 85%;" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"><span><i class="fe-icon-code"></i>Bootstrap - 85%</span></div>
                        </div>
                        <!-- Progress-->
                        <div class="progress mb-3">
                            <div class="progress-bar bg-warning" role="progressbar" style="width: 45%;" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"><span><i class="fe-icon-codepen"></i>Jquery - 45%</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="widget">
                        <h2 class="widget-title">@lang('heads.testing'):</h2>
                        <ul class="list-unstyled mb-0">
                            <li>
                                <span class="text-muted">PHP: Back-end testing</span>
                                <span class="font-weight-medium">&nbsp; PHPUnit framework</span>
                            </li>
                            <li>
                                <span class="text-muted">PHP: Front-end testing</span>
                                <span class="font-weight-medium">&nbsp; Dusk Laravel Package </span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="widget">
                        <h2 class="widget-title">@lang('heads.progress')</h2>
                        <!-- Progress-->
                        <div class="progress mb-3">
                            <div class="progress-bar bg-success" role="progressbar" style="width: 80%;" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"><span><i class="fe-icon-check-square"></i>PHPUnit - 80%</span></div>
                        </div>
                        <!-- Progress-->
                        <div class="progress mb-3">
                            <div class="progress-bar bg-success" role="progressbar" style="width: 98%;" aria-valuenow="98" aria-valuemin="0" aria-valuemax="100"><span><i class="fe-icon-check-circle"></i>Dusk - 98%</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="widget">
                        <h2 class="widget-title">@lang('heads.tools'):</h2>
                        <ul class="list-unstyled mb-0">
                            <li>
                                <span class="text-muted">VCS:</span>
                                <span class="font-weight-medium">&nbsp; GIT, GitHub, GitLab</span>
                            </li>
                            <li>
                                <span class="text-muted">Environment:</span>
                                <span class="font-weight-medium">&nbsp; Linux, LAMP, Vagrant/Homestead</span>
                            </li>
                            <li>
                                <span class="text-muted">Package Mngmt:</span>
                                <span class="font-weight-medium">&nbsp; Composer, NPM</span>
                            </li>
                            <li>
                                <span class="text-muted">IDE:</span>
                                <span class="font-weight-medium">&nbsp; PHPStorm </span>
                            </li>
                            <li>
                                <span class="text-muted">Task Mngmt:</span>
                                <span class="font-weight-medium">&nbsp; Kanban, Jira </span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="widget">
                        <h2 class="widget-title">@lang('heads.progress')</h2>
                        <!-- Progress-->
                        <div class="progress mb-3">
                            <div class="progress-bar bg-success" role="progressbar" style="width: 85%;" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"><span><i class="fe-icon-terminal"></i>Rest - 85%</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="widget mb-4">
                        <h2 class="widget-title">@lang('heads.download')</h2>
                        <a class="btn btn-gradient btn-block " href="{{ route('cv.get', ['locale' => App::getLocale()]) }}" target="_blank">
                            @lang('buttons.get-cv')
                        </a>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="widget">
                        <h2 class="widget-title">@lang('heads.other')</h2>
                        <p class="text-muted">
                            @lang('strings.cv_other')
                        </p>
                    </div>
                </div>
            </div>
            <!-- Project Share-->
            {{--<div class="d-sm-flex justify-content-between align-items-center border-top border-bottom mb-4 py-2">--}}
                {{--<div class="py-2"><span class="dinline-block align-middle py-2 mr-2"><strong>Share:</strong></span><a class="social-btn sb-style-3 sb-facebook mb-0" href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="socicon-facebook"></i></a><a class="social-btn sb-style-3 sb-twitter mb-0" href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="socicon-twitter"></i></a><a class="social-btn sb-style-3 sb-pinterest mb-0" href="#" data-toggle="tooltip" data-placement="top" title="Pinterest"><i class="socicon-pinterest"></i></a></div>--}}
                {{--<div class="py-2"></div>--}}
                {{--<div class="post-meta"><a class="text-md" href="#"><i class="fe-icon-heart text-accent"></i>&nbsp;40</a></div>--}}
            {{--</div>--}}
        </div>
    </section>
    <!-- Contact Us CTA-->
    <section class="bg-secondary py-4">
        <div class="container position-relative bg-no-repeat bg-center py-5 text-center" style="background-image: url(/site/img/pages/dotted-map.png);">
            <h2 class="block-title text-dark mt-5 mb-4">
                @lang('heads.lets_connect')
                <small class="text-dark">
                    @lang('strings.connect')
                </small>
            </h2>
            <a class="btn btn-primary mb-5" href="{{ route('contacts', ['locale' => App::getLocale()]) }}">
                @lang('buttons.contact_me')
            </a>
        </div>
    </section>
@endsection
