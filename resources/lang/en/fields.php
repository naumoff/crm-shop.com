<?php
/**
 * User: Andrey Naumoff
 * Date: 01-Jul-19
 * Time: 1:40 PM
 * E-mail: andrey.naumoff@gmail.com
 */

return [
    'attachment' => 'ATTACHMENT', //+
    'message' => 'MESSAGE', //+
    'subject' => 'SUBJECT', //+
    'your_email' => 'YOUR EMAIL', //+
    'your_name' => 'YOUR NAME', //+
];
