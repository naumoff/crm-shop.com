<?php

// @codingStandardsIgnoreStart
return [
    '404-sorry' => 'Sorry, but the page <br class="veil reveal-md-block"> was not found',
    '404-reason' => 'You may have mistyped the address or the page may have moved.',
    'address-location' => 'Ukraine, Kiev region, Vyshgorod, 07300', //+
    'address-street' => 'Sholudenko Str., 328',
    'call_to_action' => 'if you are looking for skillful PHP developer on Laravel framework - please feel free to contact me via provided below contact information, or leave your request in contact form.', //+
    'closed' => 'Closed',
    'coming_soon' => 'Coming soon...', //+
    'connect' => 'I do my best to turn your ideas into reality.', //+
    'cv_other' => 'I speak English / Russian / Ukrainian. I love <b>PROGRAMMING</b> & <b>PETANQUE!</b> ;-)',
    'database-volume' => 'GB of Productive data base',
    'development' => 'Development',
    'experience' => 'Since April 2017 our company has been providing support, and since January 2018 support & development of PROZORRO - public e-procurement system: 350 Virtual servers in 3 vertical equal zones Files’ volume in object storage - 80 Tb, increasing +3,5 Tb/month. Volume of NoSQL productive data base - 230 Gb. 200 000 system users. Quantity of fixed sessions per day - 21 mln. Traffic per day: 442 Gb downloaded, 88 Gb uploaded.',
    'get-subscription' => 'Get the latest updates and offer', //+
    'get_in_touch_soon' => 'I will get back to you as soon as possible', //+
    'our-mission' => 'We see our mission in providing innovative and effective IT solutions which help our clients grow their businesses and realize their goals.',
    'our-vision' => 'We are focused, but not limited, on 3 points: OPEN SOURCE ENTERPRISE SOLUTIONS, IT SECURITY AUDIT and CONSULTING, INTEGRATION.',
    'our-stack' => 'Ansible, git, docker, CouchDB, MongoDB, MySQL, PostgreSQL, memcached, RabbitMQ, HAProxy, nginx, apache, mod_security, varnish, ELK, Netdata, Grafana, Zabbix, InfluxDB, python, php, perl, Jenkins, Selenium, Robot framework, VMware, KVM, AWS ang others',
    'or' => 'or',
    'phone' => 'Phone',
    'request_submit_success_message' => 'Your request successfully submitted. Thank you for contacting me!', //+
    'request_unknown_error_message' => 'Your request was not submitted, please contact me by email, to solve this problem.', //+
    'servers-qty' => 'Virtual servers',
    'sessions-qty' => 'MLN Fixed sessions/day',
    'slider-description-1' => 'Whatever type of DevOps solutions or tech stack you’re about to use to solve your tasks, be sure that thanks to skills and in-depth expertise of our pros, it will all be done with best-fitting architecture, smooth integration and delivered in time.',
    'slider-description-2' => 'We specialize in development of enterprise business software and applications for custom needs. Our experience allows us to create applications with high performance with the . use of C++, PHP, C#, Java, Flex, Delphi, Linux/Unix, Python, Perl and other technologies, programming languages and platforms.',
    'slider-description-3' => 'Our core specialists have great track record in variouse projects, from development of high load dynamic databases in procurement & banking to VoIP based call centers and building secure PCI DSS compiant IT infrostructure and services.',
    'slider-description-4' => 'To reduce costs, wherever it is applicable for customer\'s needs, we utilize in best manner combinations of the most innovative open source solutions, such as Ansible, Git, Docker, CouchDB, Mongodb, MySQL, PostgreSQL, Memcached, RabbitMQ, HAProxy, nginx, apache, mod_security, varnish, ELK, Netdata, Grafana, Zabbix, InfluxDB, Jenkins, Selenium, Robot framework, VMware, KVM, AWS & other.',
    'subscribe_help_text' => 'Enter email to receive newsletters and new blog posts.', //+
    'subscribe_success_message' => 'email is added to subscription list successfully.', //+
    'subscribe_unknown_error_message' => 'email was not added to subscription list, please contact me, to solve this problem.', //+
    'users-qty' => 'system users',
    'weekdays' => 'Weekdays',
    'weekends' => 'Weekends',
    'who-we-are' => 'MK-CONSULTING was founded 7 March 2017 with the core mission of providing high-quality software solutions that work, and personal service. We take on projects both big and small, work with a diverse set of clients, and really enjoy hammering out problems and getting results. We can be your creative team or work to support your in-house employees. With a 100 % guarantee, we are here for the long haul.',
];
// @codingStandardsIgnoreEnd
