<?php

return [
    'choose_file' => 'Choose file...', //+
    'email' => 'E-mail', //+
    'enter-email' => 'Enter your email', //+
    'message' => 'Let me know more what`s on your mind...', //+
    'name' => 'Your name', //+
    'phone' => 'Phone',
    'subject' => 'Provide short title of you request', //+
];
