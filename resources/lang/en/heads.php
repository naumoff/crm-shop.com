<?php

return [
    'address' => 'Address',
    'back_end' => 'Back-end: PHP/Laravel',
    'call_me' => 'Call me', //+
    'clients' => 'Clients',
    'databases' => 'databases', //+
    'download' => 'download', //+
    'email' => 'E-Mail',
    'email_me' => 'Email me', //+
    'experience' => 'Experience',
    'find_me' => 'Find me', //+
    'follow_me' => 'Follow me', //+
    'frameworks' => 'frameworks', //+
    'front_end' => 'Front-end: JS/Vue.js',
    'get-in-touch' => 'Get in touch with me!', //+
    'get-social' => 'Get social',
    'lets_connect' => "Let's connect", //+
    'libraries' => 'libraries', //+
    'main-office' => 'Office',
    'menu' => 'Menu', //+
    'my_projects' => 'My projects', //+
    'other' => 'other', //+
    'our-mission' => 'Our mission',
    'our-stack' => 'Our stack',
    'our-vision' => 'Our vision',
    'phone' => 'Phone',
    'progress' => 'progress', //+
    'programming_languages' => 'programming languages', //+
    'statement-1' => '<span class="custom-line">Best devops</span> <span>experts</span>',
    'statement-2' => '<span class="custom-line">Wide technologies</span> <span>stack</span>',
    'statement-3' => '<span class="custom-line">Diverse</span> <span>expertise</span>',
    'statement-4' => '<span class="custom-line">Open source</span> <span>enterprise</span>',
    'subscribe' => 'Subscribe to Newsletter', //+
    'testing' => 'testing', //+
    'tools' => 'tools', //+
    'successful_form_submit' => 'Form submitted successfully!', //+
    'unknown_server_errors' => 'Attention: your request was not processed due to unknown reasons!', //+
    'validation_errors' => 'Attention: there are validation errors!', //+
    'who-we-are' => 'Who we are',
    'write_me' => 'Drop me a line', //+
];
