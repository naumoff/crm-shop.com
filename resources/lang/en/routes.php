<?php

return [
    'main' => 'Main',
    'about-us' => 'About Us',
    'services' => 'Services',
    'our-work' => 'Our work',
    'news' => 'News',
    'contacts' => 'Contacts',
];
