<?php

return [
    'contact_me' => 'contact me', //+
    'get-cv' => 'GET CV', //+
    'get-quote' => 'Get a quote',
    'go-home-page' => 'go to home page',
    'send' => 'Send', //+
    'send-message' => 'Send message',
];
