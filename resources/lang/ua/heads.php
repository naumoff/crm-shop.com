<?php

return [
    'address' => 'Адреса',
    'back_end' => 'Back-end: PHP/Laravel',
    'call_me' => 'Зателефонуйте мені', //+
    'clients' => 'Клієнти',
    'databases' => 'бази даних', //+
    'download' => 'завантажити', //+
    'email' => 'Ел. адреса',
    'email_me' => 'Напишіть мені', //+
    'experience' => 'Досвід',
    'find_me' => 'Знайдіть мене', //+
    'follow_me' => 'Підпишіться на мене', //+
    'frameworks' => 'фреймворки', //+
    'front_end' => 'Front-end: JS/Vue.js',
    'get-in-touch' => 'Зв`яжіться зі мною!', //+
    'lets_connect' => "Давайте співпрацювати!", //+
    'libraries' => 'бібліотеки', //+
    'main-office' => 'Офіс',
    'menu' => 'Меню', //+
    'my_projects' => 'Мої проекти', //+
    'other' => 'інше', //+
    'our-mission' => 'Наша місія',
    'our-vision' => 'Наше бачення',
    'our-stack' => 'Наші технології',
    'phone' => 'Телефон',
    'progress' => 'прогрес', //+
    'programming_languages' => 'мови програмування', //+
    'subscribe' => 'Підписатися на новини', //+
    'statement-1' => '<span class="custom-line">Кращі розробники</span> <span>в отраслi</span>',
    'statement-2' => '<span class="custom-line">Широкий стек</span> <span>технологий</span>',
    'statement-3' => '<span class="custom-line">Багатий і різнобічний</span> <span>досвід</span>',
    'statement-4' => '<span class="custom-line">Відкритий</span> <span>вихідний код</span>',
    'testing' => 'тестування', //+
    'tools' => 'інструменти', //+
    'successful_form_submit' => 'Форма успішно надіслана!', //+
    'unknown_server_errors' => 'Увага: Ваш запит не був оброблений з невідомих причин!', //+
    'validation_errors' => 'Увага: введені дані не пройшли валідацію!', //+
    'who-we-are' => 'Хто ми є',
    'write_me' => 'Напишіть мені', //+
    'get-social' => 'Ми в соц. мережах',
];
