<?php
/**
 * User: Andrey Naumoff
 * Date: 01-Jul-19
 * Time: 1:40 PM
 * E-mail: andrey.naumoff@gmail.com
 */

return [
    'attachment' => 'ВКЛАДЕННЯ', //+
    'message' => 'ПОВIДОМЛЕННЯ', //+
    'subject' => 'ТЕМА ЗАПИТУ', //+
    'your_email' => 'ВАША ЕЛ. АДРЕСА', //+
    'your_name' => 'ВАШЕ ІМ`Я', //+
];
