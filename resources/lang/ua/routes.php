<?php

return [
    'main' => 'Головна',
    'about-us' => 'Про нас',
    'services' => 'Послуги',
    'our-work' => 'Наші роботи',
    'news' => 'Новини',
    'contacts' => 'Контакти',
];
