<?php
/**
 * User: Andrey Naumoff
 * Date: 01-Jul-19
 * Time: 1:40 PM
 * E-mail: andrey.naumoff@gmail.com
 */


return [
    'attachment' => 'ВЛОЖЕНИЕ', //+
    'message' => 'СООБЩЕНИЕ', //+
    'subject' => 'ТЕМА ЗАПРОСА', //+
    'your_email' => 'ВАШ ЭЛ. АДРЕС', //+
    'your_name' => 'ВАШЕ ИМЯ', //+
];
