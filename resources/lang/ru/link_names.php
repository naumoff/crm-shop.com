<?php
/**
 * User: Andrey Naumoff
 * Date: 26-Nov-18
 * Time: 12:19 PM
 * E-mail: andrey.naumoff@gmail.com
 */

return [
    'login' => 'Вход', //+
    'register' => 'Регистрация', //+
    'dashboard' => 'Кабинет', //+
    'login_or_create_new' => 'Авторизируйтесь', //+
    'new_user_question' => 'Новый пользователь?' //+
];
