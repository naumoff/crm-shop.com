<?php

return [
    'address' => 'Адрес',
    'back_end' => 'Back-end: PHP/Laravel',
    'call_me' => 'Позвоните мне', //+
    'clients' => 'Клиенты',
    'databases' => 'базы данных', //+
    'download' => 'скачать', //+
    'email' => 'Эл. адрес',
    'email_me' => 'Напишите мне', //+
    'experience' => 'Опыт',
    'find_me' => 'Найдите меня', //+
    'follow_me' => 'Подпишитесь на меня', //+
    'frameworks' => 'фреймворки', //+
    'front_end' => 'Front-end: JS/Vue.js',
    'get-in-touch' => 'Свяжитесь со мной!', //+
    'get-social' => 'Мы в соц. сетях',
    'lets_connect' => "Давайте сотрудничать!", //+
    'libraries' => 'библиотеки', //+
    'main-office' => 'Офис',
    'menu' => 'Меню', //+
    'my_projects' => 'Мои проекты', //+
    'other' => 'другое', //+
    'our-mission' => 'Наша миссия',
    'our-stack' => 'Используемые нами технологии',
    'our-vision' => 'Наше виденье',
    'phone' => 'Телефон',
    'progress' => 'прогресс', //+
    'programming_languages' => 'языки программирования', //+
    'subscribe' => 'Подписаться на новости', //+
    'statement-1' => '<span class="custom-line">Лучшие разработчики</span> <span>в отрасли</span>',
    'statement-2' => '<span class="custom-line">Широкий стек</span> <span>используемых технологий</span>',
    'statement-3' => '<span class="custom-line">Богатый и разносторонний</span> <span>опыт</span>',
    'statement-4' => '<span class="custom-line">Открытый</span> <span>исходный код</span>',
    'testing' => 'тестирование', //+
    'tools' => 'инструменты', //+
    'successful_form_submit' => 'Форма успешно отправлена!', //+
    'unknown_server_errors' => 'Внимание: ваш запрос не был обработан по неизвестным причинам!', //+
    'validation_errors' => 'Внимание: введенные данные не прошли валидацию!', //+
    'who-we-are' => 'Кто мы',
    'write_me' => 'Напишите мне', //+
];
