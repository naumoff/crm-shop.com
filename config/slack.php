<?php
/**
 * User: Andrey Naumoff
 * Date: 26-Jan-19
 * Time: 10:47 PM
 * E-mail: andrey.naumoff@gmail.com
 */

return [
    'web_hooks' => [
        'subscription' => env(
            'SUBSCRIPTION_WEB_HOOK',
            'https://hooks.slack.com/services/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'
        ),
        'contact_request' => env(
            'CONTACT_REQUEST_WEB_HOOK',
            'https://hooks.slack.com/services/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'
        ),
    ]
];
