<?php
/**
 * User: Andrey Naumoff
 * Date: 9/15/2019
 * Time: 3:19 PM
 * E-mail: andrey.naumoff@crm-shop.com
 */

$productionIpList = explode(',', env('LOCAL_WHITE_IP_LIST', ''));
$localIpList = explode(',', env('PRODUCTION_WHITE_IP_LIST', ''));

return array_merge($productionIpList, $localIpList);
