<?php

namespace App\Providers;

use App\Contracts\SitePageAccessContract;
use App\Services\PageAccess\SitePageAccessService;
use Illuminate\Support\ServiceProvider;

class SitePageAccessServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->singleton(SitePageAccessContract::class, SitePageAccessService::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides(): array
    {
        return [SitePageAccessContract::class];
    }
}
