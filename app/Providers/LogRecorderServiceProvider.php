<?php

namespace App\Providers;

use App\Contracts\LogRecorderContract;
use App\Services\LogRecorders\LogRecorderService;
use Illuminate\Support\ServiceProvider;

class LogRecorderServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(LogRecorderContract::class, LogRecorderService::class);
    }
}
