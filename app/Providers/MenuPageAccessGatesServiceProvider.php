<?php

namespace App\Providers;

use App\Contracts\MenuPageAccessGatesContract;
use App\Services\PageContent\MenuPageAccessGatesService;
use Illuminate\Support\ServiceProvider;

class MenuPageAccessGatesServiceProvider extends ServiceProvider
{
    /** @var bool $defer */
    protected $defer = true;
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->singleton(MenuPageAccessGatesContract::class, MenuPageAccessGatesService::class);
    }

    /**
     * @return array
     */
    public function provides(): array
    {
        return [MenuPageAccessGatesContract::class];
    }
}
