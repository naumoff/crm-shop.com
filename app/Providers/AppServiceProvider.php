<?php

namespace App\Providers;

use App\Models\PageModels\Page;
use App\Models\PageModels\PageType;
use App\Models\UserModels\User;
use App\Models\UserModels\UserRole;
use App\Observers\PageObserver;
use App\Observers\PageTypeObserver;
use App\Observers\UserObserver;
use App\Observers\UserRoleObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Page::observe(PageObserver::class);
        PageType::observe(PageTypeObserver::class);
        User::observe(UserObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
