<?php

namespace App\Providers;

use App\Contracts\ArrayPaginationContract;
use App\Services\Paginations\ArrayPaginationService;
use Illuminate\Support\ServiceProvider;

class ArrayPaginationServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ArrayPaginationContract::class, ArrayPaginationService::class);
    }

    /**
     * Get the services provided by the provider.
     * @return array
     */
    public function provides()
    {
        return [ArrayPaginationContract::class];
    }
}
