<?php

namespace App\Providers;

use App\Contracts\SitePageAccessTableContract;
use App\Services\ViewHelpers\RedisSitePageAccessTableService;
use Illuminate\Support\ServiceProvider;

class SitePageAccessTableServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->singleton(SitePageAccessTableContract::class, RedisSitePageAccessTableService::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides(): array
    {
        return [SitePageAccessTableContract::class];
    }
}
