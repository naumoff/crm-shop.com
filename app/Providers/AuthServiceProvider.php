<?php

namespace App\Providers;

use App\Contracts\SitePageAccessContract;
use App\Contracts\UserIpCheckerContract;
use App\Models\UserModels\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    #region PROPERTIES
    /** @var null|SitePageAccessContract $sitePageAccessService */
    private $sitePageAccessService;

    /** @var array $pagesRights */
    private $pageRights = [
        SitePageAccessContract::VIEW_MENU_PAGE,
    ];

    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Models\UserModels' => 'App\Policies\ModelPolicy',
    ];

    #endregion

    #region MAIN METHODS
    public function __construct($app)
    {
        parent::__construct($app);
    }
    
    /**
     * @param SitePageAccessContract $sitePageAccess
     */
    public function boot(SitePageAccessContract $sitePageAccess): void
    {
        $this->registerPolicies();

        Passport::routes();
        Passport::tokensExpireIn(now()->addHours(1));
        Passport::refreshTokensExpireIn(now()->addHours(2));

        Passport::loadKeysFrom(storage_path('passport'));

        $this->sitePageAccessService = $sitePageAccess;

        foreach ($this->pageRights as $right) {
            /** pages rights for guests and users */
            Gate::define($right, function (?User $user, int $pageId) use ($right) {
                return $this->sitePageAccessService->$right($user, $pageId);
            });
        }
        
        Gate::define('register', function(?User $user) {
            if ($user === null) {
                /** @var UserIpCheckerContract $userIpCheckerService */
                $userIpCheckerService = app()->make(UserIpCheckerContract::class);
                return $userIpCheckerService->checkUserIp();
            }
            return false;
        });
    }
    #endregion
}
