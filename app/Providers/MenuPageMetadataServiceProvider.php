<?php

namespace App\Providers;

use App\Contracts\PageMetadataContract;
use App\Services\PageContent\PageMetadataService;
use Illuminate\Support\ServiceProvider;

class MenuPageMetadataServiceProvider extends ServiceProvider
{
    /** @var bool $defer */
    protected $defer = true;
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->singleton(PageMetadataContract::class, PageMetadataService::class);
    }

    /**
     * @return array
     */
    public function provides(): array
    {
        return [PageMetadataContract::class];
    }
}
