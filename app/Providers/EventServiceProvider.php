<?php

namespace App\Providers;

use App\Events\ContactRequestSubmitted;
use App\Events\NewUserInvited;
use App\Events\RefreshCachedModels;
use App\Events\SubscriptionRequestSubmitted;
use App\Listeners\ContactRequestNotification;
use App\Listeners\RefreshPageModel;
use App\Listeners\RefreshUserModel;
use App\Listeners\SendEmailVerificationInvitation;
use App\Listeners\SubscriptionRequestNotification;
use App\Listeners\UserEventSubscriber;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    #region PROPERTIES
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        SubscriptionRequestSubmitted::class => [
            SubscriptionRequestNotification::class,
        ],
        ContactRequestSubmitted::class => [
            ContactRequestNotification::class,
        ],
        NewUserInvited::class => [
            SendEmailVerificationInvitation::class,
        ],
        RefreshCachedModels::class => [
            RefreshUserModel::class,
            RefreshPageModel::class,
        ],
    ];

    /**
     * Event Listener without container
     * @var array $subscribe
     */
    protected $subscribe = [
        UserEventSubscriber::class,
    ];
    #endregion

    #region MAIN METHODS
    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
    #endregion
}
