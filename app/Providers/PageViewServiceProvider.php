<?php

namespace App\Providers;

use App\Contracts\PageViewContract;
use App\Services\PageContent\PageViewService;
use Illuminate\Support\ServiceProvider;

class PageViewServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(PageViewContract::class, PageViewService::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides(): array
    {
        return [PageViewContract::class];
    }
}
