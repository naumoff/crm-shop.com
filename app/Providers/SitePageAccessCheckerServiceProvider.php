<?php

namespace App\Providers;

use App\Contracts\SitePageAccessCheckerContract;
use App\Services\PageAccess\SitePageAccessCheckerService;
use Illuminate\Support\ServiceProvider;

class SitePageAccessCheckerServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->singleton(SitePageAccessCheckerContract::class, SitePageAccessCheckerService::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides(): array
    {
        return [SitePageAccessCheckerContract::class];
    }
}
