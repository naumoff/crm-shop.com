<?php

namespace App\Providers;

use App\Contracts\PageFieldsContract;
use App\Services\PageContent\PageFieldsService;
use Illuminate\Support\ServiceProvider;

class MenuPageFieldsServiceProvider extends ServiceProvider
{
    /** @var bool $defer */
    protected $defer = true;
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->singleton(PageFieldsContract::class, PageFieldsService::class);
    }

    /**
     * @return array
     */
    public function provides(): array
    {
        return [PageFieldsContract::class];
    }
}
