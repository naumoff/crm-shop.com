<?php

namespace App\Providers;

use App\Contracts\UsersCounterContract;
use App\Services\DBCache\UsersCounterRedisService;
use Illuminate\Support\ServiceProvider;

class UsersCounterServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(UsersCounterContract::class, UsersCounterRedisService::class);
    }

    /**
     * Get the services provided by the provider.
     * @return array
     */
    public function provides()
    {
        return [UsersCounterContract::class];
    }
}
