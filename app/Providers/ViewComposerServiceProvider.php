<?php

namespace App\Providers;

use App\Http\ViewComposers\AdministratorSidebarComposer;
use App\Http\ViewComposers\ContentManagerSidebarComposer;
use App\Http\ViewComposers\MenuPageFieldsComposer;
use App\Http\ViewComposers\MenuPageNamesComposer;
use App\Http\ViewComposers\MenuPageSeoComposer;
use App\Http\ViewComposers\ViewLinksComposer;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // adding translatable main menu links under variable $menuPageNames
        View::composer('site.controllers.*', MenuPageNamesComposer::class);

        #region THOSE COMPOSERS WILL WORK FOR MENU PAGES ONLY
        View::composer('site.controllers.menu.*', MenuPageSeoComposer::class);
        View::composer('site.controllers.menu.*', MenuPageFieldsComposer::class);
        #endregion

        View::composer('dashboards.layouts.dashboard', ViewLinksComposer::class);
        View::composer('dashboards.cms.manager.controllers.*', ContentManagerSidebarComposer::class);
        View::composer('dashboards.admin.controllers.*', AdministratorSidebarComposer::class);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
