<?php

namespace App\Providers;

use App\Contracts\DBCacheContract;
use App\Services\DBCache\DBCacheRedisService;
use Illuminate\Support\ServiceProvider;

class DBCacheServiceProvider extends ServiceProvider
{
    /** @var bool $defer */
    protected $defer = true;

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(): void
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->singleton(DBCacheContract::class, DBCacheRedisService::class);
    }

    /**
     * needed for deferring.
     * @return array
     */
    public function provides(): array
    {
        return [DBCacheContract::class];
    }
}
