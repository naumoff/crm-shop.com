<?php

namespace App\Providers;

use App\Contracts\UserIpCheckerContract;
use App\Services\PageAccess\UserIpCheckerService;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class UserIpCheckerServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Register the service provider.
     * @return void
     */
    public function register()
    {
        $this->app->singleton(UserIpCheckerContract::class, UserIpCheckerService::class);
    }
    
    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [UserIpCheckerContract::class];
    }
}
