<?php

namespace App\Models\UserModels;

use App\Contracts\MustVerifyEmailInvitationContract;
use App\Contracts\SlackWebHookContract;
use App\Notifications\VerifyEmailInvitation;
use App\Services\DataHelpers\UserRoles;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail, MustVerifyEmailInvitationContract
{
    use HasApiTokens, Notifiable, SoftDeletes, UserRoles;

    #region PROPERTIES
    /**
     * The attributes that are NO mass assignable.
     * @var array
     */
    protected $guarded = ['email', 'password'];

    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * columns that are transformed to Carbon
     * @var array $dates
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'last_login', 'last_logout'];

    /**
     * The attributes that should be cast to native types.
     * @var array
     */
    protected $casts = [
        'approved' => 'boolean',
    ];
    #endregion

    #region RELATION METHODS
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class)->using(UserRole::class)
            ->withPivot([
                'created_at',
                'updated_at'
            ]);
    }
    #endregion

    #region ATTACH/DETACH ROLE
    /**
     * @param int $roleId
     * @return User
     */
    public function attachRole(int $roleId): self
    {
        if (count($this->roles()->where('roles.id', $roleId)->get()) === 0) {
            $this->roles()->attach($roleId);
            $this->touch();
        }

        return $this;
    }

    /**
     * @param int $roleId
     * @return User
     */
    public function detachRole(int $roleId): self
    {
        $this->roles()->detach($roleId);
        $this->touch();
        return $this;
    }

    /**
     * @param array  int[] $rolesIds
     * @return User
     */
    public function rolesMassUpdate(array $rolesIds): self
    {
        foreach ($rolesIds as $key => $value) {
            if (!in_array($value, array_keys($this->getRealRoles()))) {
                unset($rolesIds[$key]);
            }
        }
        $this->roles()->sync($rolesIds);
        return $this;
    }
    #endregion

    #region NOTIFICATION METHODS
    /**
     * This logic provides ability to send slack messages to different web hooks defined dynamically
     * in different notification classes...
     * @param SlackWebHookContract $notification
     * @return string
     */
    public function routeNotificationForSlack(SlackWebHookContract $notification): string
    {
        return $notification->getSlackWebHook();
    }

    /**
     * @param string $temporaryPassword
     */
    public function sendEmailVerificationInvitation(string $temporaryPassword): void
    {
        $this->notify(new VerifyEmailInvitation($temporaryPassword));
    }
    #endregion

    #region MAIN METHODS
    /** @return bool */
    public function isAdmin(): bool
    {
        return count($this->roles()->where('roles.id', '=', Role::ADMIN)->get()) === 1;
    }

    /** @return bool */
    public function isContentManager(): bool
    {
        return count($this->roles()->where('roles.id', '=', Role::CONTENT_MANAGER)->get()) === 1;
    }

    /** @return bool */
    public function isContentEditor(): bool
    {
        return count($this->roles()->where('roles.id', '=', Role::CONTENT_EDITOR)->get()) === 1;
    }

    /**
     * check if currrent user is approved b administrator
     * @return bool
     */
    public function isApproved(): bool
    {
        return (integer)$this->approved === 1;
    }

    /**
     * @return bool
     */
    public function isVerified(): bool
    {
        return $this->email_verified_at !== null;
    }
    #endregion

    #region SCOPE METHODS
    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeApproved(Builder $builder): Builder
    {
        return $builder->where('approved', true);
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeNotApproved(Builder $builder): Builder
    {
        return $builder->where('approved', false);
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeVerified(Builder $builder): Builder
    {
        return $builder->whereNotNull('email_verified_at');
    }
    #endregion
}
