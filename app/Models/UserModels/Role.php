<?php

namespace App\Models\UserModels;

use Illuminate\Database\Eloquent\Model;
use  Illuminate\Database\Eloquent\Builder;

class Role extends Model
{
    #region CONSTANTS ROLES
    public const ADMIN = 1;
    public const SALES_MANAGER = 2;
    public const SALES_AGENT = 3;
    public const CONTENT_MANAGER = 4;
    public const CONTENT_EDITOR = 5;
    public const CUSTOMER = 6;
    public const SUBSCRIBER = 7;
    public const UNDEFINED = 8;
    #endregion

    #region CLASS PROPERTIES
    protected $table = 'roles';
    protected $guarded = [];
    #endregion

    #region MAIN METHODS
    #endregion

    #region SCOPE METHODS
    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeAdmin(Builder $builder): Builder
    {
        return $builder->where('id', '=', Role::ADMIN);
    }
    #endregion

    #region RELATION METHODS
    public function users()
    {
        return $this->belongsToMany(User::class)->using(UserRole::class)
            ->withPivot([
                'created_at',
                'updated_at'
            ]);
    }
    #endregion
}
