<?php

namespace App\Models\UserModels;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UserRole extends Pivot
{
    #region CLASS PROPERTIES
    /** @var string */
    protected $table = 'role_user';

    /**
     * Indicates if the IDs are auto-incrementing.
     * @var bool
     */
    public $incrementing = true;
    #endregion
}
