<?php

namespace App\Models\PageModels;

use Illuminate\Database\Eloquent\Model;

class MetadataField extends Model
{
    #region CLASS PROPERTIES
    protected $table = 'metadata_fields';
    protected $guarded = [];
    #endregion

    #region MAIN METHODS
    #endregion

    #region SCOPE METHODS
    #endregion

    #region RELATION METHODS
    public function page()
    {
        return $this->belongsTo(Page::class, 'page_id', 'id');
    }
    #endregion
}
