<?php

namespace App\Models\PageModels;

use Illuminate\Database\Eloquent\Model;

class PageAccessGate extends Model
{
    #region CLASS PROPERTIES
    protected $table = 'page_access_gates';
    protected $guarded = [];
    #endregion

    #region MAIN METHODS
    #endregion

    #region SCOPE METHODS
    #endregion

    #region RELATION METHODS
    public function page()
    {
        return $this->belongsTo(Page::class, 'page_id', 'id');
    }
    #endregion
}
