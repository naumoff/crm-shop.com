<?php

namespace App\Models\PageModels;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    #region MENU PAGES CONSTANTS
    /** menu pages ids that are included into main menu and have 'page_type_id' = 1 */
    public const MAIN = 1;
    public const CONTACTS = 2;
    #endregion

    #region CLASS PROPERTIES
    protected $table = 'pages';
    protected $guarded = [];

    /**
     * Custom added values to cached model
     * @var array $appends
     */
    protected $appends = [
        'view_path',
    ];
    #endregion

    #region MAIN METHODS

    #endregion

    #region VIRTUALLY ADDED ATTRIBUTES
    public function getViewPathAttribute()
    {
        if ($this->page_type_id === PageType::SINGLE) {
            return 'site.controllers.page.show';
        } else {
            return null;
        }
    }
    #endregion

    #region SCOPE METHODS
    public function scopeMainPage($query)
    {
        return $query->where('page_type_id', '=', PageType::MENU);
    }
    #endregion

    #region RELATION METHODS
    public function pageAccessGate()
    {
        return $this->hasOne(PageAccessGate::class, 'page_id', 'id');
    }

    public function metadataField()
    {
        return $this->hasOne(MetadataField::class, 'page_id', 'id');
    }

    public function booleanFields()
    {
        return $this->hasMany(BooleanField::class, 'page_id', 'id');
    }

    public function rawHtmlFields()
    {
        return $this->hasMany(RawHtmlField::class, 'page_id', 'id');
    }

    public function textFields()
    {
        return $this->hasMany(TextField::class, 'page_id', 'id');
    }

    public function longTextFields()
    {
        return $this->hasMany(LongTextField::class, 'page_id', 'id');
    }

    public function pageType()
    {
        return $this->belongsTo(PageType::class, 'page_type_id', 'id');
    }
    #endregion
}
