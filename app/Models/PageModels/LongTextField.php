<?php

namespace App\Models\PageModels;

use Illuminate\Database\Eloquent\Model;

class LongTextField extends Model
{
    #region CLASS PROPERTIES
    protected $table = 'long_text_fields';
    protected $guarded = [];
    #endregion

    #region MAIN METHODS
    #endregion

    #region SCOPE METHODS
    #endregion

    #region RELATION METHODS
    public function page()
    {
        $this->belongsTo(Page::class, 'page_id', 'id');
    }
    #endregion
}
