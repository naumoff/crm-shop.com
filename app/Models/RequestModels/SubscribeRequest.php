<?php

namespace App\Models\RequestModels;

use Illuminate\Database\Eloquent\Model;

class SubscribeRequest extends Model
{
    #region CLASS PROPERTIES
    protected $table = 'subscribe_requests';
    protected $guarded = [];
    #endregion

    #region MAIN METHODS
    #endregion

    #region SCOPE METHODS
    #endregion

    #region RELATION METHODS
    #endregion
}
