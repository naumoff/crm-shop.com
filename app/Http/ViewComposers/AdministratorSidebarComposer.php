<?php
/**
 * User: Andrey Naumoff
 * Date: 04-Feb-19
 * Time: 10:35 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Http\ViewComposers;

use App\Contracts\UsersCounterContract;
use App\Services\DataHelpers\UserRoles;
use Illuminate\View\View;

class AdministratorSidebarComposer
{
    use UserRoles;

    #region PROPERTIES
    /** @var UsersCounterContract $usersCounterService */
    private $usersCounterService;
    #endregion

    #region MAIN METHODS
    /**
     * AdministratorSidebarComposer constructor.
     * @param UsersCounterContract $usersCounterService
     */
    public function __construct(UsersCounterContract $usersCounterService)
    {
        $this->usersCounterService = $usersCounterService;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('roles', $this->getRealRoles());
        $view->with('counter', $this->getUserCounter());
    }
    #endregion

    #region SERVICE METHODS
    /**
     * @return array
     */
    private function getUserCounter(): array
    {
        $counters = [
            'all' => $this->usersCounterService->countAll(),
            'without-role' => $this->usersCounterService->countWithoutRole(),
            'not-approved' => $this->usersCounterService->countNotApproved(),
            'not-verified' => $this->usersCounterService->countUnverified(),
            'deleted' => $this->usersCounterService->countDeleted(),
        ];

        return array_merge($counters, $this->usersCounterService->countRoles());
    }
    #endregion
}
