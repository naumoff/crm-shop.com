<?php
/**
 * User: Andrey Naumoff
 * Date: 01-Jan-19
 * Time: 12:09 AM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Http\ViewComposers;

use App;
use App\Contracts\PageMetadataContract;
use App\Services\DataHelpers\MenuPageRoutes;
use Illuminate\Support\Facades\Route;
use Illuminate\View\View;
use stdClass;

/**
 * Class MenuPageSeoComposer
 * @package App\Http\ViewComposers
 */
class MenuPageSeoComposer
{
    use MenuPageRoutes;

    #region PROPERTIES
    /** @var PageMetadataContract $pageMetadataService */
    private $pageMetadataService;
    #endregion

    #region MAIN METHODS
    /**
     * MenuPageSeoComposer constructor.
     * @param PageMetadataContract $pageMetadataService
     */
    public function __construct(PageMetadataContract $pageMetadataService)
    {
        $this->pageMetadataService = $pageMetadataService;
    }

    /**
     * @param View $view
     */
    public function compose(View $view): void
    {
        if (in_array(Route::currentRouteName(), $this->menuPageRoutes)) {
            $view->with('seo', $this->setAndReturnSeoObject());
        }
    }
    #endregion

    #region SERVICE METHODS
    /**
     * @return stdClass
     */
    private function setAndReturnSeoObject(): stdClass
    {
        /** @var string $locale */
        $locale = App::getLocale();

        /** @var integer $menuPageId */
        $menuPageId = $this->getMenuPageIdFromRoute();

        /** @var stdClass $seo */
        $seo = $this->pageMetadataService->getMenuPageMetadata($menuPageId, $locale);
        return $seo;
    }
    #endregion
}
