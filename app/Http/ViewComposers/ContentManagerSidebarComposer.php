<?php
/**
 * User: Andrey Naumoff
 * Date: 17-Dec-18
 * Time: 10:33 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Http\ViewComposers;

use App\Contracts\DBCacheContract;
use App\Contracts\ModelObserverContract;
use App\Models\PageModels\PageType;
use Illuminate\View\View;

class ContentManagerSidebarComposer
{
    #region PROPERTIES
    /** @var DBCacheContract $dbCacheService */
    private $dbCacheService;
    #endregion

    #region MAIN METHODS
    /**
     * ContentManagerSidebarComposer constructor.
     * @param DBCacheContract $dbCacheService
     */
    public function __construct(DBCacheContract $dbCacheService)
    {
        $this->dbCacheService = $dbCacheService;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('menuPages', $this->getMenuPages());
        $view->with('regularPageTypes', $this->getRegularPageTypesList());
    }
    #endregion

    #region SERVICE METHODS
    /**
     * @return array
     */
    private function getMenuPages(): array
    {
        return $this->dbCacheService->getModelsByKey(ModelObserverContract::PAGE_MENU_KEY);
    }

    /**
     * @return array
     */
    private function getRegularPageTypesList(): array
    {
        $returnResult = [];
        foreach ($this->dbCacheService->getModelsByKey(ModelObserverContract::PAGE_TYPE_KEY) as $pageType) {
            if ($pageType->id === PageType::MENU) {
                continue;
            }
            $returnResult[] = $pageType;
        }
        return $returnResult;
    }
    #endregion
}
