<?php
/**
 * User: Andrey Naumoff
 * Date: 14-Jan-19
 * Time: 10:16 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Http\ViewComposers;

use App\Contracts\PageMetadataContract;
use Illuminate\Support\Facades\App;
use Illuminate\View\View;

class MenuPageNamesComposer
{
    #region PROPERTIES
    /** @var PageMetadataContract $pageMetadataService */
    private $pageMetadataService;

    /** @var string $locale */
    private $locale;
    #nedregion

    #region MAIN METHODS
    /**
     * MenuPageNamesComposer constructor.
     * @param PageMetadataContract $pageMetadataService
     */
    public function __construct(PageMetadataContract $pageMetadataService)
    {
        $this->pageMetadataService = $pageMetadataService;
    }

    /**
     * @param View $view
     */
    public function compose(View $view): void
    {
        $this->locale = App::getLocale();
        $view->with('menuPageNames', $this->pageMetadataService->getMenuPagesNames($this->locale));
    }
    #endregion
}
