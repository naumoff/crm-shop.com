<?php

namespace App\Http\Middleware;

use App\Contracts\UserIpCheckerContract;
use Closure;

class WhiteIpGate
{
    /** @var UserIpCheckerContract $userIpCheckerService */
    private $userIpCheckerService;
    
    /**
     * WhiteIpGate constructor.
     * @param UserIpCheckerContract $ipCheckerContract
     */
    public function __construct(UserIpCheckerContract $ipCheckerContract)
    {
        $this->userIpCheckerService = $ipCheckerContract;
    }
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userFromWhiteIp = $this->userIpCheckerService->checkUserIp();
        if ($userFromWhiteIp) {
            return $next($request);
        }
        return redirect('home');
    }
}
