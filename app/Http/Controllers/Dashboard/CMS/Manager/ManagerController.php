<?php

namespace App\Http\Controllers\Dashboard\CMS\Manager;

use App\Contracts\DBCacheContract;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ManagerController extends Controller
{
    #region PROPERTIES
    /** DBCacheContract @var DBCacheContract */
    protected $dbCacheService;
    #endregion

    #region MAIN METHODS
    public function __construct(DBCacheContract $dbCacheService)
    {
        $this->dbCacheService = $dbCacheService;
    }

    /**
     * @return Factory|View
     */
    public function welcome()
    {
        return view('dashboards.cms.manager.controllers.manager.welcome');
    }
}
