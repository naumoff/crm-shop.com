<?php

namespace App\Http\Controllers\Dashboard\CMS\Manager;

use App\Http\Controllers\Controller;
use App\Http\Requests\MenuPageAccessPatch;
use App\Models\PageModels\PageAccessGate;
use App\Services\Converters\DashConverters;
use Illuminate\Http\Request;

class MenuPageAccessController extends Controller
{
    use DashConverters;

    #region MAIN METHODS
    /**
     * @param MenuPageAccessPatch $accessPatch
     * @param PageAccessGate $pageAccessGate
     */
    public function menuPageAccessUpdate(MenuPageAccessPatch $accessPatch, PageAccessGate $pageAccessGate): void
    {
        $roleColumn = $this->dashesToUnderscore($accessPatch->validated()['role']);
        $pageAccessGate->$roleColumn = ($pageAccessGate->$roleColumn)? 0:1;
        $pageAccessGate->save();
        $pageAccessGate->page->touch();
    }
    #endregion
}
