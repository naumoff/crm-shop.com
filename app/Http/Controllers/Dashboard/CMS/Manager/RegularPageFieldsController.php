<?php

namespace App\Http\Controllers\Dashboard\CMS\Manager;

use App\Contracts\LocaleContract;
use App\Http\Requests\RegularPageRawHtmlPatch;
use App\Models\PageModels\RawHtmlField;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use stdClass;

class RegularPageFieldsController extends ManagerController
{

    /**
     * @param stdClass $pageType
     * @return Factory|View
     */
    public function show(stdClass $pageType)
    {
        return view('dashboards.cms.manager.controllers.regular_page_fields.show', ['pageType' => $pageType]);
    }

    /**
     * @param string $pageTypeName
     * @param string $pageName
     * @return Factory|View
     */
    public function fieldsEdit(string $pageTypeName, string $pageName)
    {
        $key = 'page:' . $pageTypeName;
        $cachedPageModel = $this->dbCacheService->getModelByKeyAndId($key, $pageName);
        return view(
            'dashboards.cms.manager.controllers.regular_page_fields.fields_edit',
            ['pageModel' => $cachedPageModel]
        );
    }

    /**
     * @param RegularPageRawHtmlPatch $request
     * @return RedirectResponse
     */
    public function rawHtmlUpdate(RegularPageRawHtmlPatch $request): RedirectResponse
    {
        $rawHtmlFieldId = $request->input('field_id');
        $rawHtml = RawHtmlField::find($rawHtmlFieldId);
        foreach (LocaleContract::AVAILABLE_LOCALES as $locale) {
            $rawHtml->$locale = $request->input($locale);
        }
        $rawHtml->save();
        $rawHtml->page->touch();
        return redirect()->back();
    }
}
