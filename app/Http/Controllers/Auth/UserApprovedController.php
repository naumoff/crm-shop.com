<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserApprovedController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function checkIfUserApproved(Request $request)
    {
        if ($request->user()->isApproved()) {
            return redirect('panel');
        } else {
            return view('auth.confirmation-stand-by');
        }
    }
}
