<?php

namespace App\Http\Controllers\Front;

use App\Contracts\LocaleContract;
use App\Contracts\PageViewContract;
use LogRec;
use stdClass;

class PageController extends FrontController
{
    #region PROPERTIES
    /** @var PageViewContract $pageViewService */
    private $pageViewService;
    #endregion

    #region MAIN METHODS
    /**
     * PageController constructor.
     * @param LocaleContract $localeService
     * @param PageViewContract $pageViewService
     */
    public function __construct(LocaleContract $localeService, PageViewContract $pageViewService)
    {
        parent::__construct($localeService);
        $this->pageViewService = $pageViewService;
    }

    /**
     * @param null|string $locale
     * @param stdClass $cachedModel
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(?string $locale, stdClass $cachedModel)
    {
        try {
            $view = view(
                $cachedModel->view_path,
                $this->pageViewService->fetchVariablesArray($cachedModel, $this->locale)
            );
            return $view;
        } catch (\Throwable $throwable) {
            LogRec::error($throwable->getMessage());
            return redirect()->route('not-found', ['locale' => $this->locale]);
        }
    }
    #endregion
}
