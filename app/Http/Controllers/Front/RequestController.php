<?php

namespace App\Http\Controllers\Front;

use App\Events\ContactRequestSubmitted;
use App\Events\SubscriptionRequestSubmitted;
use App\Http\Requests\ContactRequestPost;
use App\Http\Requests\SubscriptionRequestPost;
use App\Models\RequestModels\ContactRequest;
use App\Models\RequestModels\SubscribeRequest;
use App\Services\Translation\LocaleValidator;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Storage;
use LogRec;

class RequestController extends FrontController
{
    use LocaleValidator;

    #region MAIN METHODS
    /**
     * @param ContactRequestPost $request
     * @param string $locale
     * @return string
     */
    public function storeContactRequest(ContactRequestPost $request, string $locale): string
    {
        $input = $request->validated();
        try {
            $contactRequest = new ContactRequest();
            $contactRequest->user_id = (empty(\Auth::user()))? null : \Auth::user()->id;
            $contactRequest->name = $input['contact_name'];
            $contactRequest->subject = $input['contact_subject'];
            $contactRequest->message = $input['contact_message'];
            $contactRequest->email = $input['contact_email'];
            $contactRequest->locale = $this->validateAndGetLocale($locale);
            $contactRequest->save();
            if (!empty($input['contact_file'])) {
                $this->saveFileToDisk($input['contact_file'], $contactRequest);
            }
            event(new ContactRequestSubmitted($contactRequest));
            return json_encode(['status' => 201, 'success' => trans('strings.request_submit_success_message')]);
        } catch (\Throwable $throwable) {
            LogRec::error($throwable->getMessage());
            return Response::json([
                'status'      =>  501,
                'message'   =>  trans('strings.request_unknown_error_message'),
            ], 501);
        }
    }

    /**
     * @param SubscriptionRequestPost $request
     * @param string $locale
     * @return string
     */
    public function storeSubscriptionRequest(SubscriptionRequestPost $request, string $locale): string
    {
        $validatedInput = $request->validated();

        try {
            $subscribeRequest = new SubscribeRequest();
            $subscribeRequest->user_id = (empty(\Auth::user()))? null : \Auth::user()->id;
            $subscribeRequest->email = $validatedInput['email'];
            $subscribeRequest->locale = $this->validateAndGetLocale($locale);
            $subscribeRequest->save();
            event(new SubscriptionRequestSubmitted($subscribeRequest));
            return json_encode(['status' => 201, 'message' => trans('strings.subscribe_success_message')]);
        } catch (\Throwable $throwable) {
            LogRec::error($throwable->getMessage());
            return Response::json([
                'status'      =>  501,
                'message'   =>  trans('strings.subscribe_unknown_error_message'),
            ], 501);
        }
    }

    /**
     * @return HttpResponse
     */
    public function getCV(): HttpResponse
    {
        $filename = 'app/site/cv.pdf';
        $path = storage_path($filename);
        return Response::make(file_get_contents($path), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="'.$filename.'"'
        ]);
    }
    #endregion

    #region SERVICE METHODS
    /**
     * @param UploadedFile $fileInput
     * @param ContactRequest $contactRequest
     */
    private function saveFileToDisk(UploadedFile $fileInput, ContactRequest $contactRequest): void
    {
        $path = Storage::disk('private')->putFile("{$contactRequest->id}", $fileInput);
        $contactRequest->path_to_file = $path;
        $contactRequest->save();
    }
    #endrgion
}
