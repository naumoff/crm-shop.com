<?php

namespace App\Http\Controllers\Front;

use App\Contracts\LocaleContract;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class FrontController extends Controller
{
    #region PROPERTIES
    /** @var string $locale */
    protected $locale;
    #endregion

    #region MAIN METHODS
    /**
     * MenuController constructor.
     * @param LocaleContract $localeService
     */
    public function __construct(LocaleContract $localeService)
    {
        $this->locale = $localeService->setLocale()->getLocale();
        Cookie::queue(Cookie::make('locale', $this->locale, 90));
    }
    #endregion
}
