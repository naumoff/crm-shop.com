<?php

namespace App\Http\Controllers\Front;

use App\Contracts\SitePageAccessContract;
use App\Models\PageModels\Page;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Gate;
use Illuminate\View\View;
use LogRec;

class MenuController extends FrontController
{
    #region MAIN METHODS
    public function index()
    {
        $view = view('site.controllers.menu.index');
        return $this->passTroughViewRight(Page::MAIN, $view);
    }

    public function contacts()
    {
        $view = view('site.controllers.menu.contacts');
        return $this->passTroughViewRight(Page::CONTACTS, $view);
    }

    public function notFoundPage()
    {
        return view('site.controllers.menu.not_found_page');
    }
    #endregion

    #region SERVICE METHODS

    /**
     * @param int $pageId
     * @param View $view
     * @return RedirectResponse|View
     */
    private function passTroughViewRight(int $pageId, View $view)
    {
        try {
            if (Gate::allows(SitePageAccessContract::VIEW_MENU_PAGE, $pageId)) {
                return $view;
            } else {
                return redirect()->route('not-found', ['locale' => $this->locale]);
            }
        } catch (\Throwable $throwable) {
            LogRec::error($throwable->getMessage());
            return redirect()->route('not-found', ['locale' => $this->locale]);
        }
    }
    #endregion
}
