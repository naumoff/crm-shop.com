<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class ContactRequestPost
 * @package App\Http\Requests
 */
class ContactRequestPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contact_name' => 'required|min:3',
            'contact_email' => 'required|email',
            'contact_subject' => 'required|min:3',
            'contact_file' => 'nullable|file|max:10000|mimes:pdf,doc,docx,xls,xlsx,ppt,pptx,jpeg,bmp,png',
            'contact_message' => 'required|min:15'
        ];
    }
}
