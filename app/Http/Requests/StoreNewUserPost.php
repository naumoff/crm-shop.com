<?php

namespace App\Http\Requests;

use App\Services\DataHelpers\UserRoles;
use Illuminate\Foundation\Http\FormRequest;

class StoreNewUserPost extends FormRequest
{
    use UserRoles;

    #region MAIN METHODS
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $userValidators = [
            'userName' => 'required',
            'userEmail' => 'required|unique:users,email',
        ];
        return array_merge($userValidators, $this->getRolesValidators());
    }

    /**
     * Get the error messages for the defined validation rules.
     * @return array
     */
    public function messages(): array
    {
        return $this->getRolesCheatMessages();
    }
    #endregion

    #region SERVICE METHODS
    /**
     * @return array
     */
    private function getRolesValidators(): array
    {
        $rolesValidators = [];
        foreach ($this->getRealRoles() as $roleId => $realRole) {
            $rolesValidators[$realRole] = 'nullable|integer|size:'.$roleId;
        }
        return $rolesValidators;
    }

    /**
     * @return array
     */
    private function getRolesCheatMessages(): array
    {
        $rolesCheatMessages = [];
        foreach ($this->getRealRoles() as $role) {
            $rolesCheatMessages[$role.'.size'] = 'do not try to cheat this form';
        }
        return $rolesCheatMessages;
    }
    #endregion
}
