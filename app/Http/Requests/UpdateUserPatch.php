<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserPatch extends FormRequest
{
    #region MAIN METHODS
    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        $userId = $this->request->all()['userId'];

        return [
            'userId' => 'required|exists:users,id',
            'userName' => 'required',
            'userEmail' => 'required|email|unique:users,email,' . $userId,
            'userPhone' => 'nullable|min:7',
            'userLocation' => 'nullable|min:3',
            'userAddress' => 'nullable|min:3',
            'userApproved' => 'required|in:true,false',
            'userRoles' => 'array',
        ];
    }
    #endregion
}
