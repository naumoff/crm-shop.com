<?php

namespace App\Http\Requests;

use App\Services\DataHelpers\UserRoles;
use Illuminate\Foundation\Http\FormRequest;

class MenuPageAccessPatch extends FormRequest
{
    use UserRoles;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'role' => 'required|in:'.implode(',', $this->roleNames),
            'page_id' => 'required|numeric|exists:pages,id',
        ];
    }
}
