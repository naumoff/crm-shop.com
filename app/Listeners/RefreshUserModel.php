<?php

namespace App\Listeners;

use App\Events\RefreshCachedModels;
use App\Models\UserModels\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RefreshUserModel
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param RefreshCachedModels $event
     */
    public function handle(RefreshCachedModels $event)
    {
        $dbCacheService = $event->DBCacheService;
        $dbCacheService->refreshAllModels(User::class);
    }
}
