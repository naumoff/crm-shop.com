<?php

namespace App\Listeners;

use App\Events\RefreshCachedModels;
use App\Models\PageModels\Page;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RefreshPageModel
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param RefreshCachedModels $event
     */
    public function handle(RefreshCachedModels $event)
    {
        $dbCacheService = $event->DBCacheService;
        $dbCacheService->refreshAllModels(Page::class);
    }
}
