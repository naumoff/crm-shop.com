<?php

namespace App\Listeners;

use App\Events\SubscriptionRequestSubmitted;
use App\Models\RequestModels\SubscribeRequest;
use App\Notifications\SubscriptionRequestSubmitted as SubscribeRequestSubmissionNotifier;
use App\Services\NotificationHelpers\AdminReceptors;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;
use LogRec;

class SubscriptionRequestNotification implements ShouldQueue
{
    use AdminReceptors;

    #region PROPERTIES
    /**
     * The number of times the job may be attempted.
     * @var int
     */
    public $tries = 5;
    #endregion


    #region MAIN METHODS
    /**
     * @param SubscriptionRequestSubmitted $event
     */
    public function handle(SubscriptionRequestSubmitted $event): void
    {
        /** @var Collection|null $receptors */
        $receptors = $this->getReceptors();
        if (!empty($receptors)) {
            Notification::send($receptors, new SubscribeRequestSubmissionNotifier($event->subscribeRequest));
        } else {
            LogRec::error($this->getErrorMessage('no receptors are set for notification', $event));
        }
    }

    /**
     * @param SubscriptionRequestSubmitted $event
     * @param $exception
     */
    public function failed(SubscriptionRequestSubmitted $event, $exception): void
    {
        LogRec::error($this->getErrorMessage($exception->getMessage(), $event));
    }
    #endregion

    #region SERVICE METHODS
    /**
     * @param string $message
     * @param SubscriptionRequestSubmitted $event
     * @return array
     */
    private function getErrorMessage(string $message, SubscriptionRequestSubmitted $event): array
    {
        /** @var SubscribeRequest $contactRequest */
        $subscriptionRequest = $event->subscribeRequest;
        $message = [
            'error' => $message,
            'action' => 'subscription request submission',
            'email' => $subscriptionRequest->email,
            'locale' => $subscriptionRequest->locale,
        ];
        return $message;
    }
    #endregion
}
