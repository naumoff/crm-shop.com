<?php
/**
 * User: Andrey Naumoff
 * Date: 14-Feb-19
 * Time: 10:50 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Listeners;

use App\Contracts\MustVerifyEmailInvitationContract;
use App\Events\NewUserInvited;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailVerificationInvitation implements ShouldQueue
{
    /**
     * @param NewUserInvited $event
     */
    public function handle(NewUserInvited $event)
    {
        if ($event->user instanceof MustVerifyEmailInvitationContract && ! $event->user->hasVerifiedEmail()) {
            $event->user->sendEmailVerificationInvitation($event->temporaryPassword);
        }
    }
}
