<?php

namespace App\Listeners;

use App\Events\ContactRequestSubmitted;
use App\Notifications\ContactRequestSubmitted as ContactRequestSubmissionNotifier;
use App\Models\RequestModels\ContactRequest;
use App\Services\NotificationHelpers\AdminReceptors;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Notification;
use LogRec;

class ContactRequestNotification implements ShouldQueue
{
    use AdminReceptors;

    #region PROPERTIES
    /**
     * The number of times the job may be attempted.
     * @var int
     */
    public $tries = 5;
    #endregion

    #region MAIN METHODS
    /**
     * @param ContactRequestSubmitted $event
     */
    public function handle(ContactRequestSubmitted $event): void
    {
        /** @var Collection|null $receptors */
        $receptors = $this->getReceptors();
        if (!empty($receptors)) {
            Notification::send($receptors, new ContactRequestSubmissionNotifier($event->contactRequest));
        } else {
            LogRec::error($this->getErrorMessage('no receptors are set for notification', $event));
        }
    }

    /**
     * @param ContactRequestSubmitted $event
     * @param $exception
     */
    public function failed(ContactRequestSubmitted $event, $exception): void
    {
        LogRec::error($this->getErrorMessage($exception->getMessage(), $event));
    }
    #endregion

    #region SERVICE METHODS
    /**
     * @param string $message
     * @param ContactRequestSubmitted $event
     * @return array
     */
    private function getErrorMessage(string $message, ContactRequestSubmitted $event): array
    {
        /** @var ContactRequest $contactRequest */
        $contactRequest = $event->contactRequest;
        $message = [
            'error' => $message,

            'action' => 'contact request submission',
            'locale' => $contactRequest->locale,
            'name' => $contactRequest->name,
            'email' => $contactRequest->email,
            'subject' => $contactRequest->subject,
            'message' => $contactRequest->message,
        ];
        return $message;
    }
    #endregion
}
