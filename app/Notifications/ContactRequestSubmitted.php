<?php

namespace App\Notifications;

use App\Contracts\SlackWebHookContract;
use App\Models\RequestModels\ContactRequest;
use App\Models\UserModels\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ContactRequestSubmitted extends Notification implements ShouldQueue, SlackWebHookContract
{
    use Queueable;

    #region PROPERTIES
    /** @var ContactRequest $contactRequest */
    private $contactRequest;

    /** @var int $messageCounter */
    private $messageCounter = 0;
    #endregion

    #region MAIN METHODS

    /**
     * ContactRequestSubmitted constructor.
     * @param ContactRequest $contactRequest
     */
    public function __construct(ContactRequest $contactRequest)
    {
        $this->contactRequest = $contactRequest;
    }

    /**
     * Get the notification's delivery channels.
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable): array
    {
        $this->messageCounter += 1;
        if ($this->messageCounter === 1) {
            return ['mail', 'database', 'slack']; //send notification to slack and database only one time
        } else {
            return ['mail'];
        }
    }

    /**
     * Get the mail representation of the notification.
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable): MailMessage
    {
        list($user, $email, $subject, $message, $preferredLocale, $fileAttached) = $this->getContactDetails();
        return (new MailMessage)
            ->subject('Contact Request Submitted')
            ->markdown(
                'mail.contact.request.submitted',
                [
                    'locale' => $preferredLocale,
                    'user' => $user,
                    'email' => $email,
                    'subject' => $subject,
                    'message' => $message,
                    'fileAttached' => $fileAttached,
                ]
            );
    }

    /**
     * @param mixed $notifiable
     * @return array
     */
    public function toDatabase($notifiable): array
    {
        list($user, $email, $subject, $message, $preferredLocale, $fileAttached) = $this->getContactDetails();
        return [
            'locale' => $preferredLocale,
            'user' => $user,
            'email' => $email,
            'subject' => $subject,
            'message' => $message,
            'fileAttached' => $fileAttached,
        ];
    }

    /**
     * @param $notifiable
     * @return SlackMessage
     */
    public function toSlack($notifiable): SlackMessage
    {
        list($user, $email, $subject, $message, $preferredLocale, $fileAttached) = $this->getContactDetails(true);
        return (new SlackMessage)
            ->from($user, ':ghost:')
            ->content('Contact request submitted:')
            ->attachment(function ($attachment) use ($email, $subject, $message, $preferredLocale, $fileAttached) {
                $attachment->content('Message: ' . $message)
                    ->fields([
                        'email: ' => $email,
                        'subject: ' => $subject,
                        'preferred locale: ' => $preferredLocale,
                        'file attached:' => $fileAttached,
                    ]);
            });
    }

    /**
     * This method called in User model User::routeNotificationForSlack()
     * and provides ability to send message to different web hooks depending on each Notification class
     * @return string
     */
    public function getSlackWebHook(): string
    {
        return config('slack.web_hooks.contact_request');
    }

    /**
     * Get the array representation of the notification.
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
    #endregion

    #region SERVICE METHODS
    /**
     * @param bool $shortUserName
     * @return array
     */
    private function getContactDetails($shortUserName = false): array
    {
        if ($this->contactRequest->user_id === null) {
            $user = $this->contactRequest->contact_name;
        } else {
            /** @var User $requestedUser */
            $requestedUser = User::find($this->contactRequest->user_id);
            if ($shortUserName === false) {
                $user = ucfirst($requestedUser->name) . ', registered with email ' . $requestedUser->email . ', ';
            } else {
                $user = ucfirst($requestedUser->name);
            }
        }
        $email = $this->contactRequest->email;
        $subject = $this->contactRequest->subject;
        $message = $this->contactRequest->message;
        $preferredLocale = $this->contactRequest->locale;
        $fileAttached = $this->contactRequest->path_to_file ?? false;

        return [$user, $email, $subject, $message, $preferredLocale, $fileAttached];
    }
    #endregion
}
