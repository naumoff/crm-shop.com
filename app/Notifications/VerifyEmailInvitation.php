<?php
/**
 * User: Andrey Naumoff
 * Date: 14-Feb-19
 * Time: 11:06 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\URL;

class VerifyEmailInvitation extends Notification implements ShouldQueue
{
    use Queueable;

    #region PROPERTIES
    /** @var string $temporaryPassword */
    private $temporaryPassword;
    #endregion

    #region MAIN METHODS
    /**
     * VerifyEmailInvitation constructor.
     * @param string $temporaryPassword
     */
    public function __construct(string $temporaryPassword)
    {
        $this->temporaryPassword = $temporaryPassword;
    }

    /**
     * @return array
     */
    public function via(): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('invitation to join '. config('app.name'))
            ->greeting('Hello ' . $notifiable->name . ',')
            ->line('You are invited by administrator of ' . config('app.name') . ' to register in our application.')
            ->line('To confirm your agreement please follow the provided link and login with these credentials:')
            ->line('Login: ' . $notifiable->email)
            ->line('Password: ' . $this->temporaryPassword)
            ->action('Email Verification', $this->verificationUrl($notifiable))
            ->line('Thank you for using our application!');
    }
    #endregion

    #region SERVICE METHODS
    /**
     * Get the verification URL for the given notifiable.
     *
     * @param  mixed  $notifiable
     * @return string
     */
    protected function verificationUrl($notifiable)
    {
        return URL::temporarySignedRoute(
            'verification.verify',
            Carbon::now()->addMinutes(60),
            ['id' => $notifiable->getKey()]
        );
    }
    #endregion
}
