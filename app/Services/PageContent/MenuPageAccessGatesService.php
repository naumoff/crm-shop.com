<?php
/**
 * User: Andrey Naumoff
 * Date: 07-Jan-19
 * Time: 10:30 AM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Services\PageContent;

use App\Contracts\DBCacheContract;
use App\Contracts\MenuPageAccessGatesContract;
use App\Contracts\ModelObserverContract;
use App\Services\Converters\DashConverters;
use App\Services\DataHelpers\UserRoles;
use stdClass;

class MenuPageAccessGatesService implements MenuPageAccessGatesContract
{
    use UserRoles, DashConverters;

    #region PROPERTIES
    private $dbCacheService;
    #endregion

    #region MAIN METHODS
    /**
     * MenuPageAccessGatesService constructor.
     * @param DBCacheContract $dbCacheService
     */
    public function __construct(DBCacheContract $dbCacheService)
    {
        $this->dbCacheService = $dbCacheService;
    }

    /**
     * get allowable roles ids for menu page
     * @param int $pageId
     * @return array
     */
    public function getAllowableRolesIds(int $pageId): array
    {
        $cachedPage = $this->dbCacheService->getModelByKeyAndId(ModelObserverContract::PAGE_MENU_KEY, $pageId);
        return $this->fetchAllowableRolesIds($cachedPage->page_access_gate);
    }
    #endrgion

    #region SERVICE METHODS
    /**
     * @param null|stdClass $pageAccessObject
     * @return array
     */
    private function fetchAllowableRolesIds(?stdClass $pageAccessObject): array
    {
        $allowableRolesIds = [];
        if (empty($pageAccessObject)) {
            return $allowableRolesIds;
        }
        foreach ($this->roleNames as $roleId => $roleName) {
            $roleName = $this->dashesToUnderscore($roleName);
            if (!empty($pageAccessObject->$roleName)) {
                $allowableRolesIds[] = $roleId;
            }
        }
        return $allowableRolesIds;
    }
    #endregion
}
