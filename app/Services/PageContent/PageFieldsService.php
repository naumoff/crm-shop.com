<?php
/**
 * User: Andrey Naumoff
 * Date: 01-Jan-19
 * Time: 2:37 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Services\PageContent;

use App\Contracts\DBCacheContract;
use App\Contracts\PageFieldsContract;
use App\Contracts\ModelObserverContract;
use App\Services\DataHelpers\MenuPageRoutes;
use App\Services\Translation\LocaleValidator;
use stdClass;

/**
 * Class PageFieldsService
 * Used on site only (not in dashboards)
 * @package App\Services\PageContent
 */
class PageFieldsService implements PageFieldsContract
{
    use LocaleValidator, MenuPageRoutes;

    #region CLASS PROPERTIES
    /** @var DBCacheContract $dbCacheService */
    private $dbCacheService;
    #endregion

    #region MAIN METHODS
    /**
     * PageFieldsService constructor.
     * @param DBCacheContract $dbCacheService
     */
    public function __construct(DBCacheContract $dbCacheService)
    {
        $this->dbCacheService = $dbCacheService;
    }

    /**
     * @param string $locale
     * @return stdClass
     */
    public function getMenuPageFields(string $locale): stdClass
    {
        /** @var string $locale */
        $locale = $this->validateAndGetLocale($locale);

        /** @var integer $menuPageId */
        $menuPageId = $this->getMenuPageIdFromRoute();

        /** @var stdClass $page */
        $page = $this->dbCacheService->getModelByKeyAndId(ModelObserverContract::PAGE_MENU_KEY, $menuPageId);

        return $this->fetchPageFields($page, $locale);
    }

    /**
     * @param stdClass $page
     * @return stdClass
     */
    public function getRegularPageFields(stdClass $page, string $locale): stdClass
    {
        /** @var string $locale */
        $locale = $this->validateAndGetLocale($locale);

        return $this->fetchPageFields($page, $locale);
    }
    #endregion

    #region SERVICE METHODS
    /**
     * @param stdClass $page
     * @param string $locale
     * @return stdClass
     */
    private function fetchPageFields(stdClass $page, string $locale): stdClass
    {
        $fields = [];
        $fields['booleans'] = $this->fetchBooleanFields($page);
        $fields['htmls'] = $this->fetchRawHtmlFields($page, $locale);
        return (object) $fields;
    }

    /**
     * @param stdClass $page
     * @return stdClass
     */
    private function fetchBooleanFields(stdClass $page): stdClass
    {
        $booleans = [];
        foreach ($page->boolean_fields as $object) {
            $booleans[$object->name] = $object->value;
        }
        return (object) $booleans;
    }

    /**
     * @param stdClass $page
     * @param string $locale
     * @return stdClass
     */
    private function fetchRawHtmlFields(stdClass $page, string $locale): stdClass
    {
        $rawHtmls = [];
        foreach ($page->raw_html_fields as $object) {
            $rawHtmls[$object->name] = $object->$locale;
        }
        return (object) $rawHtmls;
    }
    #endregion
}
