<?php
/**
 * User: Andrey Naumoff
 * Date: 14-Jan-19
 * Time: 10:42 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Services\PageContent;

use App\Contracts\PageFieldsContract;
use App\Contracts\PageMetadataContract;
use App\Contracts\PageViewContract;
use stdClass;

class PageViewService implements PageViewContract
{
    #region PROPERTIES
    /** @var PageMetadataContract $pageMetadataService */
    private $pageMetadataService;

    /** @var PageFieldsContract $pageFieldsService */
    private $pageFieldsService;
    #endregion

    #region MAIN METHODS
    /**
     * PageViewService constructor.
     * @param PageMetadataContract $pageMetadataService
     * @param PageFieldsContract $pageFieldsService
     */
    public function __construct(PageMetadataContract $pageMetadataService, PageFieldsContract $pageFieldsService)
    {
        $this->pageMetadataService = $pageMetadataService;
        $this->pageFieldsService = $pageFieldsService;
    }

    /**
     * this method takes cached page model and returns array ['variableName' => 'variableValue'] for view files
     * @param stdClass $cachedPageModel
     * @param string $locale
     * @return array
     */
    public function fetchVariablesArray(stdClass $cachedPageModel, string $locale): array
    {
        // this variable used in language switcher
        $returnArray['pageName'] = $cachedPageModel->name;

        // this variable used  in page meta tags section
        $returnArray['seo'] = $this->pageMetadataService->getRegularPageMetadata($cachedPageModel, $locale);

        // this variable contains page fields
        $returnArray['fields'] = $this->pageFieldsService->getRegularPageFields($cachedPageModel, $locale);

        return $returnArray;
    }
    #endregion
}
