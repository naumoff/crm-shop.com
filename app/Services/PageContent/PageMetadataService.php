<?php
/**
 * User: Andrey Naumoff
 * Date: 23-Dec-18
 * Time: 1:52 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Services\PageContent;

use App\Contracts\DBCacheContract;
use App\Contracts\LocaleContract;
use App\Contracts\ModelObserverContract;
use App\Contracts\PageMetadataContract;
use App\Services\DataHelpers\MenuPageRoutes;
use App\Services\Translation\LocaleValidator;
use LogRec;
use stdClass;

class PageMetadataService implements PageMetadataContract
{
    use MenuPageRoutes, LocaleValidator;

    #region PROPERTIES
    /** @var DBCacheContract $cacheService */
    private $cacheService;
    #endregion

    #region MAIN METHODS
    /**
     * PageMetadataService constructor.
     * @param DBCacheContract $cacheService
     */
    public function __construct(DBCacheContract $cacheService)
    {
        $this->cacheService = $cacheService;
    }

    /**
     * @param int $pageId
     * @param string $locale
     * @return stdClass
     */
    public function getMenuPageMetadata(int $pageId, string $locale): stdClass
    {
        $locale = $this->validateAndGetLocale($locale);
        $page = $this->cacheService->getModelByKeyAndId(ModelObserverContract::PAGE_MENU_KEY, $pageId);
        $seo = $this->fetchSeoObject($page, $locale);
        return $seo;
    }

    /**
     * @param stdClass $page
     * @param string $locale
     * @return stdClass
     */
    public function getRegularPageMetadata(stdClass $page, string $locale): stdClass
    {
        $locale = $this->validateAndGetLocale($locale);
        $seo = $this->fetchSeoObject($page, $locale);
        return $seo;
    }

    /**
     * Menu Page Names are used in view composer to translate menu names
     * @param string $locale
     * @return stdClass
     */
    public function getMenuPagesNames(string $locale): stdClass
    {
        $locale = $this->validateAndGetLocale($locale);
        $menuPagesHeaders = [];
        foreach ($this->convertMenuPageRoutesToVarsArray() as $id => $menuPageVar) {
            $menuPagesHeaders[$menuPageVar] = $this->getMenuPageMetadata($id, $locale)->header;
        }
        return (object)$menuPagesHeaders;
    }
    #endregion

    #region SERVICE METHODS
    /**
     * @param stdClass $page
     * @param string $locale
     * @return stdClass
     */
    private function fetchSeoObject(stdClass $page, string $locale): stdClass
    {
        $rawSeo = $page->metadata_field;
        if ($rawSeo === null) {
            if (config('app.env') === 'production') {
                LogRec::error("page {$page->name} has no meta tags at all extremely needed for SEO!");
            }
            return (object) [
                'header' => null,
                'title' => null,
                'description' => null,
                'keywords' => null,
            ];
        } else {
            return (object) [
                'header' => $this->getMetaTag($page, $locale, 'header'),
                'title' => $this->getMetaTag($page, $locale, 'title'),
                'description' => $this->getMetaTag($page, $locale, 'description'),
                'keywords' => $this->getMetaTag($page, $locale, 'keywords')
            ];
        }
    }

    /**
     * @param stdClass $page
     * @param string $locale
     * @param string $metaTag
     * @return null|string
     */
    private function getMetaTag(stdClass $page, string $locale, string $metaTag): ?string
    {
        $rawSeo = $page->metadata_field;
        $field = $metaTag . '_' . $locale;
        $defaultField = $metaTag . '_' . LocaleContract::AVAILABLE_LOCALES['primary'];
        if (!empty($rawSeo->$field)) {
            return $rawSeo->$field;
        } elseif (!empty($rawSeo->$defaultField)) {
            return $rawSeo->$defaultField;
        } else {
            if (!in_array($metaTag, ['header', 'keywords'])) { //header and keywords are not required for seo
                LogRec::error("meta tag field {$field} in page {$page->name} needed for SEO is empty!");
            }
            return null;
        }
    }
    #ednregion
}
