<?php
/**
 * User: Andrey Naumoff
 * Date: 16-Feb-19
 * Time: 8:53 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Services\Paginations;

use App\Contracts\ArrayPaginationContract;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class ArrayPaginationService implements ArrayPaginationContract
{
    /**
     * @param Request $request
     * @param array $items
     * @param int $perPage
     * @return LengthAwarePaginator
     */
    public function paginateArray(Request $request, array $items, int $perPage = 15): LengthAwarePaginator
    {
        // Get current page form url e.x. &amp;page=1
        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        // Create a new Laravel collection from the array data
        $itemCollection = collect($items);

        // Slice the collection to get the items to display in current page
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();

        // Create our paginator and pass it to the view
        $paginatedItems = new LengthAwarePaginator($currentPageItems, count($itemCollection), $perPage);

        // set url path for generated links
        $paginatedItems->setPath($request->url());

        return $paginatedItems;
    }
}
