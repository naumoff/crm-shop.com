<?php
/**
 * User: Andrey Naumoff
 * Date: 06-Jan-19
 * Time: 1:32 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Services\Converters;


trait DashConverters
{
    #region SERVICE METHODS
    /**
     * @param string $string
     * @param bool $capitalizeFirstChar
     * @return string
     */
    private function dashesToCamelCase(string $string, $capitalizeFirstChar = false): string
    {
        $str = str_replace(' ', '', ucwords(str_replace('-', ' ', $string)));
        if (!$capitalizeFirstChar) {
            $str[0] = strtolower($str[0]);
        }
        return $str;
    }

    /**
     * @param string $string
     * @return string
     */
    private function dashesToUnderscore(string $string): string
    {
        return str_replace('-', '_', $string);
    }
    #endregion
}
