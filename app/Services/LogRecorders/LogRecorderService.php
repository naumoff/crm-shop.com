<?php
/**
 * User: Andrey Naumoff
 * Date: 26-Jan-19
 * Time: 11:42 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Services\LogRecorders;

use App\Contracts\LogRecorderContract;
use Illuminate\Support\Facades\Log;

/**
 * Class LogRecorderService
 * This class provides 2 methods:
 * error() for logging site errors to any default channel
 * debug() for logging to slack channel only
 * This class also modifies log messages for better readability within slack
 * Usage from facade: LogRec::debug($message) or LogRec::error($message)
 * @package App\Services\LogRecorders
 */
class LogRecorderService implements LogRecorderContract
{
    #region MAIN METHODS
    /**
     * This method used to log site failures
     * Within this method we can switch on or switch off slack error notifications
     * @param mixed $message
     */
    public function error($message): void
    {
        $message = $this->modifyMessage($message, debug_backtrace());
        $channels = config('logging.channels.stack.channels'); //getting current log channels
        if (config('logging.slack_error')) { // checking if slack log required
            if (!in_array('slack', $channels)) { // checking if slack log channel included to default
                $channels[] = 'slack'; // adding slack log channel since it is missing
                Log::stack($channels)->error($message);
            } else {
                Log::stack($channels)->error($message); // sending message to all channels including slack
            }
        } else {
            $slackKey = array_search('slack', $channels);
            if ($slackKey !== false) {
                unset($channels[$slackKey]); // removing slack from channels, since it is not required
            }
            Log::stack($channels)->emergency($message);
        }
    }

    /**
     * This method used to do detailed log (mainly on live server)
     * This method provides possibility to do detailed log of all files or for precise file listed in env file
     * Log messages goes to slack channel only
     * @param mixed $message
     */
    public function debug($message): void
    {
        $debugTrace = debug_backtrace();
        $logNeeded = $this->checkLogNeed($debugTrace);
        if ($logNeeded) {
            $message = $this->modifyMessage($message, $debugTrace);
            Log::channel('slack')->debug($message);
        }
    }
    #endregion

    #region SERVICE METHODS

    /**
     * @param array $debugTrace
     * @return bool
     */
    private function checkLogNeed(array $debugTrace): bool
    {
        $debugLogger = config('logging.slack_debug');
        $neededFile = config('logging.slack_debug_file');
        if ($debugLogger && $neededFile) { // do log only for precise file defined in env file
            $calledFile = $debugTrace[1]['file'];
            $logNeeded = $this->compareFileNames($calledFile, $neededFile);
            return $logNeeded;
        } elseif ($debugLogger) { // do log for all files
            return true;
        } else { // do not do any log
            return false;
        }
    }

    /**
     * @param string $calledFile
     * @param string $neededFile
     * @return bool
     */
    private function compareFileNames(string $calledFile, string $neededFile): bool
    {
        if (strtolower(basename($calledFile)) === strtolower(basename($neededFile))) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param mixed $message
     * @param array $debugTrace
     * @return array
     */
    private function modifyMessage($message, array $debugTrace): array
    {
        if (is_array($message)) {
            return array_merge(
                [
                    'source' => substr(realpath($debugTrace[1]['file']), -45),
                    'line' => $debugTrace[1]['line'],
                ],
                $message
            );
        } else {
            return [
                'source' => substr(realpath($debugTrace[1]['file']), -45),
                'line' => $debugTrace[1]['line'],
                'message' => $message
            ];
        }
    }
    #endregion
}
