<?php
/**
 * User: Andrey Naumoff
 * Date: 9/15/2019
 * Time: 3:11 PM
 * E-mail: andrey.naumoff@crm-shop.com
 */
namespace App\Services\PageAccess;

use App\Contracts\UserIpCheckerContract;

class UserIpCheckerService implements UserIpCheckerContract
{
    #region MAIN METHODS
    /**
     * @return bool
     */
    public function checkUserIp(): bool
    {
        $userIp = $this->getUserRealIp();
        $whiteIpList = config('white-ip-list');
        return in_array($userIp, $whiteIpList);
    }
    #endregion
    
    #region SERVICE METHODS
    /**
     * @return string
     */
    private function getUserRealIp(): string
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            //ip from share internet
            $userIp = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            //ip pass from proxy
            $userIp = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $userIp = $_SERVER['REMOTE_ADDR'];
        }
        return $userIp;
    }
    #endregion
}
