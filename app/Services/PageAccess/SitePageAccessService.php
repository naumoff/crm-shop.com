<?php
/**
 * User: Andrey Naumoff
 * Date: 11-Dec-18
 * Time: 3:38 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Services\PageAccess;

use App\Contracts\SitePageAccessCheckerContract;
use App\Contracts\SitePageAccessContract;
use App\Models\UserModels\User;

final class SitePageAccessService implements SitePageAccessContract
{
    #region PROPERTIES
    /** @var SitePageAccessCheckerContract $accessCheckerService */
    private $accessCheckerService;
    #endregion

    #region MAIN METHODS
    /**
     * SitePageAccessService constructor.
     * @param SitePageAccessCheckerContract $accessCheckerService
     */
    public function __construct(SitePageAccessCheckerContract $accessCheckerService)
    {
        $this->accessCheckerService = $accessCheckerService;
    }

    public function viewMenuPage(?User $user, int $pageId): bool
    {
        return $this->accessCheckerService->checkUserRightToViewMenuPage($user, $pageId);
    }
    #endregion
}
