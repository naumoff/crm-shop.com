<?php
/**
 * User: Andrey Naumoff
 * Date: 06-Jan-19
 * Time: 10:03 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Services\PageAccess;

use App\Contracts\MenuPageAccessGatesContract;
use App\Contracts\SitePageAccessCheckerContract;
use App\Models\UserModels\Role;
use App\Models\UserModels\User;
use App\Services\DataHelpers\MenuPageRoutes;

class SitePageAccessCheckerService implements SitePageAccessCheckerContract
{
    use MenuPageRoutes;

    #region PROPERTIES
    /** @var null|array $userRolesIds */
    private $userRolesIds;

    /** @var MenuPageAccessGatesContract $menuPageAccessService */
    private $menuPageAccessService;
    #endregion

    #region MAIN METHODS
    /**
     * SitePageAccessCheckerService constructor.
     * @SuppressWarnings(PHPMD)
     * @param MenuPageAccessGatesContract $menuPageAccessService
     */
    public function __construct(MenuPageAccessGatesContract $menuPageAccessService)
    {
        $this->menuPageAccessService = $menuPageAccessService;
    }

    /**
     * @SuppressWarnings(PHPMD)
     * @param User|null $user
     * @return bool
     */
    public function checkUserRightToViewMenuPage(?User $user, int $pageId): bool
    {
        if (empty($this->userRolesIds)) {
            /** @var array $userRolesIds */
            $this->userRolesIds = $this->getUserRolesIds($user);
        }

        /** @var array $pageAllowableRolesIds */
        $pageAllowableRolesIds = $this->menuPageAccessService->getAllowableRolesIds($pageId);

        return $this->checkGateForUser($pageAllowableRolesIds, $this->userRolesIds);
    }
    #endregion

    #region SERVICE METHODS
    /**
     * @param User|null $user
     * @return array
     */
    private function getUserRolesIds(?User $user): array
    {
        $userRolesIds = [];
        if ($user === null ||
            !$user->hasVerifiedEmail() ||
            !$user->isApproved() ||
            count($user->roles()->get()) === 0
        ) {
            $userRolesIds[] = Role::UNDEFINED;
        } else {
            $roles = $user->roles()->get();
            $userRolesIds = $roles->map(function ($role) {
                return $role->only(['id']);
            })->toArray();

            $userRolesIds = array_map(function ($element) {
                return $element['id'];
            }, $userRolesIds);
        }
        return $userRolesIds;
    }

    /**
     * @param array $allowedRolesIds
     * @param array $userRolesIds
     * @return bool
     */
    private function checkGateForUser(array $allowedRolesIds, array $userRolesIds): bool
    {
        $passedRolesIds = array_intersect($allowedRolesIds, $userRolesIds);
        if (count($passedRolesIds) > 0) {
            return true;
        }
        return false;
    }
    #endregion
}
