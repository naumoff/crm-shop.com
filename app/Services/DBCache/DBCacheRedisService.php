<?php
/**
 * User: Andrey Naumoff
 * Date: 17-Dec-18
 * Time: 7:42 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Services\DBCache;

use App\Contracts\DBCacheContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redis;
use ReflectionClass;
use stdClass;

/**
 * Class DBCacheRedisService
 * @package App\Services\DBCache
 */
final class DBCacheRedisService implements DBCacheContract
{
    #region PROPERTIES
    /** @var \Redis $redis */
    private $redis;

    /** @var string $connection */
    private $connection = 'default';
    #endregion

    #region MAIN METHODS
    public function __construct()
    {
        $this->redis = Redis::connection($this->connection);
    }

    /**
     * @param Model $model
     * @param string $key
     * @param string|null $customModelKeyField (any name different from 'id')
     */
    public function saveModel(Model $model, string $key, string $customModelKeyField = null): void
    {
        if (empty($customModelKeyField)) {
            $this->redis->hset($key, $model->id, $model->toJson());
        } else {
            $this->redis->hset($key, $model->$customModelKeyField, $model->toJson());
        }
    }

    /**
     * @param Model $model
     * @param string $key
     * @param string|null $customModelKeyField (any name different from 'id')
     */
    public function deleteModel(Model $model, string $key, string $customModelKeyField = null): void
    {
        if (empty($customModelKeyField)) {
            $this->redis->hdel($key, $model->id);
        } else {
            $this->redis->hdel($key, $model->$customModelKeyField);
        }
    }

    /**
     * Delete all models stored in redis
     * @param string $model
     */
    public function refreshAllModels(string $model): void
    {
        try {
            if (is_subclass_of($model, Model::class)) {
                $reflectionObject = new ReflectionClass($model);
                $keyPart = strtolower($reflectionObject->getShortName());
                foreach ($this->redis->keys("$keyPart:*") as $key) {
                    foreach (array_keys($this->redis->hGetAll($key)) as $id) {
                        $this->redis->hDel($key, $id);
                    }
                }
                $models = $model::all();
                foreach ($models as $model) {
                    $model->touch();
                }
            } else {
                throw new \Exception("class $model is not inherited from Eloquent model class");
            }
        } catch (\Throwable $throwable) {
            \LogRec::error($throwable->getMessage());
        }
    }

    /**
     * @param string $key
     * @return array
     */
    public function getModelsByKey(string $key): array
    {
        $jsonModels = $this->redis->hgetall($key);
        sort($jsonModels);
        $arrayModels = array_map(function ($hash) {
            return json_decode($hash);
        },
            $jsonModels);
        return $arrayModels;
    }

    /**
     * @param string $key
     * @param $modelKeyValue
     * @return null|stdClass
     */
    public function getModelByKeyAndId(string $key, $modelKeyValue): ?stdClass
    {
        $jsonModel = $this->redis->hget($key, $modelKeyValue);
        return json_decode($jsonModel);
    }
    #endregion
}
