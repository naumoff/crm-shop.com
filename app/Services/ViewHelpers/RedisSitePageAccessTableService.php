<?php
/**
 * User: Andrey Naumoff
 * Date: 06-Jan-19
 * Time: 4:09 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Services\ViewHelpers;

use App\Contracts\SitePageAccessTableContract;
use App\Models\UserModels\Role;
use App\Services\Converters\DashConverters;
use App\Services\DataHelpers\UserRoles;
use stdClass;

class RedisSitePageAccessTableService implements SitePageAccessTableContract
{
    use UserRoles, DashConverters;

    #region MAIN METHODS
    /**
     * converts one row of page_access_gates table into array of objects for easy foreach in cms table
     * @param stdClass $model
     * @return stdClass[]
     */
    public function convertModelToArray(stdClass $model): array
    {
        $arrayOfObjects = [];
        foreach ($this->roleNames as $roleId => $roleName) {
            $object['id'] = $model->id;
            $object['role'] = $roleName;
            $roleName = $this->dashesToUnderscore($roleName);
            $object['value'] = $model->$roleName;
            if ($roleId === Role::UNDEFINED) {
                $object['description'] = 'guest, unverified or no roles assigned';
            } else {
                $object['description'] = null;
            }
            $arrayOfObjects[] = (object) $object;
        }
        return $arrayOfObjects;
    }
    #endregion
}
