## Folder content

#### This folder contains services that are injected directly to blade views

``` 
@inject('accessGate', 'App\Contracts\SitePageAccessTableContract')
@foreach($accessGate->convertModel($pageModel->page_access_gate) as $roleRightObject)
```
Services used as data formatters, data transformers, data helpers only... <br>
Sth. like view-plugins...