<?php
/**
 * User: Andrey Naumoff
 * Date: 16-Jan-19
 * Time: 8:09 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Services\DataHelpers;

/**
 * Trait RegularPageTypeRoutes
 * contains regular  page route names in CMS
 * @package App\Services\DataHelpers
 */
trait RegularPageTypeRoutes
{
    #region REGULAR PAGE TYPES
    /**
     * key = regular page name in CMS
     * value = route name to page list in CMS
     * @var array $regularPageTypes
     */
    private $regularPageTypesRoutesInCMS = [
        'single-page' => '',
    ];
    #endregion
}
