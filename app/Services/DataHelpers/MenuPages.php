<?php
/**
 * User: Andrey Naumoff
 * Date: 04-Jan-19
 * Time: 6:49 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Services\DataHelpers;

use App\Models\PageModels\Page;

trait MenuPages
{
    #region TRAIT PROPERTIES
    /**
     * array values must be equal to appropriate route names to appropriate menu pages
     * @var array
     */
    private $menuPageNames = [
        Page::MAIN => 'main',
        Page::CONTACTS => 'contacts',
    ];

    #endregion

    #region TRAIT METHODS
    /**
     * @return array
     */
    protected function getMenuPageIds(): array
    {
        return array_keys($this->menuPageNames);
    }
    #endregion
}
