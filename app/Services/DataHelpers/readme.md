## Folder content

#### This folder contains traits with data arrays and helper methods to work with this data.

Traits' data mainly used in migrations and contains relative Model's ids joined into groups...

``` 
namespace App\Services\DataHelpers;

use App\Models\PageModels\Page;

trait MenuPages
{
    #region TRAIT PROPERTIES
    /**
     * array values must be equal to appropriate route names to appropriate menu pages
     * @var array
     */
    private $menuPageNames = [
        Page::MAIN => 'main',
        Page::ABOUT_US => 'about-us',
        Page::SERVICES => 'services',
        Page::OUR_WORK => 'our-work',
        Page::NEWS => 'news',
        Page::CONTACTS => 'contacts',
    ];
```