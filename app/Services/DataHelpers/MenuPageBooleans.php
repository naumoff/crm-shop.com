<?php
/**
 * User: Andrey Naumoff
 * Date: 31-Dec-18
 * Time: 4:39 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Services\DataHelpers;

/**
 * Trait MenuPageBooleans
 * Used only during data migration to db
 * @package App\Services\DataHelpers
 */
trait MenuPageBooleans
{
    #region MAIN PAGE
    protected $mainPageBooleans = [
        [
            'name' => 'showShareButtons',
            'description' => 'share buttons show/hide',
            'value' => false
        ],
    ];
    #endregion

    #region CONTACTS PAGE
    protected $contactsPageBooleans = [
        [
            'name' => 'showGoogleMap',
            'description' => 'google Map show/hide',
            'value' => true
        ],
    ];
    #endregion
}
