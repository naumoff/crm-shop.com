## Folder content

#### This folder contains traits that defines different receptors for different notifications.
All traits must define one method getReceptors() that must return appropriate collection of users

``` 
    /**
     * @return Collection|null
     */
    private function getReceptors(): ?Collection
    {
        $admins = Role::admin()->first()->users->where('approved', '=', 1);
        return $admins;
    }
```