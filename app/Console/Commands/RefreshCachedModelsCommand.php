<?php

namespace App\Console\Commands;

use App\Contracts\DBCacheContract;
use App\Events\RefreshCachedModels;
use Illuminate\Console\Command;

class RefreshCachedModelsCommand extends Command
{
    #region PROPERTIES
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'refresh:cached.models';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command is refreshing models stored in cache';

    /** @var DBCacheContract $dbCacheService */
    private $dbCacheService;
    #endregion

    #region MAIN METHODS
    /**
     * RefreshCachedModelsCommand constructor.
     * @param DBCacheContract $DBCacheContract
     */
    public function __construct(DBCacheContract $DBCacheContract)
    {
        parent::__construct();
        $this->dbCacheService = $DBCacheContract;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        event(new RefreshCachedModels($this->dbCacheService));
    }
    #endregion
}
