<?php

namespace App\Events;

use App\Contracts\DBCacheContract;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class RefreshCachedModels
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    #region PROPERTIES
    public $DBCacheService;
    #endregion

    #region MAIN METHODS
    /**
     * RefreshCachedModels constructor.
     * @param DBCacheContract $DBCacheContract
     */
    public function __construct(DBCacheContract $DBCacheContract)
    {
        $this->DBCacheService = $DBCacheContract;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }

    #endregion
}
