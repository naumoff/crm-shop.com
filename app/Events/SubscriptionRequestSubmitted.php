<?php

namespace App\Events;

use App\Models\RequestModels\SubscribeRequest;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Queue\ShouldQueue;

class SubscriptionRequestSubmitted implements ShouldQueue
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    #region PROPERTIES
    /** @var SubscribeRequest $subscribeRequest */
    public $subscribeRequest;
    #endregion

    #region MAIN METHODS

    /**
     * SubscriptionRequestSubmitted constructor.
     * @param SubscribeRequest $subscribeRequest
     */
    public function __construct(SubscribeRequest $subscribeRequest)
    {
        $this->subscribeRequest = $subscribeRequest;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
    #endregion
}
