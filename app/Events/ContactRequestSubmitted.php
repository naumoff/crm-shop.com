<?php

namespace App\Events;

use App\Models\RequestModels\ContactRequest;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactRequestSubmitted implements ShouldQueue
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    #region PROPERTIES
    /** @var ContactRequest $contactRequest */
    public $contactRequest;
    #endregion

    #region MAIN METHODS
    /**
     * ContactRequestSubmitted constructor.
     * @param ContactRequest $contactRequest
     */
    public function __construct(ContactRequest $contactRequest)
    {
        $this->contactRequest = $contactRequest;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
    #endregion
}
