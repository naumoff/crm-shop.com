<?php

namespace App\Observers;

use App\Contracts\DBCacheContract;
use App\Contracts\ModelObserverContract;
use App\Models\PageModels\PageType;
use Illuminate\Database\Eloquent\Model;

class PageTypeObserver implements ModelObserverContract
{
    #region PROPERTIES
    /** @var DBCacheContract $cacheRecorder */
    private $dbCacheService;
    #endregion

    #region MAIN METHODS
    /**
     * PageObserver constructor.
     * @param DBCacheContract $dbCacheService
     */
    public function __construct(DBCacheContract $dbCacheService)
    {
        $this->dbCacheService = $dbCacheService;
    }

    /**
     * @param Model $pageType
     */
    public function saved(Model $pageType): void
    {
        $this->loadPageTypeRelations($pageType);
        if (in_array($pageType->id, [PageType::MENU, PageType::SINGLE])) {
            $this->savePageType($pageType, self::PAGE_TYPE_KEY, 'type');
        }
    }

    /**
     * @param Model $pageType
     */
    public function deleted(Model $pageType):void
    {
        if (in_array($pageType->id, [PageType::MENU, PageType::SINGLE])) {
            $this->deletePage($pageType, self::PAGE_TYPE_KEY, 'type');
        }
    }

    public function refreshed(): void
    {
        // TODO: Implement refreshed() method. Queue job is required here
    }
    #endregion

    #region SERVICE METHODS
    /**
     * @param Model $pageType
     */
    private function loadPageTypeRelations(Model $pageType): void
    {
        $pageType->load('pages');
    }

    /**
     * @param Model $pageType
     * @param string $pageTypeKey
     * @param string|null $customModelKeyField
     */
    private function savePageType(Model $pageType, string $pageTypeKey, string $customModelKeyField = null): void
    {
        $this->dbCacheService->saveModel($pageType, $pageTypeKey, $customModelKeyField);
    }

    /**
     * @param Model $pageType
     * @param string $pageTypeKey
     * @param string|null $customModelKeyField
     */
    private function deletePage(Model $pageType, string $pageTypeKey, string $customModelKeyField = null): void
    {
        $this->dbCacheService->deleteModel($pageType, $pageTypeKey, $customModelKeyField);
    }
    #endregion
}
