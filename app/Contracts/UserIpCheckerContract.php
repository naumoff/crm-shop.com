<?php
/**
 * User: Andrey Naumoff
 * Date: 9/15/2019
 * Time: 3:12 PM
 * E-mail: andrey.naumoff@crm-shop.com
 */
namespace App\Contracts;

interface UserIpCheckerContract
{
    /**
     * method returns true, if user ip is within predefined ip's whitelist
     * @return bool
     */
    public function checkUserIp(): bool;
}
