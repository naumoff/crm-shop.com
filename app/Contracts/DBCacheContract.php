<?php
/**
 * User: Andrey Naumoff
 * Date: 17-Dec-18
 * Time: 7:41 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Contracts;

use Illuminate\Database\Eloquent\Model;
use stdClass;

interface DBCacheContract
{
    /**
     * @param Model $model
     * @param string $key
     * @param string|null $customModelKeyField
     */
    public function saveModel(Model $model, string $key, string $customModelKeyField = null): void;

    /**
     * @param Model $model
     * @param string $key
     * @param string|null $customModelKeyField
     */
    public function deleteModel(Model $model, string  $key, string $customModelKeyField = null): void;

    /**
     * Delete all models stored in redis
     * @param string $model
     */
    public function refreshAllModels(string $model): void;

    /**
     * @param string $key
     * @return array
     */
    public function getModelsByKey(string $key): array;

    /**
     * @param string $key
     * @param $modelKeyValue
     * @return null|stdClass
     */
    public function getModelByKeyAndId(string $key, $modelKeyValue): ?stdClass;
}
