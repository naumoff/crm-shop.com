<?php
/**
 * User: Andrey Naumoff
 * Date: 11-Dec-18
 * Time: 3:44 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Contracts;

use App\Models\UserModels\User;

interface SitePageAccessContract
{
    #region CONSTANTS VIEW RIGHTS LIST
    /** view rights values must be equal to relevant method names in service implementing this contract */
    public const VIEW_MENU_PAGE = 'viewMenuPage';
    #endregion

    #region CONTRACTS
    public function viewMenuPage(?User $user, int $pageId): bool;
    #endregion
}
