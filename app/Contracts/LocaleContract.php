<?php
/**
 * User: Andrey Naumoff
 * Date: 22-Nov-18
 * Time: 11:47 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Contracts;

interface LocaleContract
{
    public const AVAILABLE_LOCALES = [
        'primary' => 'en',
        'ru',
        'ua'
    ];

    public function setLocale(): self;

    public function getLocale(): string;
}
