<?php
/**
 * User: Andrey Naumoff
 * Date: 06-Jan-19
 * Time: 4:10 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Contracts;

use stdClass;

interface SitePageAccessTableContract
{
    /**
     * @param stdClass $model
     * @return stdClass[]
     */
    public function convertModelToArray(stdClass $model): array;
}
