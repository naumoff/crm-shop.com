<?php
/**
 * User: Andrey Naumoff
 * Date: 01-Jan-19
 * Time: 2:39 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Contracts;

use stdClass;

interface PageFieldsContract
{
    /**
     * @param string $locale
     * @return stdClass
     */
    public function getMenuPageFields(string $locale): stdClass;

    /**
     * @param stdClass $page
     * @param string $locale
     * @return stdClass
     */
    public function getRegularPageFields(stdClass $page, string $locale): stdClass;
}
