<?php
/**
 * User: Andrey Naumoff
 * Date: 17-Dec-18
 * Time: 10:00 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Contracts;

use Illuminate\Database\Eloquent\Model;

interface ModelObserverContract
{
    #region KEYS CONSTANTS
    public const PAGE_MENU_KEY = 'page:menu';
    public const PAGE_SINGLE_KEY = 'page:single';

    public const PAGE_TYPE_KEY = 'type';

    public const USER_ALL = 'user:all';
    public const USER_NOT_APPROVED = 'user:not-approved';
    public const USER_NOT_VERIFIED = 'user:not-verified';
    public const USER_WITHOUT_ROLE = 'user:without-role';
    public const USER_DELETED = 'user:deleted';
    #endregion

    #region CONTRACTS
    public function saved(Model $model): void;

    public function deleted(Model $model):void;

    public function refreshed(): void;
    #endregion
}
