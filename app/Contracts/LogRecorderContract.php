<?php
/**
 * User: Andrey Naumoff
 * Date: 27-Jan-19
 * Time: 6:33 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Contracts;


interface LogRecorderContract
{
    public function error($message): void;

    public function debug($message): void;
}
