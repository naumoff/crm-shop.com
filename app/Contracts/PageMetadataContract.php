<?php
/**
 * User: Andrey Naumoff
 * Date: 23-Dec-18
 * Time: 1:53 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Contracts;

use stdClass;

interface PageMetadataContract
{
    /**
     * @param int $pageId
     * @param string $locale
     * @return stdClass
     */
    public function getMenuPageMetadata(int $pageId, string $locale): stdClass;

    /**
     * @param stdClass $page
     * @param string $locale
     * @return stdClass
     */
    public function getRegularPageMetadata(stdClass $page, string $locale): stdClass;

    /**
     * @param string $locale
     * @return stdClass
     */
    public function getMenuPagesNames(string $locale): stdClass;
}
