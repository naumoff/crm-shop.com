<?php
/**
 * User: Andrey Naumoff
 * Date: 07-Jan-19
 * Time: 10:32 AM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Contracts;


interface MenuPageAccessGatesContract
{
    /**
     * @param int $pageId
     * @return array
     */
    public function getAllowableRolesIds(int $pageId): array;
}
