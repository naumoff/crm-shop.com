<?php
/**
 * User: Andrey Naumoff
 * Date: 06-Jan-19
 * Time: 10:04 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Contracts;

use App\Models\UserModels\User;

interface SitePageAccessCheckerContract
{
    /**
     * @param User|null $user
     * @param int $pageId
     * @return bool
     */
    public function checkUserRightToViewMenuPage(?User $user, int $pageId): bool;
}
