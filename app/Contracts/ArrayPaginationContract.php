<?php
/**
 * User: Andrey Naumoff
 * Date: 16-Feb-19
 * Time: 8:54 PM
 * E-mail: andrey.naumoff@gmail.com
 */

namespace App\Contracts;


use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

interface ArrayPaginationContract
{
    /**
     * @param Request $request
     * @param array $items
     * @param int $perPage
     * @return LengthAwarePaginator
     */
    public function paginateArray(Request $request, array $items, int $perPage = 15): LengthAwarePaginator;
}
