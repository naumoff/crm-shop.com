<?php
/**
 * User: Andrey Naumoff
 * Date: 25-Nov-18
 * Time: 10:20 PM
 * E-mail: andrey.naumoff@gmail.com
 */

use App\Models\UserModels\Role;

/** CONTENT MANAGEMENT DASHBOARD */
Route::group(
    [
        'middleware' => [
            'role:' . Role::ADMIN .','. Role::CONTENT_MANAGER,
            'approved',
            'auth'
        ],
        'prefix' => '/cms-manager',
        'namespace' => 'Manager',
    ],
    function () {
        Route::get('/', 'ManagerController@welcome')->name('cms-manager');
        Route::get('/menu-page-metadata/{pageId}/edit', 'MenuPageSeoController@seoEdit')
            ->name('menu-page-metadata-form');
        Route::patch('/menu-page-metadata-title/{page}', 'MenuPageSeoController@menuPageMetadataTitleUpdate')
            ->name('menu-page-metadata-title-patch');
        Route::patch(
            '/menu-page-metadata-description/{page}',
            'MenuPageSeoController@menuPageMetadataDescriptionUpdate'
        )->name('menu-page-metadata-description-patch');
        Route::patch('/menu-page-metadata-keywords/{page}', 'MenuPageSeoController@menuPageMetadataKeywordsUpdate')
            ->name('menu-page-metadata-keywords-patch');
        Route::patch('/menu-page-metadata-header/{page}', 'MenuPageSeoController@menuPageMetadataHeaderUpdate')
            ->name('menu-page-metadata-header-patch');
        Route::get('/menu-page-fields/{pageId}/edit', 'MenuPageFieldsController@fieldsEdit')
            ->name('menu-page-fields-form');
        Route::patch(
            '/menu-page-fields-booleans/{booleanField}',
            'MenuPageFieldsController@menuPageFieldsBooleansUpdate'
        )->name('menu-page-fields-booleans-patch');
        Route::patch('/menu-page-access/{pageAccessGate}', 'MenuPageAccessController@menuPageAccessUpdate')
            ->name('menu-page-access-patch');

        Route::get('/regular-page-types/{pageType}', 'RegularPageFieldsController@show')->name('page-type-pages');
        Route::get('/regular-page/{pageTypeName}/{page}/edit', 'RegularPageFieldsController@fieldsEdit')
            ->name('regular-page-fields-form');
        Route::patch(
            '/regular-page-raw-html-patch',
            'RegularPageFieldsController@rawHtmlUpdate'
        )->name('regular-page-raw-html-patch');
    }
);

/** CONTENT EDITORIAL DASHBOARD */
Route::group(
    [
        'middleware' => [
            'role:' . Role::ADMIN .','. Role::CONTENT_MANAGER .','. Role::CONTENT_EDITOR,
            'approved',
            'auth'
        ],
        'prefix' => '/cms-editor',
        'namespace' => 'Editor',
    ],
    function () {
        Route::get('/', 'EditorController@index')->name('cms-editor');
    }
);
