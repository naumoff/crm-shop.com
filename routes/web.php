<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(
    [
        'prefix'=>'{locale?}',
    ],
    function () {
        Route::get('', 'MenuController@index')->name('main');
        Route::get('contacts', 'MenuController@contacts')->name('contacts');
        Route::get('not-found', 'MenuController@notFoundPage')->name('not-found');

        Route::post('contacts', 'RequestController@storeContactRequest')
            ->name('contacts.store');
        Route::post('subscriptions', 'RequestController@storeSubscriptionRequest')
            ->name('subscriptions.store');
        Route::get('get-cv', 'RequestController@getCV')->name('cv.get');

        /**  do not change placeholders names here */
        // must be last in list due to greedy pattern
        Route::get('{singlePage}', 'PageController@show')->name('single-page');
    }
);
